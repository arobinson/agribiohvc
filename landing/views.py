
import logging

from django.shortcuts import render
from django.urls import reverse
from django.db import models
from AgriBioHVC import settings
from django.http.response import Http404, HttpResponse
from django.core.exceptions import ObjectDoesNotExist
import re
try:
    from django.template.context_processors import csrf
except:
    from django.core.context_processors import csrf

import common
from common.bl.messagemanager import MessageManager
from ptadmin.bl import datamanager
from common.bl.datamanager import DataManager


def home(request, collectionuid=None):
    '''Renders the homepage'''
    
    mycollectionuid = collectionuid
    if not collectionuid:
        mycollectionuid = settings.DEFAULT_COLLECTION
    try:
        collection = common.models.Collection.objects.get(uid=mycollectionuid)
    except ObjectDoesNotExist:
        raise Http404
    
    page = settings.PORTAL_NAME
    if collectionuid:
        page = collection.get_name_short()
    
#    logging.debug(collectionuid)
    
    # prepare response
    params = {
              'request': request, 
              'messages': MessageManager.getMessages(request),
              'group': 'landing', 
              'parents': _homeParentList(collection, not collectionuid),
              'page': page, 
              'children': _organismURLs(common.models.Species.objects.filter(deleted__isnull=True), collectionuid),
              'organism': DataManager.getOrganismsForCollectionAndUser(collection, request.user).order_by('name'),
              'settings': settings,
              'title': '',
              'collection': collection,
              'collections': DataManager.getCollections(request.user).filter(link_collection=True).order_by('name'),
             }
    params.update(csrf(request))
    
    return render(request, "landing/home.html", params)
    

def _organismURLs(organisms, collectionuid):
    '''Generator that yields 2-tuples for each organism'''
    
    for o in organisms.order_by('name').values('name', 'uid'):
        yield (o['name'], reverse('datasetlist', kwargs={'collectionuid': collectionuid, 'organismuid': o['uid'],}))

def _homeParentList(collection, isdefault):
    ''''''
    parents = []
    if collection.link_ltu:
        parents.append(settings.HOME_LINK)
    if collection.link_top and not isdefault and collection.uid != settings.DEFAULT_COLLECTION:
        parents.append((settings.PORTAL_NAME, reverse('home'))),
    return parents

def _parentList(collection, isdefault, *args):
    '''Constructs the parent list for the given collection'''
    
    parents = []
    if collection.link_ltu:
        parents.append(settings.HOME_LINK)
    if collection.link_top:
        parents.append((settings.PORTAL_NAME, reverse('home'))),
    if not isdefault:
        parents.append((collection.get_name_short(), reverse('home', kwargs={'collectionuid': collection.uid}))),
    parents.extend(args)
    return parents

def datasetlist(request, organismuid, collectionuid=None):
    '''Renders a list of data-sets for specified organism'''
    
    mycollectionuid = collectionuid
    if not collectionuid:
        mycollectionuid = settings.DEFAULT_COLLECTION
    try:
        collection = common.models.Collection.objects.get(uid=mycollectionuid)
        organism = common.models.Species.objects.get(uid=organismuid, deleted__isnull=True)
        datasets = DataManager.getDatasetsForUser(request.user, organism.dataset_set).filter(collections__in=[collection,], deleted__isnull=True).order_by('name')
    except ObjectDoesNotExist:
        raise Http404
    
    # prepare response
    params = {
              'request': request, 
              'messages': MessageManager.getMessages(request),
              'group': 'landing', 
              'parents': _parentList(collection, not collectionuid),
              'page': organism.name, 
              'children': [ (ds.name, reverse('datasetdetails', kwargs={'collectionuid': collectionuid, 'organismuid': organism.uid, 'datasetuid': ds.uid,})) for ds in datasets ],
              'siblings': _organismURLs(common.models.Species.objects.filter(deleted__isnull=True).exclude(uid=organismuid), collectionuid),
              'organism': organism,
              'datasets': datasets,
              'settings': settings,
              'title': '%s data-sets' % organism.name,
              'collection': collection,
             }
    params.update(csrf(request))
    
    return render(request, "landing/datasetlisting.html", params)
    
    
def datasetdetails(request, organismuid, datasetuid, collectionuid=None):
    '''Renders detailed view of a data-set'''
    
    mycollectionuid = collectionuid
    if not collectionuid:
        mycollectionuid = settings.DEFAULT_COLLECTION
    try:
        collection = common.models.Collection.objects.get(uid=mycollectionuid)
        organism = common.models.Species.objects.get(uid=organismuid)
        dataset = DataManager.getDatasetsForUser(request.user, organism.dataset_set.filter(deleted__isnull=True)).get(uid=datasetuid)
        samples = dataset.sample_set.filter(deleted__isnull=True).order_by('order')
    except ObjectDoesNotExist:
        raise Http404
    
    # prepare response
    params = {
              'request': request, 
              'messages': MessageManager.getMessages(request),
              'group': 'landing', 
              'parents': _parentList(collection, not collectionuid,
                  ('%s datasets' % organism.name, reverse('datasetlist', kwargs={'collectionuid': collectionuid, 'organismuid': organism.uid, })),
              ),
              'page': dataset.name, 
              'children': [ (s.name, reverse('sampledetails', kwargs={'collectionuid': collectionuid, 'organismuid': organism.uid, 'datasetuid': dataset.uid, 'sampleuid': s.name})) for s in samples ],
              'organism': organism,
              'dataset': dataset,
              'samples': samples,
              'settings': settings,
              'title': '%s details' % dataset.name,
              'collection': collection,
             }
    params.update(csrf(request))
    
    return render(request, "landing/datasetdetails.html", params)


def sampledetails(request, organismuid, datasetuid, sampleuid, collectionuid=None):
    '''Renders detailed view of a sample'''
    
    mycollectionuid = collectionuid
    if not collectionuid:
        mycollectionuid = settings.DEFAULT_COLLECTION
    try:
        collection = common.models.Collection.objects.get(uid=mycollectionuid)
        organism = common.models.Species.objects.get(uid=organismuid)
        dataset = DataManager.getDatasetsForUser(request.user, organism.dataset_set.filter(deleted__isnull=True)).get(uid=datasetuid)
        sample = dataset.sample_set.get(name=sampleuid, deleted__isnull=True)
    except ObjectDoesNotExist:
        raise Http404
    
    # prepare response
    params = {
              'request': request, 
              'messages': MessageManager.getMessages(request),
              'group': 'landing', 
              'parents': _parentList(collection, not collectionuid,
                  ('%s datasets' % organism.name, reverse('datasetlist', kwargs={'collectionuid': collection.uid, 'organismuid': organism.uid, })),
                  (dataset.name, reverse('datasetdetails', kwargs={'collectionuid': collection.uid, 'organismuid': organism.uid, 'datasetuid': dataset.uid,})),
              ),
              'page': sample.name, 
              'children': [ ],
              'organism': organism,
              'dataset': dataset,
              'sample': sample,
              'settings': settings,
              'title': '%s details' % sample.name,
              'collection': collection,
             }
    params.update(csrf(request))
    
    return render(request, "landing/sampledetails.html", params)


def datasetfeature(request, datasetuid, featureuid, collectionuid=None):
    '''Renders detailed view of a feature with links to feature in viewers'''
    
    mycollectionuid = collectionuid
    if not collectionuid:
        mycollectionuid = settings.DEFAULT_COLLECTION
    try:
        collection = common.models.Collection.objects.get(uid=mycollectionuid)
        dataset = DataManager.getDatasetsForUser(request.user).get(uid=datasetuid)
        feature = dataset.features.get(uid=featureuid)
    except ObjectDoesNotExist:
        raise Http404
    
    if feature.name:
        title = '%s: %s (%s)' % (dataset.name, feature.name, feature.uid)
        featurename = '%s (%s)' % (feature.name, feature.uid)
    else:
        title = '%s: %s' % (dataset.name, feature.uid)
        featurename = feature.uid
    
    # prepare response
    params = {
              'request': request, 
              'messages': MessageManager.getMessages(request),
              'group': 'landing', 
              'parents': _parentList(collection, not collectionuid,
                  ('%s datasets' % dataset.species.name, reverse('datasetlist', kwargs={'collectionuid': collection.uid, 'organismuid': dataset.species.uid, })),
                  (dataset.name, reverse('datasetdetails', kwargs={'collectionuid': collection.uid, 'organismuid': dataset.species.uid, 'datasetuid': dataset.uid})),
              ),
              'page': featurename,
              'children': [ ],
              'dataset': dataset,
              'feature': feature,
              'settings': settings,
              'title': title,
              'collection': collection,
              'extlink': ExternalLinker(dataset, feature.uid),
             }
    params.update(csrf(request))
    
    return render(request, "landing/datasetfeature.html", params)

class ExternalLinker(object):
    def __init__(self, dataset, feature):
        self._dataset = dataset
        self._feature = feature
        self._geneid = None
    def url(self):
        if self._feature:
            self._loadGeneID()
            return self._dataset.externallinkurl.replace('GENEID', self._geneid)
        return None
    def label(self):
        if self._feature:
            self._loadGeneID()
            return self._dataset.externallinklabel.replace('GENEID', self._geneid)
        return None
    def _loadGeneID(self):
        if self._geneid is None:
            if self._dataset.externallinkregex:
                p = re.compile(self._dataset.externallinkregex)
                s = self._dataset.externallinksub
                if not s:
                    s = r'\0'
                self._geneid = p.sub(s, self._feature)
            else:
                self._geneid = self._feature
            

def datasetdownload(request, datasetuid, downloadformat):
    '''
    Serves the data-set input data in various forms
    
    @param datasetuid: Unique id for dataset
    @param format: 'default'=format data was uploaded, 'raw', 'tpm' or, 'rpkm'
    '''
    
    # load dataset object
    try:
        dataset = DataManager.getDatasetsForUser(request.user).get(uid=datasetuid)
    except:
        raise Http404
    
    # check we are allowed to download (ticked allow downloads)
    if not dataset.countsdownload:
        raise Http404
    
    # get the required filename
    try:
        if downloadformat == "default":
            filename = datamanager.DataManager.getFilenameForDataset(dataset, None)
            attachname = dataset.countsfile
        elif downloadformat == "raw":
            filename = datamanager.DataManager.getFilenameForDataset(dataset, "RAW")
            attachname = 'counts-raw.csv'
        elif downloadformat == "tpm":
            filename = datamanager.DataManager.getFilenameForDataset(dataset, "TPM")
            attachname = 'counts-tpm.csv'
        elif downloadformat == "rpkm":
            filename = datamanager.DataManager.getFilenameForDataset(dataset, "RPKM")
            attachname = 'counts-rpkm.csv'
        else:
            raise Http404
    except:
        raise Http404
    
    # serve file content
    fc = open(filename, 'rb')
    response = HttpResponse(content=fc)
    response['Content-Type'] = 'text/csv'
    response['Content-Disposition'] = 'attachment; filename="%s"'% attachname
    return response


# def referencelist(request):
#     '''Renders a list of available references'''
#     
#     # prepare response
#     params = {
#               'request': request, 
#               'messages': MessageManager.getMessages(request),
#               'group': 'landing', 
#               'parents': [
#                   settings.HOME_LINK,
#                   (settings.PORTAL_NAME, reverse('home')),
#               ],
#               'page': 'References', 
#               'species': common.models.Species.objects.filter(deleted__isnull=True),
#               'settings': settings,
#               'title': 'Reference List',
#              }
#     params.update(csrf(request))
#     
#     return render(request, "landing/referencelist.html", params)
# 
# 
# def referencedetails(request, referenceuid):
#     '''Renders reference details including a list of data-sets'''
#     
#     reference = common.models.Reference.objects.get(uid=referenceuid, deleted__isnull=True)
#     datasets = DataManager.getDatasetsForUser(request.user, reference.dataset_set)
#     
#     # prepare response
#     params = {
#               'request': request, 
#               'messages': MessageManager.getMessages(request),
#               'group': 'landing', 
#               'parents': [
#                   settings.HOME_LINK,
#                   (settings.PORTAL_NAME, reverse('home')),
#                   ('References', reverse('referencelist')),
#               ],
#               'page': '%s datasets' % reference.name, 
#               'children': [ (ds.name, reverse('datasetdetails', args=(reference.uid, ds.uid))) for ds in datasets ],
#               'reference': reference,
#               'datasets': datasets,
#               'settings': settings,
#               'title': '%s data-sets' % reference.name,
#              }
#     params.update(csrf(request))
#     
#     return render(request, "landing/referencedetails.html", params)
#     
#     
# def referencegene(request, referenceuid, featureid):
#     '''Renders reference gene/feature details with links directly to each viewer'''
#     
# #     try:
# #         feature = common.models.ReferenceFeature.objects.get(uid=featureid)
# #         assert feature.reference.uid == referenceuid
# #     except:
# #         raise Http404
# #     
# #     # prepare response
# #     params = {
# #               'request': request, 
# #               'messages': MessageManager.getMessages(request),
# #               'group': 'landing', 
# #               'parents': [
# #                   settings.HOME_LINK,
# #                   (settings.PORTAL_NAME, reverse('home')),
# #                   ('References', reverse('referencelist')),
# #               ],
# #               'page': '%s datasets' % reference.name, 
# #               'children': [ (ds.name, reverse('datasetdetails', args=(reference.uid, ds.uid))) for ds in datasets ],
# #               'reference': reference,
# #               'datasets': datasets,
# #               'settings': settings,
# #               'title': '%s data-sets' % reference.name,
# #              }
# #     params.update(csrf(request))
# #     
# #     return render(request, "landing/referencegene.html", params)
#     

