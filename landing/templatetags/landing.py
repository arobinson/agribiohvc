'''
Created on 28Mar.,2017

@author: arobinson
'''

import markdown

from django import template
from django.db.models import Q
from django.utils import timezone

import common
import logging

register = template.Library() 

@register.filter(name="markdown", is_safe=True)
def markdown_filter(src):
    '''Renders markdown source into HTML'''
    if src:
        return markdown.markdown(src)
    return ""

@register.filter(name="updateable")
def updateable_value_filter(name):
    '''Renders an updateable value from database'''
    try:
        return common.models.UpdateableValue.objects.get(name=name).value
    except:
        return ""

@register.filter(name="notdeleted")
def notdeleted_filter(relation):
    '''Filters result set by non-deleted objects'''
    try:
        return relation.filter(deleted__isnull=True)
    except:
        return ""
    
@register.filter(name="count")
def count_filter(relation):
    '''Gets result set size'''
    try:
        return relation.count()
    except:
        return "0"

@register.filter(name="publicprivate")
def publicprivate_filter(dataset):
    '''Returns string for 'Public' | 'Private' | 'Private (until DD-MM-YYYY)' based on it's published status'''
    try:
        if dataset.publish is None:
            return "Private"
        if dataset.publish > timezone.now():
            from django.utils.formats import date_format
            return "Embargo (until %s)" % (date_format(timezone.localtime(dataset.publish), format='SHORT_DATETIME_FORMAT', use_l10n=True))
        return "Public"
    except:
        logging.exception("")
        return "Private (error)"

@register.filter(name="in_collection")
def in_collection_filter(relation, collection):
    '''Filters dataset relation to those only in collection'''
    return relation.filter(collections__in=[collection,])
    
@register.filter(name="accessible")
def accessible_filter(relation, user, collection=None):
    '''Computes a string describing accessible data-sets'''
    try:
        public = relation.filter(publish__lte=timezone.now()).count() # only published data-sets
        
        privateq = relation.filter(Q(publish__gt=timezone.now())|Q(publish__isnull=True)) # all non-published data-sets
        if (not user.has_perm('AgriBioHVC.can_view_any_dataset')):
            privateq = privateq.filter(labgroup__in=user.groups.all()) # only ones from user's lab-group
        
        private = privateq.count()
        
        if (private > 0):
            return "%s %s (+%s private)" % (public, pluralise(public, 'data-set'), private)
        
        return "%s %s" % (public, pluralise(public, 'data-set'))
    except:
        logging.exception("accessible error")
        return "0 data-sets"
    
def pluralise(count, string, method=('s', None)):
    '''Creates the plural version of string if count =0 or >=2'''
    
    if count == 1:
        return string
    return string[:method[1]] + method[0]
    