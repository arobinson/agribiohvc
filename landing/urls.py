"""
Landing portal URL Configuration

NOTE: these urlpatterns are appended to end of master URLs so they are 
      effectively top-level URLs too.
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.views.generic.base import RedirectView

import landing.views

from common.uid import ORGANISMUID, COLLECTIONUID, DATASETUID, SAMPLEUID, FEATUREUID

urlpatterns = [
    
    url(r'^(?:(?P<collectionuid>'+COLLECTIONUID+r')?)$', landing.views.home, name='home'),
    
    # /[COLLECTIONUID/]dataset/ORGANISMUID/DATASETUID/SAMPLEUID
    url(r'^(?:(?P<collectionuid>'+COLLECTIONUID+r')/)?dataset/(?P<organismuid>'+ORGANISMUID+r')$', landing.views.datasetlist, name='datasetlist'),
    url(r'^(?:(?P<collectionuid>'+COLLECTIONUID+r')/)?dataset/(?P<organismuid>'+ORGANISMUID+r')/(?P<datasetuid>'+DATASETUID+r')$', landing.views.datasetdetails, name='datasetdetails'),
    url(r'^(?:(?P<collectionuid>'+COLLECTIONUID+r')/)?dataset/(?P<organismuid>'+ORGANISMUID+r')/(?P<datasetuid>'+DATASETUID+r')/(?P<sampleuid>'+SAMPLEUID+r')$', landing.views.sampledetails, name='sampledetails'),
    
    # /download/DATASETUID/FORMAT
    url(r'^download/('+DATASETUID+r')/([a-z]+)$', landing.views.datasetdownload, name='datasetdownload'),
    
    # /feature/DATASETUID/FEATUREUID
    url(r'^(?:(?P<collectionuid>'+COLLECTIONUID+r')/)?feature/(?P<datasetuid>'+DATASETUID+r')/(?P<featureuid>'+FEATUREUID+r')$', landing.views.datasetfeature, name='datasetfeature'),
    
    # /reference/REFERENCEUID[/FEATUREUID]
#     url(r'^reference$', landing.views.referencelist, name='referencelist'),
#     url(r'^reference/('+REFERENCEUID+r')$', landing.views.referencedetails, name='referencedetails'),
#     url(r'^reference/('+REFERENCEUID+r')/('+FEATUREUID+r')$', landing.views.referencegene, name='referencegene'),
    
    url(r'^help', RedirectView.as_view(url='https://docs.google.com/document/d/1o7IyQp02bsBW_FZDo6RFzSHD34aG_wJuhuvBYFv2GLc', permanent=False)),

]
