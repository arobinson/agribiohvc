# AgriSeqDB - Expression Viewer

A portal to connect eFP Browser and Degust viewer systems

## Documentation

* **Installation guide**: [https://docs.google.com/document/d/1PEXzw_xNHFmAh6LqcQVnKbaLdO5N-gZj71inanVC1hw](https://docs.google.com/document/d/1PEXzw_xNHFmAh6LqcQVnKbaLdO5N-gZj71inanVC1hw)
* **Dataset admin guide**: [https://docs.google.com/document/d/1H7452YW3vHa4KZlbwh8GpfZBqXQBnJ1u8AE4Zj2Lpkg](https://docs.google.com/document/d/1H7452YW3vHa4KZlbwh8GpfZBqXQBnJ1u8AE4Zj2Lpkg)
* **Dataset checklist**: [https://docs.google.com/document/d/1S6EgsEPNDwYu_JJ3qSmjTpxVlVkeCFqSXRWe_QbwUUE](https://docs.google.com/document/d/1S6EgsEPNDwYu_JJ3qSmjTpxVlVkeCFqSXRWe_QbwUUE)

## External Links

* **[Degust](http://degust.erc.monash.edu/)**: used by GeneExplore and allows you to find genes of interest based on their expression profile.
* **[eFP Browser](http://www.bar.utoronto.ca/)**: used by GeneView (eFP) and allows you to view the expression of a single gene or transcript.
* **[Galaxy RNA-seq tutorial](https://galaxyproject.org/tutorials/rb_rnaseq/)**: useful for preparing your data for use with AgriSeqDB.
