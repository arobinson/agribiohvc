"""
Landing portal URL Configuration

Namespace: efp
"""
from django.conf.urls import include, url
from django.contrib import admin

import efpbrowser.views

from common.uid import COLLECTIONUID, ORGANISMUID, DATASETUID

urlpatterns = [
    
    # /ORGANISMUID/DATASETUID/
    url(r'^(?:(?P<collectionuid>'+COLLECTIONUID+r')/)?(?P<organismuid>'+ORGANISMUID+r')/(?P<datasetuid>'+DATASETUID+r')$', efpbrowser.views.home, name='home'),

    url(r'^static(?:/([^/]*))?$', efpbrowser.views.static, name='static'),
    
    # /ORGANISMUID/DATASETUID/data/FILENAME
    url(r'^('+ORGANISMUID+r')/('+DATASETUID+r')/data(?:/([^/]*))?$', efpbrowser.views.data, name='data'),
    url(r'^('+ORGANISMUID+r')/('+DATASETUID+r')/data(?:/(output/[^/]*))?$', efpbrowser.views.data, name='dataoutput'),

]
