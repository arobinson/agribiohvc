
import os
import sys
from types import ModuleType

from django.shortcuts import render, redirect
from django.urls import reverse
import logging
from django.http.response import Http404, HttpResponse
from django.core.exceptions import ObjectDoesNotExist

try:
    from django.template.context_processors import csrf
except:
    from django.core.context_processors import csrf

from common.bl.messagemanager import MessageManager

from AgriBioHVC import settings
import common.models
from landing.views import _parentList, ExternalLinker

class Configuration(object):
    def __init__(self, values={}, download=False):
        '''Object that allows access to dict elements via attributes'''
        
        self.lookup = self._lookupDict
        self.set = self._setDict
        self.keys = []
        if type(values) == dict:
            self.values = values
            self.keys = values.keys()
        elif type(values) in (str, unicode):
            self.values = eval(values, {"__builtins__":None, 'False': False, 'True': True}, {})
            self.keys = self.values.keys()
        elif type(values) == ModuleType:
            self.values = values
            self.lookup = self._lookupModule
            self.set = self._setModule
            self.keys = dir(values)
        
        # update config for download quality
        multiplier = self['PRINT_IMG_MULTIPLIER']
        if download:
            # data source
            self['defaultDataSource'] += "_hr"
            # graph scale
            datasets = self['GRAPH_SCALE_UNIT'].keys()
            for ds in datasets:
                vals = []
                for s in self['GRAPH_SCALE_UNIT'][ds]:
                    vals.append((s[0], s[1]*multiplier))
                self['GRAPH_SCALE_UNIT'][ds] = vals
            # positions
            self['GENE_ID1_POS'] = self._multtuple(self['GENE_ID1_POS'], multiplier)
            self['GENE_ID2_POS'] = self._multtuple(self['GENE_ID2_POS'], multiplier)
            self['GENE_PROBESET1_POS'] = self._multtuple(self['GENE_PROBESET1_POS'], multiplier)
            self['GENE_PROBESET2_POS'] = self._multtuple(self['GENE_PROBESET2_POS'], multiplier)
            self['GENE_ALIAS1_POS'] = self._multtuple(self['GENE_ALIAS1_POS'], multiplier)
            self['GENE_ALIAS2_POS'] = self._multtuple(self['GENE_ALIAS2_POS'], multiplier)
            self['GENE_ORTHO1_POS'] = self._multtuple(self['GENE_ORTHO1_POS'], multiplier)
            
        self.keys.sort()
        
    def _multtuple(self, t, multiplier):
        return tuple(map(lambda v: int(v*multiplier), t))
            
            
    def _lookupDict(self, k):
        return self.values[k]
    def _lookupModule(self, k):
        return getattr(self.values, k)
    def _setDict(self, k, v):
        self.values[k] = v
    def _setModule(self, k, v):
        setattr(self.values, k, v)
    def __getattr__(self, k):
        return self.lookup(k)
    def __getitem__(self, k):
        return self.lookup(k)
#     def __setattr__(self, k, v):
#         return self.set(k, v)
    def __setitem__(self, k, v):
        return self.set(k, v)
    
    def __str__(self, *args, **kwargs):
        result = "Configuration("
        for k in self.keys:
            result += "\n %s = %s" % (k, self.lookup(k))
        result += ")"
        return result
        

def home(request, organismuid, datasetuid, collectionuid=None):
    '''Renders the eFP browser (home view) inside our template'''
    
    # load data objects
    mycollectionuid = collectionuid
    if not collectionuid:
        mycollectionuid = settings.DEFAULT_COLLECTION
    try:
        collection = common.models.Collection.objects.get(uid=mycollectionuid)
        organism = common.models.Species.objects.get(uid=organismuid, deleted__isnull=True)
        dataset = organism.dataset_set.get(uid=datasetuid)
    except ObjectDoesNotExist:
        raise Http404
    
    
    # make efp import-able
    efpDir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'efp/cgi-bin') #TODO: put efp code directory path in a setting
    if efpDir not in sys.path:
        sys.path.append(efpDir)
    
    # copy GET / POST vars
    VARS = {}
    for var in ("dataSource", "primaryGene", "secondaryGene", 
                "threshold", "orthoListOn", "ncbi_gi", 
                "threshold", "modeInput", "override", 
                "modeMask_low", "modeMask_stddev"):
        if var in request.POST:
            VARS[var] = request.POST.get(var)
        else:
            VARS[var] = request.GET.get(var, None)
    
    # create a dictionary from efpConfig module
    download = request.POST.get('qual', 'view') == 'down'

    CONF=Configuration(dataset.efpconfig, download)
    
#     logging.error(CONF)
    
    ## process request
    import efpBase
    result = efpBase.processRequest(VARS, CONF)
    
#     keys = result.keys()
#     keys.sort()
#     for k in keys:
#         print "- %s: %s" % (k, result[k])
        
    if download:
        # url(r'^('+ORGANISMUID+r')/('+DATASETUID+r')/data(?:/(output/[^/]*))?$', efpbrowser.views.data, name='dataoutput'),
#         return redirect('efp:dataoutput', organismuid, datasetuid, 'output/' + os.path.basename(result['imgFilename']['all']))

        filename = "%s-absolute-%s.png" % (datasetuid, result['primaryGene'],)
        if result['mode'] == "Relative":
            filename = "%s-relative-%s.png" % (datasetuid, result['primaryGene'], )
        elif result['mode'] == "Compare":
            filename = "%s-%s-compare-%s.png" % (datasetuid, result['primaryGene'], result['secondaryGene'])

        with open(result['imgFilename']['all'], 'r') as f:
            response = HttpResponse(f, content_type='image/png')
            response['Content-Disposition'] = 'attachment; filename="%s"' % filename
            return response
    else:
        # prepare response
        params = {
                  'request': request,
                  'messages': MessageManager.getMessages(request),
                  'group': 'efp',
                  'parents': _parentList(collection, not collectionuid,
                      ('%s datasets' % organism.name, reverse('datasetlist', kwargs={'collectionuid': collectionuid, 'organismuid': organism.uid, })),
                      (dataset.name, reverse('datasetdetails', kwargs={'collectionuid': collectionuid, 'organismuid': organism.uid, 'datasetuid': dataset.uid})),
                  ),
                  'page': settings.EFP_COMMON_NAME, 
                  'children': [ ],
                  'result': result,
                  'organism': organism,
                  'dataset': dataset,
                  'conf': CONF,
                  'settings': settings,
                  'title': settings.EFP_COMMON_NAME,
                  'collection': collection,
                  'extlink': ExternalLinker(dataset, result['primaryGene']),
                 }
        params.update(csrf(request))
        
        return render(request, "efp/home.html", params)


MIMEEXTS = {
    'js': 'text/javascript',
    'css': 'text/css',
    'png': 'image/png',
    'gif': 'image/gif',
    'jpg': 'image/jpeg',
    'html': 'text/html',
}

def static(request, filename):
    '''Obtain static files from disk'''
    
    ext = str(filename).split('.')[-1]
    
    if ext in MIMEEXTS:
        try:
            with open(os.path.join(settings.VIEWERSETTINGS['efp']['staticdirectory'], filename)) as f:
                return HttpResponse(f.read(), content_type=MIMEEXTS[ext])
        except:
            pass
        
    raise Http404
    
    
def data(request, organismuid, datasetuid, filename):
    '''Obtain data-set specific files (including generated ones)'''
    
    # load data objects
    organism = common.models.Species.objects.get(uid=organismuid, deleted__isnull=True)
    dataset = organism.dataset_set.get(uid=datasetuid, deleted__isnull=True)
    
    ext = str(filename).split('.')[-1]
    fullpath = os.path.join(settings.VIEWERSETTINGS['efp']['datadirectory'], dataset.uid, filename)
    
    if ext in MIMEEXTS:
        try:
            with open(fullpath) as f:
                return HttpResponse(f.read(), content_type=MIMEEXTS[ext])
        except:
            logging.exception("data")
            pass
        
    raise Http404

