'''
Created on 27Feb.,2017

@author: arobinson
'''

from django.template import loader, Context

from AgriBioHVC import settings
import logging

class EFPConfig(object):
    
    @classmethod
    def getConfig(cls, dataset):
        '''Constructs the config dictionary for a dataset'''
        
        try:
            return cls._eval(dataset.efpconfig)
        except:
            return None
    
    @classmethod
    def generateConfig(cls, dataset):
        '''Get the eFP Config for the given dataset'''
        
        CONF = {}
        
        efpsettings = settings.VIEWERSETTINGS['common'].copy()
        efpsettings.update(settings.VIEWERSETTINGS['efp'])
        
        # load defaults template
        t = loader.get_template('UPDATEABLE_EFPCONFIG_DEFAULT')
        code = t.render({'dataset': dataset, 'species': dataset.species, 'settings': efpsettings})
        
        
        # update result with defaults
        CONF.update(cls._getConf(code, CONF))
        
        # add generated scale unit
        CONF['GRAPH_SCALE_UNIT'] = {'efp': dataset._GRAPH_SCALE_UNIT}
        
        # update results with dataset overrides
        CONF.update(cls._getConf(dataset.efpconfigoverride, CONF))
        
#         print CONF
        
        dataset.efpconfig = str(CONF)
        dataset.save()
        
        return CONF


    @classmethod
    def _getConf(cls, code, CONF={}):
        '''Execute code and return CONF variable'''
        
        g = {'__builtins__': {}, 'False': False, 'True': True}
        l = {'CONF': CONF}
        
        exec(code, g, l)
        
        return l['CONF']
    
    @classmethod
    def _eval(cls, configstr):
        
        g = {'__builtins__': {}, 'False': False, 'True': True}
        l = {}
        
        return eval(configstr, g, l)
        
        