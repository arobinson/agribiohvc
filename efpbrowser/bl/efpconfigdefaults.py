'''
Created on Nov 24, 2009

@author: rtbreit {% load stringutils %}
'''

CONF = {}

# Read-only database config
CONF['DB_HOST'] = '{{settings.database.0}}'           # hostname of MySQL database server
CONF['DB_USER'] = '{{settings.runtimeuser.0}}'             # username for database access
CONF['DB_PASSWD'] = '{{settings.runtimeuser.1}}'          # password for database access

# Species annotations
CONF['DB_ANNO'] = '{{settings.database.2}}'  # database name for annotations lookup

# lookup table for gene annotation
CONF['DB_ANNO_TABLE'] = '{{species.uid}}_annotation' # SPECIES_annotation
CONF['DB_ANNO_GENEID_COL'] = 'loc' # geneid
#DB_ORTHO_LOOKUP_TABLE = 'rice_maize_lookup'
#DB_ORTHO_GENEID_COL = 'rice_id'
# lookup table for probeset id
CONF['DB_LOOKUP_TABLE'] = '{{species.uid}}_lookup' # SPECIES_lookup
CONF['DB_LOOKUP_GENEID_COL'] = 'loc' # geneid

#DB_ORTHO = 'ortholog_db'        # ortholog DB

## path definitions (AR: 19/01/2017) ##
# Directory containing static files 
CONF['STATIC_FILES'] = '{{settings.staticdirectory}}'
CONF['STATIC_FILES_WEB'] = '{% url "efp:static" %}'

# Directory to place output files
CONF['OUTPUT_FILES'] = '{{settings.datadirectory}}/{{dataset.uid}}/output'
CONF['OUTPUT_FILES_WEB'] = '{% url "efp:data" species.uid dataset.uid "output" %}'

# Directory that contains data-set data files (i.e. images)
CONF['dataDir'] = '{{settings.datadirectory}}/{{dataset.uid}}'
CONF['dataDirWeb'] = '{% url "efp:data" species.uid dataset.uid %}'
## end paths (AR) ##

# lookup tables for ncbi ids
CONF['DB_NCBI_GENE_TABLE'] = None
CONF['DB_NCBI_PROT_TABLE'] = None
CONF['DB_NCBI_GENEID_COL'] = None

#Check Y-AXIS message
CONF['Y_AXIS'] = {}
CONF['Y_AXIS']['{{dataset.uid}}'] = "GCOS expression signal (TGT=100, Bkg=20)"

#Check if lookup exists
CONF['LOOKUP'] = {}
CONF['LOOKUP']['{{dataset.uid}}'] = "1"

# initial gene ids when start page is loaded
CONF['GENE_ID_DEFAULT1'] = ''
CONF['GENE_ID_DEFAULT2'] = ''

# the little graph on the tga image has a scale
# such that 1 unit is x pixels for different ranges on the x-axis of the graph
# the GRAPH_SCAL_UNIT consists of value pairs: upper end of range and scale unit
# so ((1000, 0.031), (10000, 0.003222), (1000000, 0.00031)) means:
# use 0.031 as scale unit for 0 < signal < 1000
# use 0.003222 as scale unit for 1000 < signal < 10000
# use 0.00031 as scale unit for 10000 < signal < 1000000
# see also efp.drawImage()
CONF['GRAPH_SCALE_UNIT'] = {}
# the default values are used if for the given data source no special values are defined
# GRAPH_SCALE_UNIT["default"] = ((0.06, 16) (0.006, 73), (0.0006, 130))
CONF['GRAPH_SCALE_UNIT']["{{dataset.uid}}"] = [(1000, 0.06), (10000, 0.006), (100000, 0.0006)]

# define additional header text for individual data sources
# you can use key 'default' for each not individually defined
CONF['datasourceHeader'] = {}
CONF['datasourceHeader']['default'] = ''

# define additional footer text for individual data sources
# you can use key 'default' for each not individually defined
CONF['datasourceFooter'] = {}
CONF['datasourceFooter']['default'] = ''

# regular expression for check of gene id input
CONF['inputRegEx'] = "(LOC_Os[0-9]{2}g[0-9]{5})|(((AFFX|AFFX-Os)(-|_)(Ubiquitin|Actin|Cyph|Gapdh|BioB|BioC|BioDn|CreX|DapX|LysX|PheX|ThrX|TrpnX|ef1a|gapdh)(-|_)(3|5|M)_(at|x_at|s_at))|(AFFX-OS-(18SrRNA|25SrRNA|5.8SrRNA)_(s_at|at))|(AFFX-Mgr-(actin|ef1a|gapdh)-(3|5|M))_(at|x_at|s_at)|(AFFX-r2-Tag(A|B|C|D|E|F|G|H)_at)|(AFFX-r2-Tag(IN|I|J|O|Q)-(3|5|M)_at)|((AFFX|AFFX-Os)-r2-(Bs|Ec|P1)-(dap|lys|phe|thr|bioB|bioC|bioD|cre)-(3|5|M)(_|_x_|_s_)at)|(Os|OsAffx)\.[0-9]{1,5}\.[0-9]{1}\.(S1|A1|S2)_(at|x_at|s_at|a_at))"

# default thresholds
CONF['minThreshold_Compare'] = 0.6  # Minimum color threshold for comparison is 0.6, giving us [-0.6, 0.6] on the color legend
CONF['minThreshold_Relative'] = 0.6 # Minimum color threshold for median is 0.6, giving us [-0.6, 0.6] on the color legend ~ 1.5-Fold
CONF['minThreshold_Absolute'] = 10  # Minimum color threshold for max is 10, giving us [0, 10] on the color legend

# coordinates where to write gene id, probeset id and gene alias into image
CONF['GENE_ID1_POS'] = (0, 30)
CONF['GENE_ID2_POS'] = (0, 45)
CONF['GENE_PROBESET1_POS'] = (150, 30)
CONF['GENE_PROBESET2_POS'] = (150, 45)
CONF['GENE_ALIAS1_POS'] = (0, 0)
CONF['GENE_ALIAS2_POS'] = (0, 0)
CONF['GENE_ORTHO1_POS'] = (0,285)
CONF['defaultDataSource'] = '{{dataset.uid}}'

CONF['dbGroupDefault'] = 'group1'
# list of datasources in same group to find max signal 
CONF['groupDatasource'] = {}
CONF['groupDatasource']["group1"] = ['{{dataset.uid}}']

# mapping of xml files to show datasource name
CONF['groupDatasourceName'] = {}
CONF['groupDatasourceName']["group1"] = {'{{dataset.uid}}':'{{dataset.uid}}'}

# ortholog configuration
# list of species where orthologs should be tried to retrieve (names must be the same as in ortholog db)
CONF['ortholog_species'] = () #'POP', 'TAIR8', 'MEDV3', 'RICE', 'BARLEY', 'SOYBEAN'

CONF['efpLink'] = {}
# CONF['efpLink']['{{species.uid|upper}}'] = None
# CONF['efpLink']['POP'] = "http://bar.utoronto.ca/efp_poplar/cgi-bin/efpWeb.cgi?dataSource=Poplar&primaryGene=%s&modeInput=Absolute"

CONF['species'] = '{{species.uid|upper}}'

CONF['spec_names'] = {}
# CONF['spec_names']['POP'] = 'Poplar'
CONF['spec_names']['{{species.uid|upper}}'] = '{{dataset.uid|title}}'
