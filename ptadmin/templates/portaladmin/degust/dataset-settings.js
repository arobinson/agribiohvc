{
	"code" : "{{dataset.uid}}",
	"remote_addr" : "192.168.56.1",
	"created" : "2017-01-05 23:34:19.008003 UTC",
	"user_settings" : {
		"fdr_column" : "",
		"analyze_server_side" : true,
		"avg_column" : "",
		"primary_name" : "",
		"fc_columns" : [],
		"min_counts" : 200,
		"name":"{{dataset.name}}",
		"hide_columns" : [],
		"init_select" : [ "24C", "24R", "48R", "48D" ],
		"skip" : 0,
		"csv_format" : false,
		"replicates" : [ [ "24C", [ "24C1_R1", "24C2_R1", "24C3_R1" ] ],
				[ "24R", [ "24R1_R1", "24R2_R1", "24R3_R1" ] ],
				[ "48R", [ "48R1_R1", "48R2_R1", "48R3_R1" ] ],
				[ "48D", [ "48D1_R1", "48D2_R1", "48D3_R1" ] ] ],
		"link_url" : "http://localhost:8000/gene/croc1/%s",
		"link_column" : "seqname",
		"info_columns" : [ "seqname", "Trinity.length" ]
	},
	"locked" : false
}
