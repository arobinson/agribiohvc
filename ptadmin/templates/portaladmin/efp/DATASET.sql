CREATE TABLE `{{dataset.uid}}_data` (
  `sample_id` int(10) unsigned NOT NULL DEFAULT '0',
  `proj_id` varchar(15) NOT NULL DEFAULT '0',
  `sample_file_name` tinytext,
  `data_probeset_id` varchar(60) NOT NULL,
  `data_signal` float DEFAULT '0',
  `data_call` tinytext,
  `data_p_val` float DEFAULT '0',
  `data_bot_id` varchar(26) NOT NULL,
  `sample_tissue` text NOT NULL,
  KEY `data_probeset_id` (`data_probeset_id`,`data_bot_id`,`data_signal`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1