{
    # database config
    'DB_HOST': 'localhost', 
    'DB_USER': 'db_user', # (read-only for security)
    'DB_PASSWD': 'db_user1', 
    
    'DB_DATA_TABLE': 'sample_data',     # database table which contains expression data
    
    'DB_LOOKUP_TABLE': '', 
    'LOOKUP': {'': '0'},
    
    'DB_NCBI_PROT_TABLE': None, 
    'ortholog_species': ('POP', 'TAIR8', 'MEDV3', 'RICE', 'BARLEY', 'SOYBEAN'), 
    'DB_NCBI_GENE_TABLE': None, 
    'OUTPUT_FILES_WEB': '../../static/efpoutput', 
    'STATIC_FILES_WEB': '../../static/efp', 
    'DB_ANNO_GENEID_COL': 'loc', 
    'GENE_PROBESET1_POS': (150, 30), 
    'dataDir': '/var/www/html/efp_rice/static/data_rice', 
    'Y_AXIS': {'rice_mas': 'GCOS expression signal (TGT=100, Bkg=20)'}, 
    'species': 'RICE', 
    'efpLink': {
        'TAIR8': 'http://bar.utoronto.ca/efp_arabidopsis/cgi-bin/efpWeb.cgi?dataSource=Developmental_Map&primaryGene=%s&modeInput=Absolute', 
        'MEDV3': 'http://bar.utoronto.ca/efp_medicago/cgi-bin/efpWeb.cgi?dataSource=medicago_mas&primaryGene=%s&modeInput=Absolute', 
        'POP': 'http://bar.utoronto.ca/efp_poplar/cgi-bin/efpWeb.cgi?dataSource=Poplar&primaryGene=%s&modeInput=Absolute', 
        'BARLEY': 'http://bar.utoronto.ca/efp_barley/cgi-bin/efpWeb.cgi?dataSource=barley_mas&primaryGene=%s&modeInput=Absolute', 
        'SOYBEAN': 'http://bar.utoronto.ca/efp_soybean/cgi-bin/efpWeb.cgi?dataSource=soybean&primaryGene=%s&modeInput=Absolute', 
        'RICE': 'http://bar.utoronto.ca/efp_rice/cgi-bin/efpWeb.cgi?dataSource=rice_mas&primaryGene=%s&modeInput=Absolute'
    }, 
    'minThreshold_Compare': 0.6, 
    'GENE_PROBESET2_POS': (150, 45), 
    'GENE_ALIAS2_POS': (0, 0), 
    'DB_ANNO_TABLE': 'loc_annotation', 
    'GENE_ID2_POS': (0, 45), 
    'minThreshold_Relative': 0.6, 
    'DB_NCBI_GENEID_COL': None,  
    'OUTPUT_FILES': '{{settings.datadirectory}}/{{dataset.uid}}/tmp', 
    'spec_names': {
        'TAIR8': 'Arabidopsis', 
        'MEDV3': 'Medicago', 
        'POP': 'Poplar', 
        'BARLEY': 'Barley', 
        'SOYBEAN': 'Soybean', 
        'RICE': 'Rice'
    }, 
    'datasourceHeader': {'default': ''}, 
    'datasourceFooter': {'default': ''}, 
    'inputRegEx': '(XM_[0-9]{9}\\.[0-9])|(LOC_Os[0-9]{2}g[0-9]{5})|(((AFFX|AFFX-Os)(-|_)(Ubiquitin|Actin|Cyph|Gapdh|BioB|BioC|BioDn|CreX|DapX|LysX|PheX|ThrX|TrpnX|ef1a|gapdh)(-|_)(3|5|M)_(at|x_at|s_at))|(AFFX-OS-(18SrRNA|25SrRNA|5.8SrRNA)_(s_at|at))|(AFFX-Mgr-(actin|ef1a|gapdh)-(3|5|M))_(at|x_at|s_at)|(AFFX-r2-Tag(A|B|C|D|E|F|G|H)_at)|(AFFX-r2-Tag(IN|I|J|O|Q)-(3|5|M)_at)|((AFFX|AFFX-Os)-r2-(Bs|Ec|P1)-(dap|lys|phe|thr|bioB|bioC|bioD|cre)-(3|5|M)(_|_x_|_s_)at)|(Os|OsAffx)\\.[0-9]{1,5}\\.[0-9]{1}\\.(S1|A1|S2)_(at|x_at|s_at|a_at))', 
    'dbGroupDefault': 'group1', 
    'GENE_ID1_POS': (0, 30), 
    'DB_ANNO': 'rice_annotations_lookup', 
    'defaultDataSource': 'rice_mas', 
    'GENE_ORTHO1_POS': (0, 285), 
    'STATIC_FILES': '/var/www/html/efp_rice/static', 
    'minThreshold_Absolute': 10, 
    'GENE_ID_DEFAULT1': 'LOC_Os01g01080', 
    'DB_LOOKUP_GENEID_COL': 'loc', 
    'GENE_ID_DEFAULT2': 'LOC_Os06g10770', 
    'groupDatasource': {'group1': ['rice_mas']}, 
    'GENE_ALIAS1_POS': (0, 0), 
    'GRAPH_SCALE_UNIT': {'rice_mas': [(1000, 0.06), (10000, 0.006), (100000, 0.0006)]}, 
    'groupDatasourceName': {'group1': {'rice_mas': 'rice_mas'}}, 
    'dataDirWeb': '../../static/efp/data_rice'
}