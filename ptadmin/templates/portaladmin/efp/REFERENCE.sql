CREATE TABLE `{{reference.uid}}_lookup` (
  `probeset` varchar(60) NOT NULL,
  `loc` varchar(30) NOT NULL,
  `date` date NOT NULL,
  KEY `date` (`date`),
  KEY `loc_probeset` (`loc`,`probeset`),
  KEY `probeset_loc` (`probeset`,`loc`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `{{reference.uid}}_annotation` (
  `id` int NOT NULL AUTO_INCREMENT,
  `loc` varchar(30) NOT NULL,
  `annotation` longtext NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `date` (`date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
