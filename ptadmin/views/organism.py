'''
Created on 7Apr.,2017

@author: arobinson
'''
import logging

from django.contrib.auth.decorators import permission_required
from django.http.response import Http404
from django.shortcuts import render, redirect
try:
    from django.template.context_processors import csrf
except:
    from django.core.context_processors import csrf

import common
import ptadmin.bl.datamanager
import ptadmin.forms
from AgriBioHVC import settings
from common.bl.messagemanager import MessageManager

import background.bl

@permission_required('ptadmin.view_portal')
def listOrganism(request):
    '''Displays a list of all organism'''
        
    params = {
              'request': request, 
              'messages': MessageManager.getMessages(request),
              'group': 'ptadmin', 
              'page': 'listorganism', 
              'organisms': common.models.Species.objects.filter(deleted__isnull=True).order_by('name'),
              'settings': settings,
              'title': 'Organisms',
             }
    params.update(csrf(request))

    return render(request, 'portaladmin/organism-list.html', params)
    

@permission_required('ptadmin.view_portal')
def detailOrganism(request, organismuid):
    '''Displays details of organism (and its references)'''
    
    try:
        organism = common.models.Species.objects.get(uid=organismuid, deleted__isnull=True)
    except:
        raise Http404
    
    params = {
              'request': request, 
              'messages': MessageManager.getMessages(request),
              'group': 'ptadmin', 
              'page': 'detailorganism', 
              'organism': organism,
              'settings': settings,
              'backgroundtasks': background.bl.runningJobs,
              'title': '%s details' % organism.name,
             }
    params.update(csrf(request))

    return render(request, 'portaladmin/organism-details.html', params)

@permission_required('ptadmin.view_portal')
def addOrganism(request):
    '''
    Form (render and process) to add new Organism to the database
    '''
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = ptadmin.forms.SpeciesForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            
            # create Species
            try:
                species = common.models.Species(**form.cleaned_data)
                species.save()
                
                MessageManager.addSuccess("Successfully added new organism '%s'" % species.name, request)
                
                return redirect('ptadmin:detailorganism', species.uid)
            except:
                MessageManager.addError("Failed to add new organism", request)

        # save state and redirect
        request.session['form_data'] = form.dirty_data(request.POST)
        return redirect('ptadmin:organismadd')
        
    # if a GET (or any other method) we'll create the form (with pre-filled data if available)
    elif 'form_data' in request.session and '__form__' in request.session['form_data'] and request.session['form_data']['__form__'] == "SpeciesForm":
        request.session['form_data'].pop('__form__')
        form = ptadmin.forms.SpeciesForm(request.session['form_data'], bootstrap_style='common/form/bootstrap-form2-6.html')
    else:
        form = ptadmin.forms.SpeciesForm(bootstrap_style='common/form/bootstrap-form2-6.html')
        
    params = {
              'request': request, 
              'messages': MessageManager.getMessages(request),
              'group': 'ptadmin', 
              'page': 'addorganism', 
              'species': common.models.Species.objects.filter(deleted__isnull=True),
              'datasets': common.models.Dataset.objects.filter(deleted__isnull=True),
              'form': form,
              'settings': settings,
              'title': 'Add organism',
             }
    params.update(csrf(request))

    return render(request, 'portaladmin/organism-add.html', params)


@permission_required('ptadmin.view_portal')
def addReference(request, organismuid):
    '''Form (render and process) to create new reference'''
    
    try:
        organism = common.models.Species.objects.get(uid=organismuid, deleted__isnull=True)
    except:
        raise Http404
    
    if request.method == 'POST':
        form = ptadmin.forms.ReferenceForm(request.POST, request.FILES)
        if form.is_valid():
            
            try:
                data = form.cleaned_data
#                 dict(data).pop('gfffile')
                reference = common.models.Reference(**data)
                reference.species = organism
                reference.save()
                
#                 status, reason = ptadmin.bl.datamanager.DataManager.saveReferenceUploads(reference, request.FILES)
#                 if status:
                MessageManager.addSuccess("Successfully created new reference '%s'" % reference.name, request)
                return redirect('ptadmin:referenceupload', organism.uid, reference.uid)
#                 MessageManager.addError("Failed to create new reference (%s)" % reason, request)
            except:
                logging.exception('Save reference failed')
                MessageManager.addError("Failed to create new reference", request)
                
        request.session['form_data'] = form.dirty_data(request.POST)
        return redirect('ptadmin:referenceadd', organism.uid)
    
    elif 'form_data' in request.session and '__form__' in request.session['form_data'] and request.session['form_data']['__form__'] == "ReferenceForm":
        request.session['form_data'].pop('__form__')
        form = ptadmin.forms.ReferenceForm(request.session['form_data'])
        request.session.pop('form_data')
    else:
        form = ptadmin.forms.ReferenceForm()
    return render(request, 'portaladmin/reference-add.html', {'form': form, 'messages': MessageManager.getMessages(request),
              'settings': settings,
              'title': 'Add reference',
              'organism': organism,})
    
    
@permission_required('ptadmin.view_portal')
def uploadReferenceFeatures(request, organismuid, referenceuid):
    '''Form (render and process) to upload features file for reference'''
    try:
        organism = common.models.Species.objects.get(uid=organismuid, deleted__isnull=True)
        reference = organism.reference_set.get(uid=referenceuid, deleted__isnull=True)
    except:
        raise Http404
    
    if request.method == 'POST':
        form = ptadmin.forms.ReferenceUploadForm(request.POST, request.FILES)
        if form.is_valid():
            
            status, reason = ptadmin.bl.datamanager.DataManager.saveReferenceUploads(reference, request.FILES)
            if status:
                MessageManager.addSuccess("Successfully feature file for '%s'" % reference.name, request)
                return redirect('ptadmin:detailorganism', organism.uid)
            MessageManager.addError("Failed to upload files (%s)" % reason, request)
            
    elif 'form_data' in request.session and '__form__' in request.session['form_data'] and request.session['form_data']['__form__'] == "ReferenceUploadForm":
        request.session['form_data'].pop('__form__')
        form = ptadmin.forms.ReferenceUploadForm(request.session['form_data'])
        request.session.pop('form_data')
    else:
        form = ptadmin.forms.ReferenceUploadForm()
    return render(request, 'portaladmin/reference-upload.html', {
              'form': form, 
              'organism': organism, 
              'reference': reference, 
              'messages': MessageManager.getMessages(request),
              'settings': settings,
              'title': 'Upload reference features',
            })
    

@permission_required('ptadmin.view_portal')
def deployreferenceefp(request, organismuid, referenceuid):
    '''Attempts to deploy the reference to eFP database'''
    
    try:
        organism = common.models.Species.objects.get(uid=organismuid, deleted__isnull=True)
        reference = organism.reference_set.get(uid=referenceuid, deleted__isnull=True)
    except:
        raise Http404
    
    status, reason = ptadmin.bl.datamanager.DataManager.deployReferenceEFP(reference, override=True)
    if status:
        MessageManager.addSuccess("Successfully deployed '%s' to eFP Browser" % reference.name, request)
    else:
        MessageManager.addError("Failed to deploy '%s' to eFP Browser" % reference.name, request)
    return redirect('ptadmin:detailorganism', organism.uid)


@permission_required('ptadmin.view_portal')
def deployreferencelanding(request, organismuid, referenceuid):
    '''Deploy the reference to landing portal'''
    
    try:
        organism = common.models.Species.objects.get(uid=organismuid, deleted__isnull=True)
        reference = organism.reference_set.get(uid=referenceuid, deleted__isnull=True)
    except:
        raise Http404
    
    status, reason = ptadmin.bl.datamanager.DataManager.deployReferenceLanding(reference, request.session.session_key, override=True)
#     if status:
#         MessageManager.addSuccess("Successfully deployed '%s' to Landing Portal" % reference.name, request)
#     else:
#         MessageManager.addError("Failed to deploy '%s' to Landing Portal" % reference.name, request)
    return redirect('ptadmin:detailorganism', organism.uid)
    
    



