'''
Created on 11Apr.,2017

@author: arobinson
'''

from django.shortcuts import render, redirect
from django.utils.translation import ugettext as _
from django.contrib.auth.decorators import permission_required
from django.http.response import Http404
try:
    from django.template.context_processors import csrf
except:
    from django.core.context_processors import csrf

from ptadmin.bl.usergroupmanager import UserGroupManager
from AgriBioHVC import settings
from common.bl.messagemanager import MessageManager
import ptadmin.forms
import common

@permission_required('ptadmin.view_portal')
def listgroup(request):
    '''Shows list of all groups and users in each'''
    
    groups = UserGroupManager.getGroups()
    
    params = {
              'request': request, 
              'messages': MessageManager.getMessages(request),
              'group': 'ptadmin', 
              'page': 'listorganism', 
              'groups': groups,
              'settings': settings,
              'title': 'Groups',
             }
    params.update(csrf(request))

    return render(request, 'portaladmin/group-list.html', params)
    
@permission_required('ptadmin.view_portal')
def newgroup(request):
    '''Form to add new group'''
    
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = ptadmin.forms.GroupForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            
            # create Species
            status = UserGroupManager.createGroup(**form.cleaned_data)
            if status == True:
                MessageManager.addSuccess(_("Successfully added new group %(group)s") % {'group': form.cleaned_data['name']}, request)
                return redirect('ptadmin:listgroup')
            
            MessageManager.addError(_("Failed to add new group (%(reason)s)") % {'reason': _(status)}, request)

        # save state and redirect
        request.session['form_data'] = form.dirty_data(request.POST)
        return redirect('ptadmin:newgroup')
        
    # if a GET (or any other method) we'll create the form (with pre-filled data if available)
    elif 'form_data' in request.session and '__form__' in request.session['form_data'] and request.session['form_data']['__form__'] == "GroupForm":
        request.session['form_data'].pop('__form__')
        form = ptadmin.forms.GroupForm(request.session['form_data'], bootstrap_style='common/form/bootstrap-form2-6.html')
    else:
        form = ptadmin.forms.GroupForm(bootstrap_style='common/form/bootstrap-form2-6.html')
        
    params = {
              'request': request, 
              'messages': MessageManager.getMessages(request),
              'group': 'ptadmin', 
              'page': 'addgroup', 
              'form': form,
              'settings': settings,
              'title': 'New Group',
             }
    params.update(csrf(request))

    return render(request, 'portaladmin/group-new.html', params)
    
@permission_required('ptadmin.view_portal')
def newuser(request):
    '''Form to add new user'''
    
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = ptadmin.forms.UserForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            
            # create Species
            status = UserGroupManager.createUser(**form.cleaned_data)
            if status == True:
                MessageManager.addSuccess(_("Successfully added new user %(user)s") % {'user': form.cleaned_data['username']}, request)
                return redirect('ptadmin:listgroup')
            
            MessageManager.addError(_("Failed to add new user (%(reason)s)") % {'reason': _(status)}, request)

        # save state and redirect
        request.session['form_data'] = form.dirty_data(request.POST)
        return redirect('ptadmin:newuser')
        
    # if a GET (or any other method) we'll create the form (with pre-filled data if available)
    elif 'form_data' in request.session and '__form__' in request.session['form_data'] and request.session['form_data']['__form__'] == "UserForm":
        request.session['form_data'].pop('__form__')
        form = ptadmin.forms.UserForm(request.session['form_data'], bootstrap_style='common/form/bootstrap-form2-6.html')
    else:
        form = ptadmin.forms.UserForm(bootstrap_style='common/form/bootstrap-form2-6.html')
        
    params = {
              'request': request, 
              'messages': MessageManager.getMessages(request),
              'group': 'ptadmin', 
              'page': 'adduser', 
              'form': form,
              'settings': settings,
              'title': 'New User',
             }
    params.update(csrf(request))

    return render(request, 'portaladmin/user-new.html', params)
    
    
@permission_required('ptadmin.view_portal')
def addusergroup(request, groupid):
    '''Add a user to group'''
    
    users = UserGroupManager.getUsers()
    
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = ptadmin.forms.SelectUserForm(users, request.POST)
        # check whether it's valid:
        if form.is_valid():
            
            # create Species
            status = UserGroupManager.addUsersToGroup(groupid, form.cleaned_data['users'])
            if status == True:
                MessageManager.addSuccess(_("Successfully added new users to group"), request)
                return redirect('ptadmin:listgroup')
            
            MessageManager.addError(_("Failed to add users to group (%(reason)s)") % {'reason': _(status)}, request)

        # save state and redirect
        request.session['form_data'] = form.dirty_data(request.POST)
        return redirect('ptadmin:addusergroup', groupid)
        
    # if a GET (or any other method) we'll create the form (with pre-filled data if available)
    elif 'form_data' in request.session and '__form__' in request.session['form_data'] and request.session['form_data']['__form__'] == "SelectUserForm":
        request.session['form_data'].pop('__form__')
        form = ptadmin.forms.SelectUserForm(users, request.session['form_data'])
    else:
        form = ptadmin.forms.SelectUserForm(users)
    
    try:
        group = common.models.Group.objects.get(id=groupid)
    except:
        raise Http404
    
    params = {
              'request': request, 
              'messages': MessageManager.getMessages(request),
              'group': 'ptadmin', 
              'page': 'selectusers', 
              'form': form,
              'settings': settings,
              'groupobj': group,
              'title': 'Select User(s)',
             }
    params.update(csrf(request))

    return render(request, 'portaladmin/user-addgroup.html', params)
    
@permission_required('ptadmin.view_portal')
def removeusergroup(request, groupid, userid):
    '''Remove a user from group'''
    
    status = UserGroupManager.removeUserFromGroup(groupid, userid)
    if status == True:
        MessageManager.addSuccess(_("Successfully removed user from group"), request)
    else:
        MessageManager.addError(_("Failed to remove new user from group (%(reason)s)") % {'reason': _(status)}, request)
        
    return redirect('ptadmin:listgroup')
    
