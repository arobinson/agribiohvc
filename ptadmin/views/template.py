'''
Created on 7Apr.,2017

@author: arobinson
'''

from django.contrib.auth.decorators import permission_required
from django.shortcuts import render, redirect

from common.bl.messagemanager import MessageManager
from ptadmin.bl import templatemanager


@permission_required('ptadmin.view_portal')
def updateTemplate(request):
    '''Downloads the template and saves to database'''
    
    if templatemanager.TemplateManager.updateTemplate('ltu'):
        MessageManager.addSuccess("Successfully downloaded and extracted template", request)
    else:
        MessageManager.addError("Failed to download and extract template", request)
        
    request.session['data'] = None
    return redirect('ptadmin:viewTemplate')



@permission_required('ptadmin.view_portal')
def viewTemplate(request):
    '''Shows template operations'''
    
    template = templatemanager.TemplateManager.getTemplate('ltu', True)
    
    
    params = {
        'messages': MessageManager.getMessages(request),
        'template': template,
    }
    
    return render(request, 'portaladmin/template-view.html', params)


