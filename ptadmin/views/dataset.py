import logging

from django.shortcuts import render, redirect
from django.contrib.auth.decorators import permission_required
from django.http.response import Http404, HttpResponse
from django.forms.models import modelformset_factory
import django.utils
from django.forms.formsets import formset_factory
try:
    from django.template.context_processors import csrf
except:
    from django.core.context_processors import csrf

import common
import ptadmin.bl.datamanager
import ptadmin.forms
from AgriBioHVC import settings
from common.bl.messagemanager import MessageManager

# Create your views here.

@permission_required('ptadmin.view_portal')
def home(request):
    '''Renders Admin Portal homepage'''
    
    # prepare response
    params = {
              'request': request, 
              'messages': MessageManager.getMessages(request),
              'group': 'ptadmin', 
              'page': 'home', 
              'species': common.models.Species.objects.filter(deleted__isnull=True).order_by('name'),
              'datasets': common.models.Dataset.objects.filter(deleted__isnull=True),
              'settings': settings,
              'title': 'Portal administration',
             }
    params.update(csrf(request))
    
    return render(request, "portaladmin/home.html", params)
    


@permission_required('ptadmin.view_portal')
def list(request):
    '''Renders List of datasets'''
    
    # prepare response
    params = {
              'request': request, 
              'messages': MessageManager.getMessages(request),
              'group': 'ptadmin', 
              'page': 'home', 
#               'species': common.models.Species.objects.filter(deleted__isnull=True).order_by('name'),
              'datasets': common.models.Dataset.objects.filter(deleted__isnull=True),
              'settings': settings,
              'title': 'Portal administration',
             }
    params.update(csrf(request))
    
    return render(request, "portaladmin/dataset-list.html", params)



@permission_required('ptadmin.view_portal')
def newdataset(request):
    '''Form (render and process) to create new dataset'''
    
    if request.method == 'POST':
        form = ptadmin.forms.DatasetBasicForm(request.POST)
        if form.is_valid():
            
            try:
                dataset = common.models.Dataset(**form.cleaned_data)
                dataset.save()
                
                MessageManager.addSuccess("Successfully created new data-set '%s'" % dataset.name, request)
                return redirect('ptadmin:uploaddataset', dataset.uid)
            except:
                logging.exception('Save dataset failed')
                MessageManager.addError("Failed to create new data-set", request)
                
        request.session['form_data'] = form.dirty_data(request.POST)
        return redirect('ptadmin:newdataset')
    
    elif 'form_data' in request.session and '__form__' in request.session['form_data'] and request.session['form_data']['__form__'] == "DatasetBasicForm":
        request.session['form_data'].pop('__form__')
        form = ptadmin.forms.DatasetBasicForm(request.session['form_data'], bootstrap_style='common/form/bootstrap-form2-6.html')
        request.session.pop('form_data')
    else:
        form = ptadmin.forms.DatasetBasicForm(bootstrap_style = 'common/form/bootstrap-form2-6.html')
    return render(request, 'portaladmin/dataset-new.html', {'form': form, 'messages': MessageManager.getMessages(request),
              'settings': settings,
              'title': 'New data-set',})




@permission_required('ptadmin.view_portal')
def upload(request, datasetuid):
    '''
    Form (render and process) to upload files required for dataset i.e. counts file and efp image
    '''
    try:
        dataset = common.models.Dataset.objects.get(uid=datasetuid, deleted__isnull=True)
    except:
        raise Http404
    
    if request.method == 'POST':
        form = ptadmin.forms.DataUploadForm(request.POST, request.FILES)
        if form.is_valid():
            
            dataset.countsformat = form.cleaned_data['countsformat']
            dataset.efpprintimgmultiplier = form.cleaned_data['efpprintimgmultiplier']
            dataset.save()
            
            status, reason = ptadmin.bl.datamanager.DataManager.saveUploads(dataset, request.FILES)
            if status:
                MessageManager.addSuccess("Successfully uploaded files '%s'" % dataset.name, request)
                return redirect('ptadmin:editdataset', dataset.uid)
            MessageManager.addError("Failed to upload files (%s)" % reason, request)
            
    elif 'form_data' in request.session and '__form__' in request.session['form_data'] and request.session['form_data']['__form__'] == "DataUploadForm":
        request.session['form_data'].pop('__form__')
        form = ptadmin.forms.DataUploadForm(request.session['form_data'], bootstrap_style='common/form/bootstrap-form2-6.html')
        request.session.pop('form_data')
    else:
        form = ptadmin.forms.DataUploadForm(bootstrap_style='common/form/bootstrap-form2-6.html')
    return render(request, 'portaladmin/dataset-upload.html', {'form': form, 'dataset': dataset, 'messages': MessageManager.getMessages(request),
              'settings': settings,
              'title': 'Upload data-set',})
    
@permission_required('ptadmin.view_portal')
def efpimage(request, datasetuid):
    '''
    Downloads efpimage
    '''
    try:
        dataset = common.models.Dataset.objects.get(uid=datasetuid, deleted__isnull=True)
    except:
        raise Http404
    
    imgdata, mimetype = ptadmin.bl.datamanager.DataManager.getEFPImage(dataset)
    if mimetype:
        return HttpResponse(imgdata, content_type=mimetype)

    
@permission_required('ptadmin.view_portal')
def edit(request, datasetuid):
    '''
    Form (render and process) to edit dataset configuration
    '''
    try:
        dataset = common.models.Dataset.objects.get(uid=datasetuid, deleted__isnull=True)
    except:
        raise Http404
    
    datasetdetails = ptadmin.bl.datamanager.DataManager.getDataDetails(dataset)
#     SampleFormSet = formset_factory(ptadmin.forms.SampleForm, 
#                                     formset=ptadmin.formsets.BaseSampleFormSet,
#                                     can_delete=True,
#                                     can_order=True,
#                                     extra=0,
#                                    )
    SampleFormSet = modelformset_factory(common.models.Sample, 
                                         form=ptadmin.forms.SampleModelForm,
                                         can_delete=True,
                                         can_order=True,
                                         extra=0,
                                         fields=[
                                              'name',
                                              'is_control',
                                              'efp_colour',
                                              'efp_coords',
                                              'replicates',
                                              'description',
                                             ],
                                        )
    LinkFormSet = formset_factory(ptadmin.forms.EFPLinkForm, 
                                     can_delete=True,
                                     extra=0,
                                    )
#     for f in dir(SampleFormSet):
#         logging.error(" - %s: %s" % (f, getattr(SampleFormSet, f)))
    
    if request.method == 'POST':
#         logging.error("POST")
        form = ptadmin.forms.DatasetEditForm(datasetdetails['columns'], 
                                             request.POST, 
                                             instance=dataset)
        formset = SampleFormSet(request.POST,
                                form_kwargs={'columns': datasetdetails['columns']}, prefix='sample')
        linkformset = LinkFormSet(request.POST, prefix='link')
        
#         for k,v in request.POST.items():
#             logging.error("- %s: %s"%(k,v))
        
        if form.is_valid() and formset.is_valid() and linkformset.is_valid():
            
#             dataset.sampleconfig = formset.cleaned_data
            efplinks = []
            for f in linkformset:
                l = f.cleaned_data
                if 'DELETE' not in l or not l['DELETE']:
                    efplinks.append(l)
            dataset.efplinks = efplinks
            form.save()
            
            formset.save(commit=False)
            for samp,_ in formset.changed_objects:
#                 logging.error('Changed: %s' % samp)
                samp.save()
            for samp in formset.deleted_objects:
#                 logging.error('Deleted: %s' % samp)
                samp.deleted = django.utils.timezone.now()
                samp.save()
            for samp in formset.new_objects:
#                 logging.error('New: %s' % samp)
                samp.dataset = dataset
                samp.save()
            
            
            MessageManager.addSuccess("Successfully saved data-set '%s'" % dataset.name, request)
            return redirect('ptadmin:listdataset')
        elif not form.is_valid():
            MessageManager.addError("Validation Error (Form)", request)
            logging.error(form.errors)
        elif not formset.is_valid():
            MessageManager.addError("Validation Error (Samples)", request)
            logging.error(formset.errors)
        elif not linkformset.is_valid():
            MessageManager.addError("Validation Error (Links)", request)
            logging.error(linkformset.errors)
            
        # TODO: redirect and save values in session
        return redirect('ptadmin:editdataset', dataset.uid)
            
    elif 'form_data' in request.session and '__form__' in request.session['form_data'] and request.session['form_data']['__form__'] == "DataEditForm":
#         logging.error("FromForm")
        request.session['form_data'].pop('__form__')
        form = ptadmin.forms.DatasetEditForm(datasetdetails['columns'], request.session['form_data'])
        formset = SampleFormSet(initial=dataset.sample_set.all(), 
                                form_kwargs={'columns': datasetdetails['columns']}, prefix='sample') # TODO: make this come from session
        request.session.pop('form_data')
    else:
#         logging.error("FromDB")
        form = ptadmin.forms.DatasetEditForm(datasetdetails['columns'], instance=dataset)
        formset = SampleFormSet(queryset=dataset.sample_set.filter(deleted__isnull=True).order_by('order'), 
                                form_kwargs={'columns': datasetdetails['columns']}, prefix='sample')
        linkformset = LinkFormSet(initial=dataset.efplinks, prefix='link')
        
    
    params = {
        'form': form, 
        'formset': formset,
        'linkformset': linkformset,
        'dataset': dataset, 
        'messages': MessageManager.getMessages(request),
        'datasetdetails': datasetdetails,
        'settings': settings,
        'title': 'Edit data-set',
    }
    
    return render(request, 'portaladmin/dataset-edit.html', params)


@permission_required('ptadmin.view_portal')
def deployefp(request, datasetuid):
    '''Attempts to deploy the data-set to eFP database'''
    
    try:
        dataset = common.models.Dataset.objects.get(uid=datasetuid, deleted__isnull=True)
    except:
        raise Http404
    
    status, reason = ptadmin.bl.datamanager.DataManager.deployEFP(dataset, override=True, settingsonly=request.GET.get('s', 'false')=="true")
    if status:
        MessageManager.addSuccess("Successfully deployed '%s' to eFP Browser" % dataset.name, request)
    else:
        MessageManager.addError(_efpFailedMessage(reason, dataset), request)
    return redirect('ptadmin:listdataset')


def _efpFailedMessage(reasoncode, dataset):
    '''Generates a message for user for the reason efp deployment failed'''
    
    REASONS = {
        'EXISTS': 'Data-set already deployed',
        'DATADIR': 'Failed to create deployment directory',
        'DATATABLE': 'Failed to create database table',
        'INSERTDATA': 'Failed to insert count data',
        'EXPGRAPH': 'Failed to create expression graphs on images',
        'SAVEIMAGEFILES': 'Failed to save image files in correct format',
        'EFPINFOXML': 'Failed to save eFP config file',
        'DATASETXML': 'Failed to save dataset config file',
        'COMPLETEFILE': 'Failed to mark dataset deployment as complete',
    }
    
    try:
        reason = REASONS[reasoncode]
    except:
        if reasoncode.startswith("eFP Data-set Validation Error"):
            reason = reasoncode
        else:
            reason = "Unknown failure in eFP Deployment"
    return "Failed to deploy '%s' to eFP Browser (reason: %s)" % (dataset.name, reason)



@permission_required('ptadmin.view_portal')
def deploydegust(request, datasetuid):
    '''Attempts to deploy the data-set to Degust database'''
    
    try:
        dataset = common.models.Dataset.objects.get(uid=datasetuid, deleted__isnull=True)
    except:
        raise Http404
    
    status, reason = ptadmin.bl.datamanager.DataManager.deployDegust(dataset, override=True)
    if status:
        MessageManager.addSuccess("Successfully deployed '%s' to Degust Browser" % dataset.name, request)
    else:
        MessageManager.addError(_degustFailedMessage(reason, dataset), request)
    return redirect('ptadmin:listdataset')


def _degustFailedMessage(reasoncode, dataset):
    '''Generates a message for user for the reason Degust deployment failed'''
    
    REASONS = {
        'EXISTS': 'Data-set already deployed',
        'CLEANDATA': 'Failed to remove previous deployment',
        'SETTINGS': 'Failed to create dataset settings file',
        'COUNTS': 'Failed to create dataset counts file',
        'COMPLETEFILE': 'Failed to mark dataset deployment as complete',
    }
    
    try:
        reason = REASONS[reasoncode]
    except:
        if reasoncode.startswith("Degust Data-set Validation Error"):
            reason = reasoncode
        else:
            reason = "Unknown failure in Degust Deployment"
    return "Failed to deploy '%s' to Degust Browser (reason: %s)" % (dataset.name, reason)


def _splitRowsTab(s):
    return s.split('\t')

def _splitRowsSpace(s):
    return s.split()

@permission_required('ptadmin.view_portal')
def colourChart(request):
    '''Creates a html colour chart for helping with eFP image colouring'''
    
    params = {
        'colours': [],
        'current': '',
    }
    
    if request.method == 'POST':
        tablestr = request.POST.get('table', '')
        table = tablestr.split('\n')
        if '\t' in tablestr:
            table = map(_splitRowsTab, table)
        else:
            table = map(_splitRowsSpace, table)
        params['colours'] = table
        params['current'] = tablestr
        
    return render(request, 'portaladmin/colourchart.html', params)




