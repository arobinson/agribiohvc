'''
Created on 11Jul.,2017

@author: arobinson
'''

import subprocess

from django.shortcuts import render
from django.contrib.auth.decorators import permission_required
try:
    from django.template.context_processors import csrf
except:
    from django.core.context_processors import csrf

from AgriBioHVC import settings
from common.bl.messagemanager import MessageManager

@permission_required('ptadmin.view_portal')
def status(request):
    '''Renders a status page'''
    
    # lookup disk usage
    cmd = ['df', '-h']
    cmd.extend(settings.DISK_USAGE_MOUNTS)
    usage_string = subprocess.check_output(cmd)
    usage = []
    for line in usage_string.split('\n')[1:]:
        if line:
            cols = line.split()
            usageint = int(cols[4][:-1])
            status = "OK"
            if usageint >= 90:
                status = "ERROR"
            elif usageint >= 75:
                status = "WARNING" 
            cols.append(status)
            usage.append(cols)
        
    
    # prepare response
    params = {
              'request': request, 
              'messages': MessageManager.getMessages(request),
              'group': 'ptadmin', 
              'page': 'home', 
              'settings': settings,
              'usage': usage,
              'title': 'Portal administration',
             }
    params.update(csrf(request))
    
    return render(request, "portaladmin/status.html", params)
