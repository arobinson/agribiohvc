from django.contrib import admin

# Register your models here.
import ptadmin

admin.site.register(ptadmin.models.TemplateGroup)
admin.site.register(ptadmin.models.TemplateOperation)
