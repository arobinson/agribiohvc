'''
Created on 2 Aug 2016

@author: arobinson
'''

import importlib
import logging
import re

from ptadmin import models
from AgriBioHVC import settings

class TemplateManager():
    '''Manager class for updating templates by download a page from a main site'''
    
    @classmethod
    def getTemplate(cls, templategroup, allops=False):
        '''Gets a template'''
        if allops:
            return Template(templategroup, models.TemplateOperation.objects.filter(group__name=templategroup).order_by('order'))
        return Template(templategroup, models.TemplateOperation.objects.filter(group__name=templategroup, enabled=True).order_by('order'))
    
    
    @classmethod
    def updateTemplate(cls, templategroup):
        '''Performs the update based on configuration in the database'''
        
        # load the operations
        template = cls.getTemplate(templategroup, False)
        
        # perform operations
        status = template.run()
        
        # output debug info
        print "Context:"
        OUTPUT_SIZE = 100
        for k,v in template.context.items():
            v = str(v)
            l = len(v)
            if l > OUTPUT_SIZE:
                v = "%s ... (%s more)" % (v[:OUTPUT_SIZE], l - OUTPUT_SIZE)
            v = v.replace("\n", "\\n")
            v = v.replace("\r", "\\r")
            print "- [%s]: '%s'" % (k,v)
        
        print ""
        
        print "Results:"
        for order, funcName, result in template.results:
            print "- %s(%s, ...) = '%s'" % (funcName, order,  result)
        
        return status


    @classmethod
    def _getFunction(cls, name, module=globals()):
        '''Returns a reference to a function.  The function name can be fully qualified. 
        Alternatively you can provide a module object which is used to get the function from.'''
        
        if name.count('.') > 0:
            pathsplit = name.split('.')
            packagename = ".".join(pathsplit[:-2])
                        
            module = importlib.import_module(packagename)
            className = pathsplit[-2]
            functionname = pathsplit[-1]
        else:
            functionname = name
        try:
                    
            return getattr(getattr(module, className), functionname)
        except AttributeError:
            if settings.DEBUG:
                logging.debug("Error")
            return None
        
    @classmethod
    def _getArgs(cls, argString, context):
        '''Converts a string to a python data structure'''
                
        obj = eval(argString, {"__builtins__":None}, context) 
        return obj


class Template(object):
    '''Stores template operations'''
    def __init__(self, name, operations=[]):
        ''''''
        self.name = name
        self._operations = operations
    def __iter__(self):
        return TemplateIterator(self)
    def run(self):
        '''Run all enabled operations in Template'''
        context = {}
        funcs = {}
        results = []
        status = True
        for op in self:
            if op.enabled:
                result = None
                
                # load function
                if op.function in funcs:
                    func = funcs[op.function]
                else:
                    func = TemplateManager._getFunction(op.function)
                    funcs[op.function] = func
                
                # load arguments
                args = TemplateManager._getArgs(op.args, context)
                
                try:
                    rc = func(context, *args)
                    context['?'] = rc
                    result = rc
                except Exception as e:
                    logging.exception("Error in: %s(*%s)" % (op.function, op.args))
                    result = str(e)
                    results.append([op.order, op.function, "Exception: %s" % result])
                    status = False
                    break
                
                results.append([op.order, op.function, result])
        self.context = context
        self.results = results
        return status
        
    
class TemplateIterator(object):
    '''Iterator object for Templates'''
    def __init__(self, template):
        self.i = 0
        self.template = template
    def next(self):
        try:
            v = self.template._operations[self.i]
            self.i += 1
            return v
        except:
            raise StopIteration



##### OPERATORS #####

def get_page(context, urlvar, contentvar):
    '''Downloads a page and adds it's context as '''
    html = None
    try: ## try Python 2.7 way
        import urllib2
        response = urllib2.urlopen(context[urlvar])
        html = response.read()
    except: ## Try Python 3 way
        try:
            import urllib.request
            response = urllib.request.urlopen(context[urlvar])
            data = response.read()
            html = data.decode('utf-8')
        except:
            return False
    
    context[contentvar] = html
    
    return True

def regex_capture(context, name, regex):
    '''
    Apply regex search to context var.  Named capture groups will be stored as context 
    vars and other groups under 'groups'.
    '''
    
#     regex = r'^(.*<!-- STANDARD DEFAULT -->).*(<!--/STANDARD DEFAULT -->.*)</html>.*$'
    match = re.compile(regex, re.S).search(context[name])
    if match:
        for k,v in match.groupdict().items():
            context[k] = v
        i=1
        for v in match.groups():
            context["re%s"%i] = v
            i+=1
        return "Matched (named: %s, un-named: %s)" % (len(match.groupdict()), len(match.groups()))
    return "Warning: No Match"
     


def string_replace(context, src, search, replace, dest):
    '''performs a basic string replace on a context var'''
    c = context[src].count(search)
    context[dest] = context[src].replace(search, replace)
    if c == 0:
        return "Warning: No Match"
    return "Matched (%s times)" % c

def string_append(context, src, string, dest):
    '''Appends string to end of context var'''
    try:
        context[dest] = context[src] + string
    except:
        return False
    return True


def regex_replace(context, src, regex, replace, dest):
    '''Performs a re.sub operation on variable'''
    
    context[dest] = re.sub(regex, replace, context[src], flags=re.DOTALL)
    return True
    
    
def constant(context, name, value):
    '''Creates new context var with fixed value'''
    context[name] = value
    return True

def rename(context, old, new):
    '''Renames a context var from 'old' to 'new' '''
    context[new] = context[old]
    context.pop(old, None)
    return True

def copy(context, old, new):
    '''Copies a context var from 'old' to 'new' '''
    context[new] = context[old]
    return True
    
def delete(context, *args):
    '''Deletes one or more context var'''
    for k in args:
        context.pop(k, None)
    return True
        
from common import models as commonmodels
def write_updateable_static(context, varname, name):
    '''Writes the contents of a contextvar to an updateable static file (in database)'''
     
    obj = commonmodels.UpdateableContent.objects.get_or_create(name=name)[0]
    obj.value = context[varname]
    obj.save()
     
    return True

def get_page_to_updateable_static(context, url, name):
    '''Gets a page and writes the result to an updateable statics'''
    
    # get page
    tempcontext = {'name': url}
    if not get_page(tempcontext, 'name', 'content'):
        return (False, 'Get page failed')
    
    # update static
    return write_updateable_static(tempcontext, 'content', name)
    

def write_updateable_template(context, varname, name):
    '''Writes the contents of a contextvar to an updateable template object'''
     
    obj = commonmodels.UpdateableTemplate.objects.get_or_create(name=name)[0]
    obj.value = context[varname]
    obj.save()
     
    return True




