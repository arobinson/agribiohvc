'''
Created on 23 Jan 2017

@author: arobinson
'''

import AgriBioHVC.settings
from ptadmin.bl import efpmanager, degustmanager
import os
import shutil
import csv
import StringIO
from efpbrowser.bl import efpconfig
from common import models

import ptadmin
import logging
import common
import background.bl

EFPIMAGETYPES = {
    'png': 'image/png',
    'tga': 'image/tga',
    }

class DataManager():
    '''Creates datasets in each expression viewer'''
    
    @classmethod
    def saveUploads(cls, dataset, files):
        '''
        Uploads files to upload data directory and saves what they were called (i.e. extension) 
        '''
        uploaddir = os.path.join(AgriBioHVC.settings.VIEWERSETTINGS['common']['datasetuploadsdirectory'], dataset.uid)
        
        # create dataset directory
        try:
            if os.path.exists(uploaddir):
                shutil.rmtree(uploaddir)
            os.mkdir(uploaddir)
        except:
            return (False, 'PERMISSION:%s' % uploaddir)
        
        # save files
        # counts
        try:
            with open(os.path.join(uploaddir,'counts.csv'), 'wb+') as destination:
                for chunk in files['countsfile'].chunks():
                    destination.write(chunk)
        except:
            return (False, 'PERMISSION:%s' % os.path.join(uploaddir,'counts.csv'))
        dataset.countsfile = 'counts.csv'
        
        # efpexprimage
        ext = files['efpexprimage'].name.split('.')[-1]
        if ext not in EFPIMAGETYPES:
            return (False, 'IMAGETYPE:%s'%ext)
        try:
            with open(os.path.join(uploaddir,'efpexprimage.%s'%ext), 'wb+') as destination:
                for chunk in files['efpexprimage'].chunks():
                    destination.write(chunk)
        except:
            return (False, 'PERMISSION:%s' % os.path.join(uploaddir,'efpexprimage.%s'%ext))
        dataset.efpexprimage = 'efpexprimage.%s'%ext
        
        # efphomeimage
        if 'efphomeimage' in files and files['efphomeimage'] and files['efphomeimage'].name:
            ext = files['efphomeimage'].name.split('.')[-1]
            if ext not in EFPIMAGETYPES:
                return (False, 'IMAGETYPE:%s'%ext)
            try:
                with open(os.path.join(uploaddir,'efphomeimage.%s'%ext), 'wb+') as destination:
                    for chunk in files['efphomeimage'].chunks():
                        destination.write(chunk)
            except:
                return (False, 'PERMISSION:%s' % os.path.join(uploaddir,'efphomeimage.%s'%ext))
        else: # otherwise just use expression image
            try:
                with open(os.path.join(uploaddir,'efphomeimage.%s'%ext), 'wb+') as destination:
                    for chunk in files['efpexprimage'].chunks():
                        destination.write(chunk)
            except:
                return (False, 'PERMISSION:%s' % os.path.join(uploaddir,'efphomeimage.%s'%ext))
            
        dataset.efphomeimage = 'efphomeimage.%s'%ext
        
        # efpdownimage
        if 'efpdownimage' in files and files['efpdownimage'] and files['efpdownimage'].name:
            ext = files['efpdownimage'].name.split('.')[-1]
            if ext not in EFPIMAGETYPES:
                return (False, 'IMAGETYPE:%s'%ext)
            try:
                with open(os.path.join(uploaddir,'efpdownimage.%s'%ext), 'wb+') as destination:
                    for chunk in files['efpdownimage'].chunks():
                        destination.write(chunk)
            except:
                return (False, 'PERMISSION:%s' % os.path.join(uploaddir,'efphomeimage.%s'%ext))
        else:
            ext = files['efpexprimage'].name.split('.')[-1]
            if ext not in EFPIMAGETYPES:
                return (False, 'IMAGETYPE:%s'%ext)
            try:
                with open(os.path.join(uploaddir,'efpdownimage.%s'%ext), 'wb+') as destination:
                    for chunk in files['efpexprimage'].chunks():
                        destination.write(chunk)
                dataset.efpprintimgmultiplier = 1
                dataset.save()
            except:
                return (False, 'PERMISSION:%s' % os.path.join(uploaddir,'efphomeimage.%s'%ext))
        dataset.efpdownimage = 'efpdownimage.%s'%ext
        
        try:
            dataset.save()
        except:
            return (False, 'SAVEDATASET')
        
        return (True, None)
    
    @classmethod
    def saveReferenceUploads(cls, reference, files):
        '''
        Saves Uploaded reference files to upload data directory and saves what they were called (i.e. extension)
        '''
        uploaddir = os.path.join(AgriBioHVC.settings.VIEWERSETTINGS['common']['referenceuploadsdirectory'], reference.uid)
        
        try:
            # delete old reference directory
            if os.path.exists(uploaddir):
                shutil.rmtree(uploaddir)
                
            # create reference directory
            os.mkdir(uploaddir)
        except:
            return (False, 'PERMISSION:%s' % uploaddir)
        
        # save files
        # gfffile
        ext = files['featurefile'].name.split('.')[-1]
        if ext not in ('txt', 'tsv', 'csv'):
            return (False, 'DATATYPENOTSUPPORTED:%s' % ext)
        filename = '%s.%s'% (reference.uid,ext)
        try:
            with open(os.path.join(uploaddir,filename), 'wb+') as destination:
                for chunk in files['featurefile'].chunks():
                    destination.write(chunk)
        except:
            return (False, 'PERMISSION:%s' % os.path.join(uploaddir,filename))
        reference.featurefile = filename
        
        try:
            reference.save()
        except:
            return (False, 'SAVEDATASET')
        
        return (True, None)
    
    @classmethod
    def getEFPImage(cls, dataset):
        '''Gets the efp expression image file (data-contents)'''
        uploaddir = os.path.join(AgriBioHVC.settings.VIEWERSETTINGS['common']['datasetuploadsdirectory'], dataset.uid)
        ext = dataset.efpexprimage.split('.')[-1]
        
        with open(os.path.join(uploaddir, dataset.efpexprimage)) as i:
            return (i.read(), EFPIMAGETYPES[ext])
        return ('', None)
    
    @classmethod
    def getDataDetails(cls, dataset):
        '''Returns details about columns in dataset'''
        
        headerlist = []
        sampledata = []
        
        uploaddir = os.path.join(AgriBioHVC.settings.VIEWERSETTINGS['common']['datasetuploadsdirectory'], dataset.uid)
        
        try:
            with open(os.path.join(uploaddir, dataset.countsfile)) as f:
                header = f.readline().rstrip()
                headerio = StringIO.StringIO(header)
                
                # check for tsv/csv
                reader = None
                if len(header.split(',')) >= 3:
                    reader = csv.reader(f, delimiter=',', quotechar='"')
                    headerlist = csv.reader(headerio, delimiter=',', quotechar='"').next()
                elif len(header.split('\t')) >= 3:
                    reader = csv.reader(f, delimiter='\t', quotechar='"')
                    headerlist = csv.reader(headerio, delimiter='\t', quotechar='"').next()
                
                try:
                    for _ in xrange(10):
                        sampledata.append(reader.next())
                except:
                    pass
                
            return {
                 'columns': headerlist,
                 'sample': sampledata
                }
        except IOError:
            return {'columns': ['None'], 'sample': [['No data uploaded yet']]}
        except:
            return {'columns': ['None'], 'sample': [['Unexpected error while loading data file']]}
        
        
    @classmethod
    def getFilenameForDataset(cls, dataset, fmt=None):
        '''Calculates the filename for a dataset (and converts raw to fmt if needed)'''
        uploaddir = os.path.join(AgriBioHVC.settings.VIEWERSETTINGS['common']['datasetuploadsdirectory'], dataset.uid)
        countcolumns = []
        for s in dataset.sample_set.filter(deleted__isnull=True).order_by('order'):
            countcolumns.extend(s.replicates)
        
        # calculate filename (and convert format if required)
        okformats = [c[0] for c in models.COUNT_FORMAT_CHOICES]
        filename = os.path.join(uploaddir, dataset.countsfile)
        filenamein = filename
        
        if fmt is None or fmt == dataset.countsformat:
            pass # no change needed
        elif fmt in okformats:
            if dataset.countsformat == 'RAW':
                if dataset.genelengthcolumn not in ('', '--', None, 0, '0'):
                    if fmt == 'TPM':
                        filename = os.path.splitext(filename)[0] + '.tpm'
                        if not os.path.exists(filename):
                            cls._writeTPM(filenamein, filename, countcolumns, dataset.genelengthcolumn)
                    elif fmt == 'RPKM':
                        filename = os.path.splitext(filename)[0] + '.rpkm'
                        if not os.path.exists(filename):
                            cls._writeRPKM(filenamein, filename, countcolumns, dataset.genelengthcolumn)
                    else:
                        raise NotImplementedError("Format '%s' not implemented" % fmt)
                else:
                    raise ValueError("Gene length column must be set to normalise")
            else:
                raise ValueError("Can only normalise RAW counts")
        else:
            raise ValueError("Format '%s' unknown" % fmt)
        
        return filename
    
    @classmethod
    def data(cls, dataset, fmt=None):
        '''Returns data row objects (Generator)'''
        
        filename = cls.getFilenameForDataset(dataset, fmt)
        
        # read file
        with open(filename) as f:
            reader = cls._filereader(f)
            for o in reader:
                yield o
    
    @classmethod
    def refdata(cls, reference, fmt=None):
        '''Returns reference data row tuples (Generator)'''
        
        uploaddir = os.path.join(AgriBioHVC.settings.VIEWERSETTINGS['common']['referenceuploadsdirectory'], reference.uid)
        filename = os.path.join(uploaddir, reference.featurefile)
        
        # read file
        with open(filename) as f:
            header = f.readline().rstrip()
            headerio = StringIO.StringIO(header)
            
            # check for tsv/csv
            tabpos = header.find('\t')
            commapos = header.find(',')
            fmt = None
            if tabpos >= 0 and commapos >= 0:
                if tabpos < commapos:
                    fmt = 'tsv'
                else:
                    fmt = 'csv'
            elif tabpos >= 0:
                fmt = 'tsv'
            elif commapos >= 0:
                fmt = 'csv'
            
            # create reader
            reader = None
#             if len(header.split(',')) >= 2:
            if fmt == 'csv':
                yield csv.reader(headerio, delimiter=',', quotechar='"').next()
                reader = csv.reader(f, delimiter=',', quotechar='"')
#             elif len(header.split('\t')) >= 2:
            elif fmt == 'tsv':
                yield csv.reader(headerio, delimiter='\t', quotechar='"').next()
                reader = csv.reader(f, delimiter='\t', quotechar='"')
            else:
                raise ValueError("Doesn't appear to be CSV or TSV format")
            
            # process other records
            for o in reader:
                yield o
    
    @classmethod
    def _writeTPM(cls, infile, outfile, countcolumns, lengthcolumn):
        '''
        Read RAW infile and write TPM normalised outfile
        
        @param infile: string filename of input RAW counts file
        @param outfile: string filename of file to write TPM normalised counts to
        @param countcolumns: list of strings, the column names which contain counts
        @param lengthcolumn: string, column name which contains gene/transcript length
        '''
        
        # 1. (read and) calculate RPK values
        values = []
        rpktotal = {k: 0.0 for k in countcolumns}
        with open(infile) as f:
            reader = cls._filereader(f)
            for row in reader:
                length = float(row[lengthcolumn]) / 1000.0 # in kilobases
                for col in countcolumns:
                    rpk = float(row[col]) / length
                    row[col] = rpk
                    rpktotal[col] += rpk
                values.append(row)
        
        # 2. calculate rpk scales (per million)
        for col in rpktotal:
            rpktotal[col] /= 1000000.0
        
        # 3. convert to TPM values (and write)
        with open(outfile, 'w') as f:
            writer = csv.DictWriter(f, reader.fieldnames, dialect='excel-tab')
            writer.writeheader()
            for row in values:
                for col in countcolumns:
                    row[col] /= rpktotal[col]
                    
                writer.writerow(row)
    
    @classmethod
    def _writeRPKM(cls, infile, outfile, countcolumns, lengthcolumn):
        '''
        Read RAW infile and write RPKM normalised outfile
        
        @param infile: string filename of input RAW counts file
        @param outfile: string filename of file to write RPKM normalised counts to
        @param countcolumns: list of strings, the column names which contain counts
        @param lengthcolumn: string, column name which contains gene/transcript length
        '''
        
        # 1. (read and) calculate total counts per sample
        # 1a. reads per sample
        values = []
        rps = {k: 0.0 for k in countcolumns}
        with open(infile) as f:
            reader = cls._filereader(f)
            for row in reader:
                for col in countcolumns:
                    rps[col] += float(row[col])
                values.append(row)
        
        # 1b. million reads per sample 
        mrps = {}
        for k in rps:
            mrps[k] = rps[k]/1000000.0
        
        # 2. RPM normalise counts
        for row in values:
            for col in countcolumns:
                row[col] = float(row[col]) / mrps[col]
        
        # 3. convert to RPKM values (and write)
        with open(outfile, 'w') as f:
            writer = csv.DictWriter(f, reader.fieldnames, dialect='excel-tab')
            writer.writeheader()
            for row in values:
                length = float(row[lengthcolumn]) / 1000.0
                for col in countcolumns:
                    row[col] /= length
                    
                writer.writerow(row)
            
    
    @classmethod
    def _filereader(cls, f):
        '''Creates reader that can read TSV or CSV files'''
        
        header = f.readline().rstrip()
        headerio = StringIO.StringIO(header)
        
        print f.name
        
        # check for tsv/csv
        reader = None
        if len(header.split(',')) >= 3:
            headerlist = csv.reader(headerio, delimiter=',', quotechar='"').next()
            reader = csv.DictReader(f, fieldnames=headerlist, delimiter=',', quotechar='"')
        elif len(header.split('\t')) >= 3:
            headerlist = csv.reader(headerio, delimiter='\t', quotechar='"').next()
            reader = csv.DictReader(f, fieldnames=headerlist, delimiter='\t', quotechar='"')
            
        return reader
        
        
        
    @classmethod
    def deployEFP(cls, dataset, override=False, settingsonly=False):
        '''
        Creates a new dataset in eFP Browser
        
        @param dataset: dataset instance representing the dataset to create
        @param override: if False function will return error if dataset already exists
        
        @return: (False, REASON) = error occurred, (True, None) = Success
        '''
        
        efpsettings = AgriBioHVC.settings.VIEWERSETTINGS['common'].copy()
        efpsettings.update(AgriBioHVC.settings.VIEWERSETTINGS['efp'])
        
        # check the dataset meets eFP requirements
        errors = efpmanager.EFPManager.datasetValid(dataset, efpsettings)
        if errors != True:
            reason = "eFP Data-set Validation Error"
            for e in errors:
                reason += "\n- %s" % (e,)
            return (False, reason)
        
        # create eFP dataset
        (status, reason) = efpmanager.EFPManager.createDataset(dataset, efpsettings, override, settingsonly)
        if not status:
            dataset.efpdeployed = False
            dataset.save()
            return (False, reason)
        
        # generate eFP config
        efpconfig.EFPConfig.generateConfig(dataset)
        dataset.efpdeployed = True
        dataset.save()
        
        return (True, None)
    
    
    @classmethod
    def deployReferenceLanding(cls, reference, session_key, override=False):
        '''
        Creates a new reference in the Landing Portal
        
        @param reference: reference instance representing the reference to create
        @param override: if False function will return error if reference already exists
        
        @return: (False, REASON) = error occurred, (True, None) = Success
        '''
        # class ReferenceFeature(models.Model):
        #     '''Transcript details for describing gene on landing page'''
        #      
        #     uid = models.CharField(max_length=50, help_text='Unique identifier used in URL and efp database table name.  e.g. barley')
        #     name = models.CharField(max_length=100, null=True, blank=True, help_text='User friendly name for feature if available')
        #     description = models.TextField(null=True, blank=True, help_text='Extra annotation for the gene/transcript/feature')
        #     
        #     reference = models.ForeignKey(Reference)
        
        if reference:

            tasks = [
                "Removing existing features",
                "Add new features",
                "Load feature IDs",
            ]
            for ds in reference.dataset_set.all():
                tasks.append("Link '%s' features" % (ds.name,))
            
            _deployReferenceLanding(session_key, "Deploying '%s (%s)' to Landing Portal" % (reference.name, reference.uid), tasks, reference)
            # TODO: summarise failures and report 
        
        else:
            return (False, "No reference")
        
        return (True, None)
    
    
    @classmethod
    def deployReferenceEFP(cls, reference, override=False):
        '''
        Creates a new reference in eFP Browser
        
        @param reference: reference instance representing the reference to create
        @param override: if False function will return error if reference already exists
        
        @return: (False, REASON) = error occurred, (True, None) = Success
        '''
        
        efpsettings = AgriBioHVC.settings.VIEWERSETTINGS['common'].copy()
        efpsettings.update(AgriBioHVC.settings.VIEWERSETTINGS['efp'])
        
#         # check the dataset meets eFP requirements
#         errors = efpmanager.EFPManager.datasetValid(reference, efpsettings)
#         if errors != True:
#             reason = "eFP Data-set Validation Error"
#             for e in errors:
#                 reason += "\n- %s" % (e,)
#             return (False, reason)
        
        # create eFP reference
        (status, reason) = efpmanager.EFPManager.createReference(reference, efpsettings, override)
        if not status:
            return (False, reason)
        
        return (True, None)
    
    
    @classmethod
    def deployDegust(cls, dataset, override=False):
        '''
        Creates a new dataset in Degust Browser
        
        @param dataset: dataset instance representing the dataset to create
        @param override: if False function will return error if dataset already exists
        
        @return: (False, REASON) = error occurred, (True, None) = Success
        '''
        
        degustsettings = AgriBioHVC.settings.VIEWERSETTINGS['common'].copy()
        degustsettings.update(AgriBioHVC.settings.VIEWERSETTINGS['degust'])
        
        # check the dataset meets Degust requirements
        errors = degustmanager.DegustManager.datasetValid(dataset, degustsettings)
        if errors != True:
            reason = "Degust Data-set Validation Error"
            for e in errors:
                reason += "\n- %s" % (e,)
            return (False, reason)
        
        # create Degust dataset
        (status, reason) = degustmanager.DegustManager.createDataset(dataset, degustsettings, override)
        if not status:
            dataset.degustdeployed = False
            dataset.save()
            return (False, reason)
        
        dataset.degustdeployed = True
        dataset.save()
        return (True, None)
    
    @classmethod
    def createDataset(cls, dataset, cleaned_data, override=False):
        '''
        Creates a new dataset in each viewer
        
        @param dataset: dataset instance representing the dataset to create
        @param override: if False function will return error if dataset already exists
        
        @return: (False, REASON) = error occurred, (True, None) = Success
        '''
        
        # create eFP dataset
        efpsettings = AgriBioHVC.settings.VIEWERSETTINGS['common'].copy()
        efpsettings.update(AgriBioHVC.settings.VIEWERSETTINGS['efp'])
        (status, reason) = efpmanager.EFPManager.createDataset(dataset, efpsettings, override)
        if not status:
            return (False, 'EFP:'+reason)
        
        # create Degust dataset
#         degustsettings = AgriBioHVC.settings.VIEWERSETTINGS['common'].copy()
#         degustsettings.update(AgriBioHVC.settings.VIEWERSETTINGS['degust'])
#         (status, reason) = degustmanager.DegustManager.createDataset(dataset, degustsettings, override)
#         if not status:
#             return (False, 'DEGUST:'+reason)
        
        return (True, None)

def _chunk(l, n):
    '''Generator that splits l into n sized chunks'''
    pos = 0
    while True:
        yield (l[i] for i in xrange(pos, pos+n) if i in l)
        pos += n
        if pos >= len(l):
            break

@background.bl.job
def _deployReferenceLanding(session_key, job, tasks, reference):
    '''
    Deploys a reference to landing portal (as a background task)
    '''
    
    
    del_task, add_task, load_task = tasks[:3]
    dataset_tasks = tasks[3:]
               
    # delete all existing reference features and links to them
    try:
        del_task.running()
        reference.referencefeature_set.all().delete();
        del_task.success()
    except:
        logging.exception("Delete Reference Features")
        return False
            
            # add features
#             i = 1
#             errorcount = 0
#             feature_ids = {}
#             logging.debug("Adding new features")
#             for row in cls.refdata(reference):
#                 try:
#                     gene, desc = row[:2]
#                     feature = common.models.ReferenceFeature(uid=gene, description=desc, reference=reference)
#                     feature.save()
#                     feature_ids[gene] = feature.id 
#                 except:
#                     logging.exception("Add feature")
#                     errorcount += 1
#                     if errorcount >= 10:
#                         break
#                 if i % 1000 == 0:
#                     logging.debug(" %s features complete" % (i,))
#                 i+=1
#             logging.debug("Adding new features done (%s features, %s errors)" % (i, errorcount))
#             
#             if errorcount >= 10:
#                 return (False, "ADD_ERROR_LIMIT")
            
    # bulk add features
    add_task.running()
    add_name = add_task.name
    features = []
    featurecount = 0
    for row in DataManager.refdata(reference):
        gene, desc = row[:2]
        features.append(common.models.ReferenceFeature(uid=gene, description=desc, reference=reference))
        featurecount+=1
        if featurecount % 1000 == 0:
            add_task.setName(add_name + "[loading features %s]" % featurecount)
    add_task.setName(add_name + "[loaded %s features]" % featurecount)
    try:
        add_task.setName(add_name + " [Creating %s features]" % featurecount)
        common.models.ReferenceFeature.objects.bulk_create(features, batch_size=500)
        add_task.setName(add_name + " [Added %s features]" % featurecount)
        add_task.success()
    except:
        add_task.error()
        logging.exception("Add failed")
        return False

    # load features
    load_task.running()
    logging.debug("Loading feature IDs")
    feature_ids = {}
    i = 1
    for rf in reference.referencefeature_set.all():
        feature_ids[rf.uid] = rf.id
        if i % 1000 == 0:
            logging.debug(" %s features complete" % (i,))
        i+=1
    logging.debug("Loading features IDs done (%s features)" % (i,))
    load_task.success()

    # link new reference features to data-sets
    datasetsfailed = 0
    logging.debug("linking data-sets to features")
    i = 0
    ds_task = None
    ds_name = ""
    for dataset in reference.dataset_set.all():
        if i < len(dataset_tasks):
            ds_task = dataset_tasks[i]
            ds_task.running()
            ds_name = ds_task.name
        logging.debug("linking %s to features" % (dataset.uid,))
        idrow = dataset.idcolumn
        dserrorcount = 0
        dsi = 1
        ds_feature_ids = []
        for row in DataManager.data(dataset):
            try:
                ds_feature_ids.append(feature_ids[row[idrow]])
                dsi += 1
            except:
                logging.exception("Link feature")
                dserrorcount += 1
                if dserrorcount >=10:
                    break
            if dsi % 1000 == 0:
                logging.debug(" %s features complete" % (dsi,))
                if ds_task:
                    ds_task.setName(ds_name + " [%s linked]" % (dsi,))
        try:
            if ds_task:
                ds_task.setName(ds_name + " [Saving %s]" % (dsi,))
            
            # original: ?? seconds
            dataset.features.set(ds_feature_ids)
            dataset.save()
            
            # try 2: 555.39 Seconds
#             CHUNK_SIZE=1000
#             ci=0
#             my_ids = []
#             for ds_feature_ids_chunk in _chunk(ds_feature_ids, CHUNK_SIZE):
#                 my_ids.extend(ds_feature_ids_chunk)
#                 dataset.features.set(my_ids, clear=False)
#                 dataset.save()
#                 ci+=CHUNK_SIZE
#                 if ds_task:
#                     ds_task.setName(ds_name + " [Saved %s of %s]" % (min(dsi,ci),dsi,))
            # try 1: ??
#             CHUNK_SIZE=1000
#             ci=0
#             for ds_feature_ids_chunk in _chunk(ds_feature_ids, CHUNK_SIZE):
#                 dataset.features.add(*ds_feature_ids_chunk)
#                 dataset.save()
#                 ci+=CHUNK_SIZE
#                 if ds_task:
#                     ds_task.setName(ds_name + " [Saved %s of %s]" % (min(dsi,ci),dsi,))
            if ds_task:
                ds_task.setName(ds_name + " [Saved %s]" % (dsi,))
        except:
            logging.exception("Link features")
            dserrorcount += 10
        if dserrorcount >= 10:
            datasetsfailed += 1
            if ds_task:
                ds_task.error()
        else:
            if ds_task:
                ds_task.success()
        i += 1
             
        logging.debug("linking %s to features done (%s features, %s errors)" % (dataset.uid, dsi, dserrorcount))
    logging.debug("linking data-sets to features done (%s failed)" % (datasetsfailed,))

    return datasetsfailed == 0


class DatasetWrapper(object):
    '''Changes dataset data-structure computing required values'''
    
    def __init__(self, dataset):
        self.dataset = dataset
        
    def __getattr__(self, key):
        '''Get attributes from dataset instead'''
        return getattr(self.dataset, key)
    
    def countsAs(self, format):
        '''
        Returns a filename of the counts file in required format.  Computes required format if required.
        
        @param format: String format (must be in common.models.COUNT_FORMAT_CHOICES)
        @return: filename string (or None on error)
        '''
        
                
            
        return None


   
