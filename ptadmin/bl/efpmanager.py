'''
Created on 23 Jan 2017

@author: arobinson
'''
from django.template.loader import get_template
from django.template.context import Context
import os
import re
import logging
import shutil
import ptadmin.bl


from PIL import Image, ImageDraw, ImageFont
import math
from common import models
from AgriBioHVC import settings
from django.utils import timezone
from datetime import datetime

# from ptadmin.bl import datamanager

from common.uid import DATASETUID, DATASETUID_LABEL

UIDREGEX = re.compile(r'^'+DATASETUID+r'$')
NAMEREGEX = re.compile(r'[^ \-._a-zA-Z0-9]')
HEXREGEX = re.compile(r'[^a-fA-F0-9]')
HTMLCOLOURNAMES = set([
    'silver','gray','white','maroon','red','purple','fuchsia','green',
    'lime','olive','yellow','navy','blue','teal','aqua','orange',
    'aliceblue','antiquewhite','aquamarine','azure','beige','bisque','blanchedalmond','blueviolet',
    'brown','burlywood','cadetblue','chartreuse','chocolate','coral','cornflowerblue','cornsilk',
    'crimson','darkblue','darkcyan','darkgoldenrod','darkgray','darkgreen','darkgrey','darkkhaki',
    'darkmagenta','darkolivegreen','darkorange','darkorchid','darkred','darksalmon','darkseagreen','darkslateblue',
    'darkslategray','darkslategrey','darkturquoise','darkviolet','deeppink','deepskyblue','dimgray','dimgrey',
    'dodgerblue','firebrick','floralwhite','forestgreen','gainsboro','ghostwhite','gold','goldenrod',
    'greenyellow','grey','honeydew','hotpink','indianred','indigo','ivory','khaki',
    'lavender','lavenderblush','lawngreen','lemonchiffon','lightblue','lightcoral','lightcyan','lightgoldenrodyellow',
    'lightgray','lightgreen','lightgrey','lightpink','lightsalmon','lightseagreen','lightskyblue','lightslategray',
    'lightslategrey','lightsteelblue','lightyellow','limegreen','linen','mediumaquamarine','mediumblue','mediumorchid',
    'mediumpurple','mediumseagreen','mediumslateblue','mediumspringgreen','mediumturquoise','mediumvioletred','midnightblue','mintcream',
    'mistyrose','moccasin','navajowhite','oldlace','olivedrab','orangered','orchid','palegoldenrod',
    'palegreen','paleturquoise','palevioletred','papayawhip','peachpuff','peru','pink','plum',
    'powderblue','rosybrown','royalblue','saddlebrown','salmon','sandybrown','seagreen','seashell',
    'sienna','skyblue','slateblue','slategray','slategrey','snow','springgreen','steelblue',
    'tan','thistle','tomato','turquoise','violet','wheat','whitesmoke','yellowgreen','rebeccapurple'
])

def closedb(connection, cursor):
    success = True
    try:
        cursor.close()
    except:
        success = False
    try:
        connection.close()
    except:
        success = False
    return success

def rollback(connection):
    success = True
    try:
        connection.rollback()
    except:
        success = False
    return success
    

class EFPManager():
    '''Creates and Manages dataset files/config within eFP Browser'''
    
    @classmethod
    def createReference(cls, reference, config, override=False):
        '''
        Creates a new reference inside eFP browser
        
        @param reference: reference object to deploy to efp
        @param config: dictionary of system configuration settings
        @param override: if False function will return error if dataset already exists
        
        @return: (False, REASON) = error occurred, (True, None) = Success
        '''
        
        # create 'REFERENCE_lookup' and 'REFERENCE_annotation' tables
        if not cls._createReferenceTable(reference, config):
            return (False, 'REFERENCETABLE')
        
        if not cls._insertReferenceData(reference, config):
            return (False, 'REFERENCEDATA')
        
        return (True, None)

    @classmethod
    def createDataset(cls, dataset, config, override=False, settingsonly=False):
        '''
        Creates a new dataset in eFP
        
        @param dataset: dataset instance representing the dataset to create
        @param config: dictionary of system configuration settings
        @param override: if False function will return error if dataset already exists
        
        @return: (False, REASON) = error occurred, (True, None) = Success
        '''
        
        # check if it exists already
        if not override and cls._exists(dataset, config):
            return (False, 'EXISTS')
        
        # create output directory
        if not cls._createOutputDir(dataset, config):
            return (False, 'DATADIR')
        
        if not settingsonly:
            # create 'DATASET_data' table
            if not cls._createDataTable(dataset, config):
                return (False, 'DATATABLE')
                   
            # insert data
            if not cls._insertData(dataset, config):
                return (False, 'INSERTDATA')
        
        # generate expression graph
        if not cls._createExpressionGraph(dataset, config):
            return (False, 'EXPGRAPH')
        
        # save image file PNG and TGA
        if not cls._saveImageFiles(dataset, config):
            return (False, 'SAVEIMAGEFILES')
        
        # create efp_info.xml file
        if not cls._createEFPInfoXML(dataset, config):
            return (False, 'EFPINFOXML')
        
        # create DATASET.xml file
        if not cls._createDatasetXML(dataset, config):
            return (False, 'DATASETXML')
        
        # create settings dictionary
        
        
        # mark as complete
        if not cls._markComplete(dataset, config):
            return (False, 'COMPLETEFILE')
        
        return (True, None)
    
    @classmethod
    def datasetValid(cls, dataset, config):
        '''
        Checks if a dataset has the required values for eFP deployment
        
        @return: True or list
        '''
                
        errors = []
        
        cls._validateUID(errors, dataset, 'uid')
#     countsfile = models.CharField(max_length=50, blank=True, help_text='The name of the counts file (as saved in upload dir)')
#     efpimage = models.CharField(max_length=50, blank=True, help_text='The name of the efpimage file (as saved in upload dir)')

        cls._validateName(errors, dataset, 'name')
        cls._validateNotNull(errors, dataset, 'species')
        
        cls._validatePositiveInt(errors, dataset, 'graphtop')
        cls._validatePositiveInt(errors, dataset, 'graphleft')
        cls._validatePositiveInt(errors, dataset, 'graphbottom')
        cls._validatePositiveInt(errors, dataset, 'graphright')
        cls._validatePositiveInt(errors, dataset, 'legendtop')
        cls._validatePositiveInt(errors, dataset, 'legendleft')
        
        cls._validateGreaterThan(errors, dataset, 'graphbottom', 'graphtop')
        cls._validateGreaterThan(errors, dataset, 'graphright', 'graphleft')
        
        cls._validateReverseForeignKeyCount(errors, dataset.sample_set.filter(deleted__isnull=True), 'samples', 2)
        cls._validateReverseForeignKeyCount(errors, dataset.sample_set.filter(deleted__isnull=True, is_control=True), 'samples-control', 1)
        cls._validateReverseForeignKeyCount(errors, dataset.sample_set.filter(deleted__isnull=True, is_control=False), 'samples-other', 1)
        
        cls._validateCountsFormat(errors, dataset, 'efpcountsformat', 'countsformat', 'genelengthcolumn')
        
        for sample in dataset.sample_set.filter(deleted__isnull=True).order_by('order'):
            cls._validateName(errors, sample, 'name')
#             replicates = modelfields.MultiTextField(blank=True, default="", help_text='Tab-delimited list of replicate column names (matchs uploaded dataset)')
#             description = models.TextField(help_text='Text describing this sample such as treatments and/or time points')
#             
#             # efp sample settings
#             efp_colour = models.CharField(max_length=7, default="", blank=True, help_text='The HTML colour code for this sample in eFP Image. e.g. #FF0000')
#             efp_coords = models.CharField(max_length=1000, default="", blank=True, help_text='Imagemap area i.e. comma-separated x,y coords of click polygon')
            cls._validateHTMLColour(errors, sample, 'efp_colour', blank=sample.is_control==True)
            if sample.is_control==False:
                cls._validateHasValueString(errors, sample, 'efp_coords')
#             cls._validateIntegerCommaList(errors, sample, 'efp_coords')
        
        if len(errors) > 0:
            return errors
        return True
    
    def getConfiguration(self, dataset):
        '''Gets the settings for a given dataset.  Loads defaults and applies 
        any overrides from dataset'''
        
        result = {}
        result.update()
        
    
    ## Internal helper functions
    
    @classmethod
    def _validateCountsFormat(cls, errors, obj, efpcountfield, uploadcountfield, lengthfield):
        '''Makes sure counts format can be generated'''
        
        efpcountsformat = getattr(obj, efpcountfield)
        uploadcountsformat = getattr(obj, uploadcountfield)
        lengthfield = getattr(obj, lengthfield)
        
        
        formats = [c[0] for c in models.COUNT_FORMAT_CHOICES]
        
        if efpcountsformat in formats: # specified output format
            if efpcountsformat != uploadcountsformat: # conversion needed?
                if uploadcountsformat != 'RAW':
                    errors.append((efpcountfield, 'Upload format must be RAW if you wish to convert count format'))
                if lengthfield in ('', '--', None, 0, '0'):
                    errors.append((lengthfield, '{field1} field is required to normalise counts'))
    
    @classmethod
    def _validatePositiveInt(cls, errors, obj, field1):
        '''Makes sure field is a positive integer'''
        val = getattr(obj, field1)
        try:
            if str(val)!=str(int(val)):
                raise ValueError
            elif int(val) < 0:
                errors.append((field1, '{field1} must be greater than or equal zero (>= 0)'))
        except ValueError:
            errors.append((field1, '{field1} must be a positive integer (whole number)'))
    
    @classmethod
    def _validateGreaterThan(cls, errors, obj, field1, field2):
        '''Makes sure field1 is greater than field2'''
        val1 = getattr(obj, field1)
        val2 = getattr(obj, field2)
        if float(val1) <= float(val2):
            errors.append(((field1, field2), '{field1} must be greater than {field2}'))
            
    @classmethod
    def _validateHasValueString(cls, errors, obj, field1):
        '''Makes sure field1 has a value (non-whitespace)'''
        val = getattr(obj, field1)
        if val is None or len(str(val).strip()) == 0:
            errors.append((field1, '{field1} must have a value'))
            return False
        return True
            
    @classmethod
    def _validateNotNull(cls, errors, obj, field1):
        '''Makes sure field1 has a value (non-whitespace)'''
        val = getattr(obj, field1)
        if val is None:
            errors.append((field1, '{field1} must have a value'))
            return False
        return True
    
    @classmethod
    def _validateReverseForeignKeyCount(cls, errors, obj, field1, count):
        '''Checks resultset has at least count values'''
        if obj is None or obj.count() < count:
            errors.append((field1, '{field1} must have at least ' + str(count) + ' values'))
            return False
        return True
    
    @classmethod
    def _validateUID(cls, errors, obj, field1):
        '''Checks field1 only contains [0-9a-zA-Z] characters'''
        val = getattr(obj, field1)
        if not bool(UIDREGEX.search(val)):
            errors.append((field1, '{field1} contains invalid characters; ' + DATASETUID_LABEL))
            return False
        return True
    
    @classmethod
    def _validateName(cls, errors, obj, field1):
        '''Checks field1 only contains [0-9a-zA-Z] characters'''
        val = getattr(obj, field1)
        if bool(NAMEREGEX.search(val)): # checks for existence of not allowed characters
            errors.append((field1, '{field1} contains invalid characters; it must only include letters, numbers, space, dash (-) or underscore (_).'))
            return False
        return True
    
    @classmethod
    def _validateHTMLColour(cls, errors, obj, field1, blank=False):
        '''Checks field1 only contains [0-9a-zA-Z] characters'''
        val = str(getattr(obj, field1)).strip()
        if blank and val == "":
            return True
        if val not in HTMLCOLOURNAMES and not (len(val) in (4, 7) and val.startswith("#") and not HEXREGEX.search(val[1:])):
            errors.append((field1, '{field1} must be valid HTML hex code or colour name. e.g. "grey", "#AAA", or "#AAAAAA"'))
            return False
        return True
    
    #######################################################################################################
    
    @classmethod
    def _exists(cls, dataset, config):
        '''Checks if dataset already exists in eFP'''
        
        datadir = os.path.join(config['datadirectory'], dataset.uid)
        
        return os.path.exists(datadir)
    
    @classmethod
    def _createOutputDir(cls, dataset, config):
        '''Creates the directory for the datafiles to live'''
        
        try:
            datadir = os.path.join(config['datadirectory'], dataset.uid)
            if os.path.exists(datadir):
                shutil.rmtree(datadir)
            os.mkdir(datadir)
            os.mkdir(os.path.join(datadir, 'output'))
        except:
            return False
        return True
    
    @classmethod
    def _splitQueries(cls, queryString):
        '''Creates a list of queries by parsing queryString'''
        lines = queryString.split('\n')
        
        # remove comment lines
        cleanLines = []
        for l in lines:
            if not l.startswith('--') and l.strip() != '':
                cleanLines.append(l)
                
        # split by lines ending with ';'
        queries = []
        query = ''
        for l in cleanLines:
            query += ' ' + l.strip()
            if query.endswith(';'):
                queries.append(query.strip())
                query = ''      
        if query != '':
            queries.append(query.strip())
            query = ''
        
        return queries
    
    @classmethod
    def _createReferenceTable(cls, reference, config):
        '''Creates the required database table'''
        
        # create cleanup queries
        template = get_template('portaladmin/efp/REFERENCE-cleanup.sql')
        context = Context({
             'species': reference.species,
             'reference': reference,
             'config': config,
            })
        queries = cls._splitQueries(template.render(context))
        
        # create queries
        template = get_template('portaladmin/efp/REFERENCE.sql')
        context = Context({
             'species': reference.species,
             'reference': reference,
             'config': config,
            })
        queries.extend(cls._splitQueries(template.render(context)))
        
        try:
            import MySQLdb
            db =MySQLdb.Connect(host=config['database'][0], 
                                user=config['setupuser'][0], 
                                passwd=config['setupuser'][1], 
                                db=config['database'][2],
    #                             port=config['database'][1],
                                )
            cur = db.cursor()
            for q in queries:
                cur.execute(q)
        except MySQLdb.Error:
            logging.exception('Create exception')
            rollback(db)
            closedb(db, cur)
            return False
        
        db.commit()
        return closedb(db, cur)
    
    @classmethod
    def _insertReferenceData(cls, reference, config):
        '''Writes uploaded reference data to ReferenceTable'''

        date = datetime.now().strftime("%Y-%m-%d")

        queryLookup = 'INSERT INTO `'+reference.uid+'_lookup` (`probeset`,`loc`,`date`) VALUES (%s, %s, "' + date + '");'
        queryAnnotation = 'INSERT INTO `'+reference.uid+'_annotation` (`loc`,`annotation`,`date`) VALUES (%s, %s, "' + date + '");'
        
        # run queries
        try:
            import MySQLdb
            db =MySQLdb.Connect(host=config['database'][0], 
                                user=config['setupuser'][0], 
                                passwd=config['setupuser'][1], 
                                db=config['database'][2],
    #                             port=config['database'][1],
                                )
            cur = db.cursor()
            for row in ptadmin.bl.datamanager.DataManager.refdata(reference):
                
                gene, desc = row[:2]
                
                # insert annotation
                cur.execute(queryAnnotation, (gene, desc))
                
                # insert lookup
                cur.execute(queryLookup, (gene, gene))
            
            # next row
            
        except MySQLdb.Error:
            logging.exception('INSERT exception')
            rollback(db)
            closedb(db, cur)
            return False
        
        db.commit()
        return closedb(db, cur)
    
    @classmethod
    def _createDataTable(cls, dataset, config):
        '''Creates the required database table'''
        
        # create cleanup queries
        template = get_template('portaladmin/efp/DATASET-cleanup.sql')
        context = Context({
             'dataset': dataset,
             'config': config,
             'efp': EFPCompute(dataset),
            })
        queries = cls._splitQueries(template.render(context))
        
        # create queries
        template = get_template('portaladmin/efp/DATASET.sql')
        context = Context({
             'dataset': dataset,
             'config': config,
             'efp': EFPCompute(dataset),
            })
        queries.extend(cls._splitQueries(template.render(context)))
        
        try:
            import MySQLdb
            db =MySQLdb.Connect(host=config['database'][0], 
                                user=config['setupuser'][0], 
                                passwd=config['setupuser'][1], 
                                db=config['database'][2],
    #                             port=config['database'][1],
                                )
            cur = db.cursor()
            for q in queries:
                cur.execute(q)
        except MySQLdb.Error:
            logging.exception('Create exception')
            rollback(db)
            closedb(db, cur)
            return False
        
        db.commit()
        return closedb(db, cur)
    
    @classmethod
    def _insertData(cls, dataset, config):
        '''Writes uploaded data to DataTable'''
        
        # get replicate details
        replicates = {}
        reporder = []
        repnum = 1
        controlreps = []
        controlsamp = None
        for s in dataset.sample_set.filter(deleted__isnull=True).order_by('order'):
            for r in s.replicates:
                replicates[r] = ['', 0, repnum, '%s.txt'%repnum, r, s.name]
                reporder.append(r)
                repnum += 1
                if s.is_control == True:
                    controlreps.append(r)
                    controlsamp = s
    
        query = 'INSERT INTO `'+dataset.uid+'_data` ' + \
                '(data_probeset_id,data_signal,sample_id,proj_id,sample_file_name,data_call,data_p_val,data_bot_id,sample_tissue) VALUES '+\
                '(%s,              %s,         %s,       1,      %s,              NULL,     0,         %s,         %s);'
                # Gene/transcript id
                # count
                # 1 based index for sample
                # sample_id + '.txt'
                # Replicate name
                # Sample name
                
#         logging.error('QUERY: %s' % query)
        
        # run queries
        try:
            import MySQLdb
            db =MySQLdb.Connect(host=config['database'][0], 
                                user=config['setupuser'][0], 
                                passwd=config['setupuser'][1], 
                                db=config['database'][2],
    #                             port=config['database'][1],
                                )
            cur = db.cursor()
            rownum = 1
            for row in ptadmin.bl.datamanager.DataManager.data(dataset, dataset.efpcountsformat):
                geneid = row[dataset.idcolumn]
                
                # add signal values
                for rep in reporder:
                    vals = replicates[rep]
                    vals[0] = geneid
                    vals[1] = row[rep]
                    cur.execute(query, vals)
                    
                # add control value
                if controlsamp:
                    mean = 0.0
                    for ctrl in controlreps:
                        mean += float(row[ctrl])
                    if len(controlreps) > 1:
                        mean /= len(controlreps)
                    
                    vals = [geneid, mean, repnum, '%s.txt'%repnum, controlsamp.name, controlsamp.name+"_CTRL"]
                    cur.execute(query, vals)
                if rownum % 1000 == 0:
                    logging.debug("Row %s"% (rownum,))
                
                rownum += 1
            
            # next row
            logging.debug("Uploaded %s rows"% (rownum - 1,))
            
        except MySQLdb.Error:
            logging.exception('INSERT exception')
            rollback(db)
            closedb(db, cur)
            return False
        
        db.commit()
        return closedb(db, cur)
    
    @classmethod
    def _createExpressionGraph(cls, dataset, config):
        '''Creates the expression graph for the image'''
        
        # get count columns
        countColumns = []
        for s in dataset.sample_set.filter(deleted__isnull=True).order_by('order'):
            countColumns.extend(s.replicates)
        
        ## 1) analyse data points
        # - calculate the max mean
        # - count how many points are at each log10 section (i.e. 0-9, 10-99, 100-999 etc)
#         exps = {}
        expValues = []
#         meanExpression = 0.0
        expCount = 0
        log10Means = {}
        maxmean = 0
        for row in ptadmin.bl.datamanager.DataManager.data(dataset, dataset.efpcountsformat):
            
            # calculate mean expression for gene
            mean = 0.0
            count = 0
            for column in countColumns:
                mean += float(row[column])
                count += 1
            mean = mean / count
            expValues.append(mean)
            
#             meanExpression += mean
            if mean > 0:
                log10Mean = int(math.floor(math.log(math.ceil(mean), 10)))
                if log10Mean in log10Means:
                    log10Means[log10Mean] += 1
                else:
                    log10Means[log10Mean] = 1
                expCount += 1
            
            if mean > maxmean:
                maxmean = mean
#                 print "id: ", mean, row['Gene_id']
            
#             mean = int(mean)
#             if mean in exps:
#                 exps[mean] += 1
#             else:
#                 exps[mean] = 1
        
#         avgMean = meanExpression / expCount
        
#         print "log10Means", log10Means
#         print "expCount", expCount
#         print "maxmean", maxmean
#         print "avgMean", avgMean

        ## 2) work out how many divisions are needed and the range of each
        # - calculate how many division should be drawn (i.e. ones that show 99.95% of samples)
        keys = log10Means.keys()
        keys.sort()
        meanSum = 0.0
        maxDivision = 100
        divisionCount = 2
        for k in keys:
            meanSum += log10Means[k]
#             print "%s:\t%s\t%s\t(%s%%)\t%s's" % (k, meanSum, log10Means[k], meanSum / expCount, math.pow(10, k))
            if k > 1:
                maxDivision = math.pow(10, k)
                divisionCount = k
            if meanSum / expCount >= 0.9995: #99.95% of genes
                break
        
#         print "maxDivision", maxDivision
#         print "divisionCount", divisionCount
        
        # - find scale start points
#         scaleStarts = []
#         m = 10
#         
#         
        # - combine divis
        dC = divisionCount
        interval = 10
        while dC > 3:
            dC/=2.0
            interval*=10
        dC = int(math.ceil(dC))
#         print "dC", dC
#         print "interval", interval
        
        scales = []
        ls = 1
        for _ in range(dC):
            ls *= interval
            scales.append(ls)
        
#         print "scales", scales
        
        
        
        
        # calculate which scale divisions are needed
#         multiplier = 10
#         if divisionCount >= 4:
#             multiplier = 100
#         scales = []
#         cs = 1
#         while cs < maxmean:
#             cs *= multiplier
#             scales.append(cs)
#         
#         print "scales", scales
        
        # calculate the size of each scale division
        graphwidth = dataset.graphright - dataset.graphleft
#         graphdivwidth = float(graphwidth) / (len(scales)-1)
        graphdivwidth = float(graphwidth) / (len(scales))
        
#         print "graphwidth", graphwidth
#         print "graphdivwidth", graphdivwidth
        
# # the little graph on the tga image has a scale
# # such that 1 unit is x pixels for different ranges on the x-axis of the graph
# # the GRAPH_SCAL_UNIT consists of value pairs: upper end of range and scale unit
# # so ((1000, 0.031), (10000, 0.003222), (1000000, 0.00031)) means:
# # use 0.031 as scale unit for 0 < signal < 1000
# # use 0.003222 as scale unit for 1000 < signal < 10000
# # use 0.00031 as scale unit for 10000 < signal < 1000000
# # see also efp.drawImage()
# GRAPH_SCALE_UNIT = {}
# # the default values are used if for the given data source no special values are defined
# # GRAPH_SCALE_UNIT["default"] = ((0.06, 16) (0.006, 73), (0.0006, 130))
# GRAPH_SCALE_UNIT[DATA_SRC] = [(1000, 0.06), (10000, 0.006), (100000, 0.0006)]
        
        # update scale factors
        lastscale = 0
        GRAPH_SCALE_UNIT = []
        scalepixelwidths = []
        offset = 0.0
        firstBinSize = None
        for scale in scales:
            units = scale-lastscale
            GRAPH_SCALE_UNIT.append((scale, graphdivwidth / units))
            
            # calculate the scale for values in this division (first is 1)
            binSize = units/graphdivwidth
            if firstBinSize is None:
                firstBinSize = binSize
            binScale = firstBinSize/binSize
                
            scalepixelwidths.append((scale, units / graphdivwidth, lastscale, round(offset), binScale))
            
            lastscale = scale
            offset += graphdivwidth
        
#         print "GRAPH_SCALE_UNIT", GRAPH_SCALE_UNIT
#         print "scalepixelwidths", scalepixelwidths
        
        # store graph scale so it can be used in config
        dataset._GRAPH_SCALE_UNIT = GRAPH_SCALE_UNIT
        
        # construct bins
        countbins = [0] * graphwidth
#         for exp in exps:
#             for limit, divider, prefix, binoffset in scalepixelwidths:
#                 if exp <= limit:
#                     binnum = int((exp - prefix) / divider + binoffset)
#                     try:
#                         countbins[binnum] += exps[exp]
#                     except IndexError:
#                         print exp
#                         pass
#                     break
        # map each gene's mean expression to a bin
        for exp in expValues:
            for limit, divider, prefix, binoffset, scale in scalepixelwidths:
                if exp <= limit:
                    binnum = int((exp - prefix) / divider + binoffset)
                    try:
                        countbins[binnum] += scale
                    except IndexError:
#                         print exp
                        pass
                    break
#         print countbins
        
        countbins[0] = 0
        
        # log10 values and calculate max height
        maxexp = 0
        for i in xrange(len(countbins)):
#             if countbins[i] > 0:
#                 countbins[i] = math.log(countbins[i], 10)
            if countbins[i] > maxexp:
                maxexp = countbins[i]
        
        # home image
        im = Image.open(os.path.join(config['datasetuploadsdirectory'], dataset.uid, dataset.efphomeimage))
        cls._drawGraph(EFPCompute(dataset, False), im, countbins, scales, maxexp)
        im.save(os.path.join(config['datasetuploadsdirectory'], dataset.uid, "graph_%s" % dataset.efphomeimage))
        
        # expression image
        im = Image.open(os.path.join(config['datasetuploadsdirectory'], dataset.uid, dataset.efpexprimage))
        cls._drawGraph(EFPCompute(dataset, False), im, countbins, scales, maxexp)
        im.save(os.path.join(config['datasetuploadsdirectory'], dataset.uid, "graph_%s" % dataset.efpexprimage))
        
        # expression highres image
        if dataset.efpdownimage:
            im = Image.open(os.path.join(config['datasetuploadsdirectory'], dataset.uid, dataset.efpdownimage))
            cls._drawGraph(EFPCompute(dataset, True), im, countbins, scales, maxexp)
            im.save(os.path.join(config['datasetuploadsdirectory'], dataset.uid, "graph_%s" % dataset.efpdownimage))
        
        return True
    
    
    @classmethod
    def _drawGraph(cls, dataset, im, countbins, scales, maxexp):
        '''
        Draws the graph data onto im
        '''
        f8 = '08'
        f10 = '10'
        fontsizes = [(8,'08'),(10,'10'),(12,'12'),(14,'14'),(18,'18'),(24,'24'),]
        for h, s in fontsizes:
            if h <= dataset.length(8): f8 = s
            if h <= dataset.length(10): f10 = s
        
        fonts = {
            'reg8pt': 'efpbrowser/efp/static/pilfonts/helvR%s.pil'%f8,
            'bold8pt': 'efpbrowser/efp/static/pilfonts/helvB%s.pil'%f8,
            'bold10pt': 'efpbrowser/efp/static/pilfonts/helvB%s.pil'%f10,
        }
#         print fonts
        
        graphwidth = dataset.graphright - dataset.graphleft
        graphheight = dataset.graphbottom - dataset.graphtop
#         graphdivwidth = float(graphwidth) / (len(scales)-1)
        graphdivwidth = float(graphwidth) / (len(scales))
        expperpixel = float(graphheight) / maxexp
        left = dataset.graphleft
        top = dataset.graphtop
        
#         print "POS: t:%s  l:%s h:%s w:%s" % (top,left,graphheight,graphwidth,)
        
        draw = ImageDraw.Draw(im)
        
        # draw counts on graph
        for i in xrange(1,len(countbins)+1):
            ex = countbins[i-1]
            valHeight = ex * expperpixel
            if valHeight > 0:
                draw.line((left+i*dataset.length(1), top+graphheight-1, left+i*dataset.length(1), top+graphheight-(valHeight)), fill='grey',width=dataset.length(1))
        
        # draw graph axis
        draw.line((left-dataset.length(3), top+graphheight-1, left+graphwidth, top+graphheight-1), fill='black',width=dataset.length(1)) # x
        draw.line((left, top, left, top+graphheight+dataset.length(3)), fill='black',width=dataset.length(1)) # y
        
        # y tick's
        draw.line((left, top, left-dataset.length(3), top), fill='black',width=dataset.length(1))
        draw.line((left, top+graphheight/2, left-dataset.length(3), top+graphheight/2), fill='black',width=dataset.length(1))
        
        # calculate how many x subdivisions to draw
        subdivs = ((10,1),(5,2),(2,5),(1,10))
        i = 0
        subdiv, multiplier = subdivs[i]
        i += 1
        while graphdivwidth/subdiv < 5 and i < len(subdivs):
            subdiv, multiplier = subdivs[i]
            i += 1
            
        # draw x divisions, subdivision and labels
        pos = 0.0
        font = ImageFont.load(os.path.join(settings.BASE_DIR, fonts['reg8pt']))
        for sc in scales: #[:-1]
            pos += graphdivwidth
            l = round(pos)
            
            # minor lines
            graphsubdivwidth = graphdivwidth / subdiv
            sl = left+l-graphdivwidth
            for i in xrange(subdiv-1):
                sl += graphsubdivwidth
                draw.line((sl, dataset.graphbottom, sl, dataset.graphbottom +dataset.length(1)), fill='black',width=dataset.length(1))
            
            # major line
            draw.line((left+l, dataset.graphbottom, left+l, dataset.graphbottom +dataset.length(3)), fill='black',width=dataset.length(1))
            
            # label
            eValue = int(round(math.log(sc,10)))
            if eValue == 1:
                label = "10"
            elif eValue == 2:
                label = "100"
            else:
                label = "1e%s" % (eValue,)
#             label = "%s" % sc
            sz = draw.textsize(label, font)[0] / 2
            szw, szh = draw.textsize(label, font)
            draw.text((left+l-szw/2, dataset.graphbottom+dataset.length(3)), label, 'black', font)
        
        label = "0"
        sz = draw.textsize(label, font)[0] / 2
        draw.text((left-sz, dataset.graphbottom+dataset.length(3)), label, 'black', font)
        
        # x label
        font = ImageFont.load(os.path.join(settings.BASE_DIR, fonts['bold8pt']))
        xlabel = models.COUNT_FORMAT_LOOKUP[dataset.efpcountsformat]
        szw, szh = draw.textsize(xlabel, font)
        draw.text((left+graphwidth/2-szw/2, dataset.graphbottom+szh+dataset.length(4)), xlabel, 'black', font)
        
        # title
        font = ImageFont.load(os.path.join(settings.BASE_DIR, fonts['bold10pt']))
        xlabel = 'Expression level histogram'
        szw, szh = draw.textsize(xlabel, font)
        draw.text((left+graphwidth/2-szw/2, dataset.graphtop-szh-dataset.length(4)), xlabel, 'grey', font)
        
    
    @classmethod
    def _saveImageFiles(cls, dataset, config):
        '''Write the image files to datadir as PNG and TGA format'''
        
        # expression image
        try:
            im = Image.open(os.path.join(config['datasetuploadsdirectory'], dataset.uid, "graph_%s" % dataset.efpexprimage))
            im.save(os.path.join(config['datadirectory'], dataset.uid,'%s_image.tga'%dataset.uid), 'TGA')
        except:
            logging.exception("expression image")
            return False
        
        # home image
        try:
            im = Image.open(os.path.join(config['datasetuploadsdirectory'], dataset.uid, "graph_%s" % dataset.efphomeimage))
            im.save(os.path.join(config['datadirectory'], dataset.uid,'%s.png'%dataset.uid), 'PNG')
        except:
            logging.exception("home image")
            return False
        
        # download expression image
        try:
            if dataset.efpdownimage:
                im = Image.open(os.path.join(config['datasetuploadsdirectory'], dataset.uid, "graph_%s" % dataset.efpdownimage))
            else:
                im = Image.open(os.path.join(config['datasetuploadsdirectory'], dataset.uid, "graph_%s" % dataset.efpexprimage))
                dataset.efpprintimgmultiplier = 1
                dataset.save()
            im.save(os.path.join(config['datadirectory'], dataset.uid,'%s_down.tga'%dataset.uid), 'TGA')
        except:
            logging.exception("download image")
            return False
        
        return True
        
    @classmethod
    def _createDatasetXML(cls, dataset, config):
        '''Creates a DATASET.xml file contents'''
        
        template = get_template('portaladmin/efp/DATASET.xml')
        
        config['HIRES'] = False
        
        context = Context({
             'dataset': dataset,
             'config': config,
             'efp': EFPCompute(dataset, False),
             'imagename': 'image',
            })
        
        # render normal config
        fc = template.render(context)
        try:
            filename = os.path.join(config['datadirectory'], dataset.uid, dataset.uid + '.xml')
#             logging.error("DATASET.xml: %s" % filename)
            with open(filename, 'w') as f:
                f.write(fc)
        except:
            logging.exception("Dataset.xml")
            return False
        
        # render highres config
        context['efp'].hires = True
        context['imagename'] = "down"
        fc = template.render(context)
        try:
            filename = os.path.join(config['datadirectory'], dataset.uid, dataset.uid + '_hr.xml')
#             logging.error("DATASET.xml: %s" % filename)
            with open(filename, 'w') as f:
                f.write(fc)
        except:
            logging.exception("Dataset_hr.xml")
            return False
        return True
    
    @classmethod
    def _createEFPInfoXML(cls, dataset, config):
        '''Creates a efp_info.xml file contents'''
        
        template = get_template('portaladmin/efp/efp_info.xml')
        
        context = Context({
             'dataset': dataset,
             'config': config,
            })
        fc = template.render(context)
        try:
            filename = os.path.join(config['datadirectory'], dataset.uid, 'efp_info.xml')
#             logging.error("DATASET.xml: %s" % filename)
            with open(filename, 'w') as f:
                f.write(fc)
        except:
            logging.exception("efp_info.xml")
            return False
        return True
    
    @classmethod
    def _markComplete(cls, dataset, config):
        '''Adds file to mark this dataset as complete'''
        
        try:
            completefile = os.path.join(config['datadirectory'], dataset.uid, config['completefilename'])
            
            with open(completefile, 'a'):
                return True
        except:
            return False



# class DatasetMultiplier(object):
#     '''Wraps a Dataset object and multiplies position attributes by multiplier'''
#     def __init__(self, dataset, multiplier):
#         self._dataset = dataset
#         self._multiplier = multiplier
#     def __getattr__(self, key):
#         return getattr(self._dataset, key)


class EFPCompute(object):
    '''Changes dataset data-structure for efp config files'''
    
    def __init__(self, dataset, hires=False):
        self.dataset = dataset
        self.hires = hires
        
    def __getattr__(self, key):
        '''Get attributes from dataset instead'''
        if self.hires:
            if key == "graphwidth":
                return int((self.dataset.graphright - self.dataset.graphleft) * self.dataset.efpprintimgmultiplier)
            if key == "graphheight":
                return int((self.dataset.graphbottom - self.dataset.graphtop) * self.dataset.efpprintimgmultiplier)
            if key == "graphtop":
                return int(self.dataset.graphtop * self.dataset.efpprintimgmultiplier)
            if key == "graphleft":
                return int(self.dataset.graphleft * self.dataset.efpprintimgmultiplier)
            if key == "graphbottom":
                return int(self.dataset.graphbottom * self.dataset.efpprintimgmultiplier)
            if key == "graphright":
                return int(self.dataset.graphright * self.dataset.efpprintimgmultiplier)
            if key == "legendleft":
                return int(self.dataset.legendleft * self.dataset.efpprintimgmultiplier)
            if key == "legendtop":
                return int(self.dataset.legendtop * self.dataset.efpprintimgmultiplier)
            if key == "legendsize":
                return int(12 * self.dataset.efpprintimgmultiplier)
        else:
            if key == "graphwidth":
                return self.dataset.graphright - self.dataset.graphleft
            if key == "graphheight":
                return self.dataset.graphbottom - self.dataset.graphtop
            if key == "legendsize":
                return 12
        
        return getattr(self.dataset, key)
    
#     def graphwidth(self):
#         if self.hires:
#             return int((self.dataset.graphright - self.dataset.graphleft) * self.dataset.efpprintimgmultiplier)
#         return self.dataset.graphright - self.dataset.graphleft
#     def graphheight(self):
#         if self.hires:
#             return int((self.dataset.graphbottom - self.dataset.graphtop) * self.dataset.efpprintimgmultiplier)
#         return self.dataset.graphbottom - self.dataset.graphtop
#     def graphleft(self):
#         if self.hires:
#             return int(self.dataset.graphleft * self.dataset.efpprintimgmultiplier)
#         return self.dataset.graphleft
#     def graphbottom(self):
#         if self.hires:
#             return int(self.dataset.graphbottom * self.dataset.efpprintimgmultiplier)
#         return self.dataset.graphbottom
#     def legendleft(self):
#         if self.hires:
#             return int(self.dataset.legendleft * self.dataset.efpprintimgmultiplier)
#         return self.dataset.legendleft
#     def legendtop(self):
#         if self.hires:
#             return int(self.dataset.legendtop * self.dataset.efpprintimgmultiplier)
#         return self.dataset.legendtop
    
    def length(self, l):
        if self.hires:
            return int(l * self.dataset.efpprintimgmultiplier)
        return l
    
    def groups(self):
        return [
            {
                'name': 'ALL',
                'samples': self.dataset.sample_set.filter(deleted__isnull=True, efp_colour__isnull=False, efp_coords__isnull=False).exclude(efp_coords="", efp_colour="")
            }
        ]
    def controlsample(self):
        try:
            return self.dataset.sample_set.filter(is_control=True,deleted__isnull=True)[0]
        except:
            return None
        
        
