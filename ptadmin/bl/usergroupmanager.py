'''
Created on 11Apr.,2017

@author: arobinson
'''

from django.contrib.auth.models import User, Group
from django.utils.translation import ugettext_noop

import logging
from AgriBioHVC import settings
from django.db.utils import IntegrityError

class UserGroupManager(object):
    '''Business logic for User and Group management'''
    
    @classmethod
    def getUsers(cls):
        '''Gets a list of all users which can be added to a lab-group'''
        
        return User.objects.filter(is_superuser=False)
    
    @classmethod
    def getGroups(cls):
        '''Gets a list of all lab-groups in system'''
        
        return Group.objects.filter(name__iendswith="lab").order_by('name')
    
    @classmethod
    def createGroup(cls, name):
        '''
        Creates a new lab-group
        
        @param name: name of group to add, must end with 'Lab' and should only contain letters and dash(-)
        '''
        
        if name.lower().endswith('lab'):
            try:
                Group(name=name).save()
                return True
            except IntegrityError:
                return ugettext_noop("GROUP_EXISTS")
            except:
                logging.exception("Group Create")
                # Translators: Error when group save failed.  Might be it already exists
                return ugettext_noop("GROUP_SAVE_FAILED")
        return ugettext_noop("GROUP_NOT_ENDING_LAB")
    
    @classmethod
    def createUser(cls, username, firstname, lastname, email, domain):
        '''
        Creates a new user in the database.  Note: this only works for active 
        directory usernames
        
        @param username: username of user
        @param firstname: 
        @param lastname: 
        @param email: 
        @param domain: string, must match a configured domain in settings.ACTIVEDIRECTORYDOMAINS
        @return: mixed, True if successful, 'UNKNOWN_DOMAIN' if unknown domain, 'USER_FAILED' if failed to create user in database (maybe existing username) 
        '''
        
        try:
            domaincfg = settings.ACTIVEDIRECTORYDOMAINS[domain]
        except:
            # Translators: domain not specified in settings.ACTIVEDIRECTORYDOMAINS (as dictionary key)
            return ugettext_noop("USER_UNKNOWN_DOMAIN")
        
        try:
            usernamedom = "%s@%s" % (username, domaincfg[0])
            
            user = User(username=usernamedom, first_name=firstname, last_name=lastname, email=email)
            user.set_unusable_password()
            user.save()
            return True
        except IntegrityError:
            return ugettext_noop("USER_EXISTS")
        except:
            logging.exception("User Create")
        
        # Translators: Saving to database failed
        return ugettext_noop("USER_SAVE_FAILED")
    
    @classmethod
    def addUsersToGroup(cls, groupID, userIDs):
        '''Adds a list of user ID's to a group (by ID)'''
        
        try:
            group = Group.objects.get(id=groupID)
        except:
            logging.exception("GROUP_NOT_FOUND")
            return ugettext_noop("GROUP_NOT_FOUND")
        
        for uid in userIDs:
            try:
                group.user_set.add(uid)
            except IntegrityError:
                pass
        
        return True
            
    @classmethod
    def removeUserFromGroup(cls, groupID, userID):
        '''Removes a user (by ID) from a group (by ID)'''
        
        try:
            group = Group.objects.get(id=groupID)
        except:
            logging.exception("GROUP_NOT_FOUND")
            return ugettext_noop("GROUP_NOT_FOUND")
        
        try:
            group.user_set.remove(userID)
        except:
            logging.exception("USER_NOT_IN_GROUP")
            return ugettext_noop("USER_NOT_IN_GROUP")
        
        return True
        
    
