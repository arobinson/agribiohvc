'''
Created on 23 Jan 2017

@author: arobinson
'''
import datetime
import json
import logging
import os
import re

import ptadmin.bl
import csv

from common.uid import DATASETUID, DATASETUID_LABEL

UIDREGEX = re.compile(r'^'+DATASETUID+r'$')
NAMEREGEX = re.compile(r'[^ \-._a-zA-Z0-9]')
HEXREGEX = re.compile(r'[^a-fA-F0-9]')

class DegustManager():
    '''Creates and Manages dataset files/config within Degust'''

    @classmethod
    def createDataset(cls, dataset, config, override=False):
        '''
        Creates a new dataset in eFP
        
        @param dataset: dataset instance representing the dataset to create
        @param config: dictionary of system configuration settings
        @param override: if False function will return error if dataset already exists
        
        @return: (False, REASON) = error occurred, (True, None) = Success
        '''
        
        # check if it exists already
        if not override and cls._exists(dataset, config):
            return (False, 'EXISTS')
        
        # clean output files
        if not cls._cleanOutputDir(dataset, config):
            return (False, 'CLEANDATA')
        
        # clean output files
        if not cls._createDatasetSettingsJSON(dataset, config):
            return (False, 'SETTINGS')
        
        # clean output files
        if not cls._createDatasetCountsCSV(dataset, config):
            return (False, 'COUNTS')
        
        # mark as complete
        if not cls._markComplete(dataset, config):
            return (False, 'COMPLETEFILE')
        
        return (True, None)
    
    @classmethod
    def datasetValid(cls, dataset, config):
        '''
        Checks if a dataset has the required values for Degust deployment
        
        @return: True or list
        '''
                
        errors = []
        
        cls._validateUID(errors, dataset, 'uid')
        cls._validateName(errors, dataset, 'name')
        
        cls._validateReverseForeignKeyCount(errors, dataset.sample_set.filter(deleted__isnull=True), 'samples', 2)
        cls._validateReverseForeignKeyCount(errors, dataset.sample_set.filter(deleted__isnull=True, is_control=True), 'samples-control', 1)
        cls._validateReverseForeignKeyCount(errors, dataset.sample_set.filter(deleted__isnull=True, is_control=False), 'samples-other', 1)
        
        cls._validateCountsFormat(errors, dataset, 'countsformat')
        
        for sample in dataset.sample_set.filter(deleted__isnull=True).order_by('order'):
            cls._validateName(errors, sample, 'name')
            # TODO: check for at least 2 replicates
        
        if len(errors) > 0:
            return errors
        return True
    
    
    
    #######################################################################################################
    @classmethod
    def _validateNotNull(cls, errors, obj, field1):
        '''Makes sure field1 has a value (non-whitespace)'''
        val = getattr(obj, field1)
        if val is None:
            errors.append((field1, '{field1} must have a value'))
            return False
        return True
    
    @classmethod
    def _validateReverseForeignKeyCount(cls, errors, obj, field1, count):
        '''Checks resultset has at least count values'''
        if obj is None or obj.count() < count:
            errors.append((field1, '{field1} must have at least ' + str(count) + ' values'))
            return False
        return True
    
#     @classmethod
#     def _validateMinLength(cls, errors, obj, field1, length):
#         '''Checks resultset has at least count values'''
#         if obj is None or len(obj) < length:
#             errors.append((field1, '{field1} must have a at least ' + length + ' values'))
#             return False
#         return True
    
    @classmethod
    def _validateCountsFormat(cls, errors, obj, uploadcountfield):
        '''Makes sure counts format can be generated'''
        
        uploadcountsformat = getattr(obj, uploadcountfield)
        
        if uploadcountsformat != 'RAW':
            errors.append((uploadcountfield, 'Upload format must be RAW if you wish to deploy to Degust (as it uses edgeR which requires unnormalised counts)'))
    
    @classmethod
    def _validateUID(cls, errors, obj, field1):
        '''Checks field1 only contains [0-9a-zA-Z] characters'''
        val = getattr(obj, field1)
        if not bool(UIDREGEX.search(val)):
            errors.append((field1, '{field1} contains invalid characters; ' + DATASETUID_LABEL))
            return False
        return True
    
    @classmethod
    def _validateName(cls, errors, obj, field1):
        '''Checks field1 only contains [0-9a-zA-Z] characters'''
        val = getattr(obj, field1)
        if bool(NAMEREGEX.search(val)): # checks for existence of not allowed characters
            errors.append((field1, '{field1} contains invalid characters; it must only include letters, numbers, space, dash (-) or underscore (_).'))
            return False
        return True
    
    #######################################################################################################
    
    @classmethod
    def _exists(cls, dataset, config):
        '''Checks if dataset already exists in eFP'''
        
        datadir = os.path.join(config['datadirectory'], dataset.uid)
        
        return os.path.exists(datadir)
    
    @classmethod
    def _cleanOutputDir(cls, dataset, config):
        '''Removes previous output files if they exist'''
         
        try:
            files = [
                    os.path.join(config['datadirectory'], dataset.uid),
                    os.path.join(config['datadirectory'], dataset.uid + "-settings.js"),
                    os.path.join(config['datadirectory'], dataset.uid + "-counts.csv"),
                ]
            for f in files:
                if os.path.exists(f):
                    os.remove(f)
        except:
            return False
        return True
        
    @classmethod
    def _createDatasetSettingsJSON(cls, dataset, config):
        '''Creates a DATASET-settings.js file contents'''
        
        # generate default
        content = {
            "code" : dataset.uid,
            "remote_addr" : config['serveraddress'],
            "created" : "%s UTC" % datetime.datetime.utcnow(),
            "user_settings" : {
                "fdr_column" : "",
                "analyze_server_side" : True,
                "avg_column" : "",
                "primary_name" : "",
                "fc_columns" : [],
                "min_counts" : 100,
                "name": dataset.name,
                "hide_columns" : [],
                "init_select" : [],
                "skip" : 0,
                "csv_format" : False,
                "replicates" : [],
                "link_url" : "%s/feature/%s/%%s" % (config['landingbaseurl'], dataset.uid),
                "link_column" : dataset.idcolumn,
                "info_columns" : dataset.annotationcolumns,
                "fdrThreshold": "0.01",
                "fcThreshold": "1",
            },
            "locked" : True,
        }
        for sample in dataset.sample_set.filter(deleted__isnull=True).all():
            content['user_settings']['replicates'].append([sample.name, sample.replicates])
            content['user_settings']['init_select'].append(sample.name)
        if dataset.idcolumn not in content['user_settings']['info_columns']:
            content['user_settings']['info_columns'].insert(0, dataset.idcolumn)
        
        # Apply overrides
        content.update(cls._getConf(dataset.degustconfigoverride, content))
        
        # convert to JSON
        fc = json.dumps(content)
        
        # write content
        try:
            filename = os.path.join(config['datadirectory'], dataset.uid + '-settings.js')
            with open(filename, 'w') as f:
                f.write(fc)
        except:
            logging.exception("settings.json")
            return False
        return True
    
    @classmethod
    def _getConf(cls, code, CONF={}):
        '''Execute code and return CONF variable'''
        
        g = {'__builtins__': {}, 'False': False, 'True': True}
        l = {'CONF': CONF}
        
        exec(code, g, l)
        
        return l['CONF']
        
    @classmethod
    def _createDatasetCountsCSV(cls, dataset, config):
        '''Creates a DATASET-counts.csv file contents'''
        
        try:
            uploaddir = os.path.join(config['datasetuploadsdirectory'], dataset.uid)
            infilename = os.path.join(uploaddir, dataset.countsfile)
            outfilename = os.path.join(config['datadirectory'], dataset.uid+ '-counts.csv')
            
            with open(infilename) as infile:
                reader = ptadmin.bl.datamanager.DataManager._filereader(infile)
                with open(outfilename, 'w') as outfile:
                    writer = csv.DictWriter(outfile, reader.fieldnames, dialect='excel-tab')
                    writer.writeheader()
                    for row in reader:
                        writer.writerow(row)
        except:
            logging.exception("counts.csv")
            return False
        return True
    
    @classmethod
    def _markComplete(cls, dataset, config):
        '''Adds file to mark this dataset as complete'''
        
        try:
            completefile = os.path.join(config['datadirectory'], dataset.uid)
            
            with open(completefile, 'a'):
                return True
        except:
            return False
