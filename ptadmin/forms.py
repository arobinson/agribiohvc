'''
Created on 23 Jan 2017

@author: arobinson
'''

from django import forms
from django.core.validators import RegexValidator

import common.models
import logging
from common import models
from AgriBioHVC import settings
from django.contrib.auth.models import User
from django.template import loader
from django.template.context import Context

#### BASE FORMS ####
class Form(forms.Form):
    '''Base class for regular forms'''
    
    def dirty_data(self, POST):
        '''Gets the dirty values for each field'''
        result = {'__form__': self.__class__.__name__}
        for f in self.fields:
            result[f] = POST.get(f, '')
        return result
    
    
class ModelForm(forms.ModelForm):
    '''Base class for regular forms'''
    
    def dirty_data(self, POST):
        '''Gets the dirty values for each field'''
        result = {'__form__': self.__class__.__name__}
        for f in self.fields:
            result[f] = POST.get(f, '')
        return result
    
    
class BootstrapForm(Form):
    '''Form that adds functions to render nicely as a bootstrap form'''
    
    def __init__(self, *args, **kwargs):
        bootstrap_style = kwargs.pop('bootstrap_style', 'common/form/bootstrap-form2-10.html')
        
        super(BootstrapForm, self).__init__(*args, **kwargs)
        
        self._bootstrap_style = bootstrap_style
        self._bootstrapped = False
        
    def _fixFields(self):
        '''Apply bootstrap styles for fields'''
        if not self._bootstrapped:
            self._bootstrapped = True
            
            for field in self.fields.values():
                fieldtype = type(field.widget).__name__
                if fieldtype in ('SelectMultiple','ChoiceField','IntegerField','TextInput','Textarea','EmailInput','Select','DateTimeInput','NumberInput',):
                    if 'class' in field.widget.attrs:
                        field.widget.attrs['class'] += " form-control"
                    else:
                        field.widget.attrs['class'] = "form-control"
                elif fieldtype not in ('RadioSelect','ClearableFileInput',):
                    print fieldtype
        
    def as_bootstrap(self):
        '''Renders the from with bootstrap3 styling'''
        
        self._fixFields()
        
        t = loader.get_template(self._bootstrap_style)
        c = Context({'form': self})
        return t.render(c)
    
    
class BootstrapModelForm(ModelForm):
    '''Form that adds functions to render nicely as a bootstrap form'''
    
    def __init__(self, *args, **kwargs):
        bootstrap_style = kwargs.pop('bootstrap_style', 'common/form/bootstrap-form2-10.html')
        
        super(BootstrapModelForm, self).__init__(*args, **kwargs)
        
        self._bootstrap_style = bootstrap_style
        self._bootstrapped = False
        
    def _fixFields(self):
        '''Apply bootstrap styles for fields'''
        if not self._bootstrapped:
            self._bootstrapped = True
            
            for field in self.fields.values():
                fieldtype = type(field.widget).__name__
                if fieldtype in ('SelectMultiple','ChoiceField','IntegerField','TextInput','Textarea','EmailInput','Select','DateTimeInput','NumberInput',):
                    if 'class' in field.widget.attrs:
                        field.widget.attrs['class'] += " form-control"
                    else:
                        field.widget.attrs['class'] = "form-control"
                elif fieldtype not in ('RadioSelect','ClearableFileInput',):
                    print fieldtype
        
    def as_bootstrap(self):
        '''Renders the from with bootstrap3 styling'''
        
        self._fixFields()
        
        t = loader.get_template(self._bootstrap_style)
        c = Context({'form': self})
        return t.render(c)


#### INSTANCE FROMS ####
from common.uid import DATASETUID, DATASETUID_LABEL, ORGANISMUID, ORGANISMUID_LABEL

DSUIDValidator = RegexValidator(r'^'+DATASETUID+r'$', DATASETUID_LABEL)
ORGUIDValidator = RegexValidator(r'^'+ORGANISMUID+r'$', ORGANISMUID_LABEL)

class SpeciesForm(BootstrapForm):
    uid = forms.CharField(label='UID', validators=[ORGUIDValidator], help_text='Unique identifier for this Organism. '+ORGANISMUID_LABEL)
    name = forms.CharField(label='Name', help_text='User facing name for organism (included on landing portal).  Can use any characters but keep it short.')


class GroupForm(BootstrapForm):
    '''Add a new lab-group to database'''
    
    name = forms.CharField(label='Name', help_text='Name for the group.  Must end with "Lab"')


class UserForm(BootstrapForm):
    '''Adding a new (domain) user to the database'''
    
    domain = forms.ChoiceField(choices=((d,d) for d in settings.ACTIVEDIRECTORYDOMAINS), widget=forms.RadioSelect)
    username = forms.CharField(label='Username', help_text='')
    firstname = forms.CharField(label='First name', help_text='')
    lastname = forms.CharField(label='Last name', help_text='')
    email = forms.EmailField(label='Email address', help_text='')


class SelectUserForm(BootstrapForm):
    '''Select one or more users'''
    
    users = forms.MultipleChoiceField()

    def __init__(self, users, *args, **kwargs):
        super(SelectUserForm, self).__init__(*args, **kwargs)
        
        c = [(u.id,u.get_full_name()) for u in users]
        self.fields['users'] = forms.MultipleChoiceField(
            choices=c,
            help_text='Select new users to add to group.  Existing ones will remain.  Duplicates will be skipped.  Press Ctrl while clicking to select multiple.'
        )



class DatasetBasicForm(BootstrapForm):
    '''Form to edit basic settings for a dataset'''

    uid = forms.CharField(label='UID', validators=[DSUIDValidator], help_text='Unique identifier for this data-set. '+DATASETUID_LABEL)
    name = forms.CharField(label='Name', help_text='User facing name for data-set (included on landing portal).  Can use any characters but keep it short.')


class DataUploadForm(BootstrapForm):
    '''Form used to upload data for dataset'''
    
    countsfile = forms.FileField(help_text="File containing counts (and other annotation).  Samples in columns and Gene/transcripts in rows")
    countsformat = forms.ChoiceField(choices=models.COUNT_FORMAT_CHOICES)
    efpexprimage = forms.FileField(help_text="Expression image (for showing on screen) with colour coded sample areas")
    efphomeimage = forms.FileField(help_text="Plain image (for showing on screen) i.e. without colour coded sample areas")
    efpdownimage = forms.FileField(help_text="Expression image (for downloading image) with colour coded sample areas.  Recommend 2x width/height of screen image.  If none available use Expression image and enter 1 in multiplier")
    efpprintimgmultiplier = forms.FloatField(help_text="Scaling factor to get size of Download image from Expression image.  Use a whole number as expression graph render doesn't support decimals", initial=2)
    
    
    def __init__(self, *args, **kwargs):
        super(DataUploadForm, self).__init__(*args, **kwargs)
        
        self.fields['countsfile'].label = "Counts file"
        self.fields['countsformat'].label = "Count format"
        self.fields['efpexprimage'].label = "eFP expression image"
        self.fields['efphomeimage'].label = "eFP home image"
        self.fields['efpdownimage'].label = "eFP expression download image"
        self.fields['efpprintimgmultiplier'].label = "eFP download multiplier"


class DatasetEditForm(BootstrapModelForm):
    '''Form to edit all settings (except uid) for a dataset'''

    class Meta:
        model = common.models.Dataset
        fields = [
            'name',
            'publish',
            'countsdownload',
            'species',
            'reference',
            'labgroup',
            'collections',
            'idcolumn',
            'genelengthcolumn',
            'annotationcolumns',
            'externallinkurl',
            'externallinklabel',
            'externallinkregex',
            'externallinksub',
            'description_header',
            'description_footer',
            
            'efpcountsformat',
            'graphtop',
            'graphleft',
            'graphbottom',
            'graphright',
            'legendtop',
            'legendleft',
            'efpconfigoverride',
            'efpdescription',
            
            'degustconfigoverride',
            'degustdescription',
        ]
        
        
    def __init__(self, columns, *args, **kwargs):
        super(DatasetEditForm, self).__init__(*args, **kwargs)
        
        # dynamically load in column choices
        c = [(o,o) for o in columns]
        c.insert(0, ('','--'))
        ht=self.fields['idcolumn'].help_text
        self.fields['idcolumn'] = forms.ChoiceField(
            choices=c,
            help_text=ht
        )
        ht=self.fields['genelengthcolumn'].help_text
        self.fields['genelengthcolumn'] = forms.ChoiceField(
            choices=c,
            help_text=ht
        )
        ht=self.fields['annotationcolumns'].help_text
        self.fields['annotationcolumns'] = forms.MultipleChoiceField(
            choices=[(o,o) for o in columns],
            required=False,
            help_text=ht
        )
#         self.fields[''] = forms.DateTimeField(input_formats=('%Y-%m-%dT%H:%M:%S+0000',))
        
        self.fields['name'].widget.attrs = {'title':'Data-set name', 'placeholder': 'Data-set name'}
        self.fields['species'].widget.attrs = {'title':'Organism of data-set'}
        self.fields['species'].label = 'Organism'
        self.fields['reference'].widget.attrs = {'title':'Reference used in mapping'}
        self.fields['publish'].input_formats=('%d/%m/%Y %H:%M %p',)
        self.fields['countsdownload'].label='Downloadable'
        self.fields['labgroup'].widget.attrs = {'title':'Lab group who can access dataset before it is public'}
        self.fields['labgroup'].label = "Lab group"
        self.fields['idcolumn'].widget.attrs = {'title':'Column name containing Gene/Transcript IDs'}
        self.fields['idcolumn'].label = "ID column"
        self.fields['genelengthcolumn'].widget.attrs = {'title':'Column name containing Gene/Transcript Length (Required for converting RAW to TPM/RPKM/FPKM values)'}
        self.fields['genelengthcolumn'].label = "Gene length column"
        self.fields['genelengthcolumn'].required = False
        self.fields['annotationcolumns'].widget.attrs = {'title':'Columns that contain extra annotation for Gene/Transcript'}
        self.fields['annotationcolumns'].label = "Annotation columns"
        self.fields['description_header'].widget.attrs = {'title':'Markdown/HTML formatted text to display above samples on landing page', 'class':"medium"}
        self.fields['description_header'].label = "Description header"
        self.fields['description_footer'].widget.attrs = {'title':'Markdown/HTML formatted text to display under samples on landing page', 'class':"medium"}
        self.fields['description_footer'].label = "Description footer"
        self.fields['efpcountsformat'].widget.attrs = {'title':'Count format to use with eFP browser.  Must already be in this format OR in RAW format with gene length column specified.'}
        self.fields['efpcountsformat'].label = "Counts format"
        self.fields['graphtop'].label = "Top"
        self.fields['graphleft'].label = "Left"
        self.fields['graphbottom'].label = "Bottom"
        self.fields['graphright'].label = "Right"
        self.fields['legendtop'].label = "Top"
        self.fields['legendleft'].label = "Left"
        self.fields['efpconfigoverride'].widget.attrs = {'title':'(Advanced) Python code to override eFP settings', 'class':"medium"}
        self.fields['efpconfigoverride'].label = "Setting overrides"
        self.fields['degustconfigoverride'].widget.attrs = {'title':'(Advanced) Python code to override Degust settings', 'class':"medium"}
        self.fields['degustconfigoverride'].label = "Setting overrides"
        
#         self.fields['annotationcolumns'].choices=[(o,o) for o in columns]
        self._fixFields()

    def dirty_data(self, POST):
        '''Gets the dirty values for each field'''
        result = {'__form__': self.__class__.__name__}
        for f in self.fields:
            result[f] = POST.get(f, '')
        return result


class SampleModelForm(forms.ModelForm):
    '''Form used for editing a single Sample (used within a FormSet)'''
    
#     name = forms.CharField(max_length=50, widget=forms.TextInput(attrs={'title':'Sample name', 'placeholder': 'Sample name', 'class':"form-control"}))
#     is_control = forms.BooleanField(required=False)
#     efp_colour = forms.CharField(max_length=7, widget=forms.TextInput(attrs={'title':'eFP Colour', 'placeholder': 'eFP Colour', 'class':"form-control"}))
#     efp_coords = forms.CharField(widget=forms.TextInput(attrs={'title':'eFP sample coordinates', 'placeholder': 'eFP sample coordinates', 'class':"form-control"}))
#     replicates = forms.MultipleChoiceField()
#     description = forms.CharField(required=False, widget=forms.Textarea(attrs={'title':'Description of Sample preparation/treatment', 'placeholder': 'Description', 'class':"form-control short"}))
    
    def __init__(self, *args, **kwargs):
        columns = kwargs.pop('columns')
        super(SampleModelForm, self).__init__(*args, **kwargs)
        
        # dynamically load in column choices
        self.fields['replicates'] = forms.MultipleChoiceField(
            choices=[(o,o) for o in columns],
            widget=forms.SelectMultiple(attrs={'title':'Replicates for this sample', 'class':"form-control short"}),
        )
        
        # insert html attributes
        self.fields['name'].widget.attrs = {'title':'Sample name', 'placeholder': 'Sample name', 'class':"form-control"}
        self.fields['efp_colour'].widget.attrs = {'title':'eFP Colour', 'placeholder': 'eFP Colour', 'class':"form-control"}
        self.fields['efp_coords'].widget.attrs = {'title':'eFP sample coordinates', 'placeholder': 'eFP sample coordinates', 'class':"form-control"}
        self.fields['description'].widget.attrs = {'title':'Description of Sample preparation/treatment', 'placeholder': 'Description', 'class':"form-control short"}

    def dirty_data(self, POST):
        '''Gets the dirty values for each field'''
        result = {'__form__': self.__class__.__name__}
        for f in self.fields:
            result[f] = POST.get(f, '')
        return result

class EFPLinkForm(forms.Form):
    '''Form used in a formset for collecting links on eFP image'''
    
#     <extra name="{{l.name}}" link="{{l.link}}"
#             parameters="{{l.parameters}}" coords="{{l.coords}}" />

    name = forms.CharField(max_length=100, required=True)
    link = forms.URLField(required=True)
    params = forms.BooleanField(required=False)
    coords = forms.CharField(max_length=250, required=True)
    
    
    def __init__(self, *args, **kwargs):
        super(EFPLinkForm, self).__init__(*args, **kwargs)
        
        # insert html attributes
        self.fields['name'].widget.attrs = {'title':'Link name', 'placeholder': 'Link name', 'class':"form-control"}
        self.fields['link'].widget.attrs = {'title':'URL', 'placeholder': 'URL', 'class':"form-control"}
        self.fields['params'].widget.attrs = {'title':'URL uses parameters'}
        self.fields['coords'].widget.attrs = {'title':'eFP link coordinates', 'placeholder': 'eFP link coordinates', 'class':"form-control"}

# class DatasetEditForm(forms.Form):
#     '''Form to edit all settings (except uid) for a dataset'''
# 
#     name = forms.CharField(label='Name')
#     publish = forms.DateTimeField(label='Publish date')
# 
#     def dirty_data(self, POST):
#         '''Gets the dirty values for each field'''
#         result = {'__form__': self.__class__.__name__}
#         for f in self.fields:
#             result[f] = POST.get(f, '')
#         return result

class ReferenceForm(BootstrapModelForm):
    '''Form to edit all settings reference'''
    
#     gfffile = forms.FileField(help_text='GFF or FastA file of features')
#     gfffeatures = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple(), choices=common.models.REFERENCE_FEATURES_CHOICES, help_text='GFF features to use (in eFP)')
    
    class Meta:
        model = common.models.Reference
        fields = [
            'uid',
            'name',
            'type',
            'description',
#             'species',
#             'gfffile',
#             'gfffeatures',
        ]
        
    def __init__(self, *args, **kwargs):
        super(ReferenceForm, self).__init__(*args, **kwargs)
        
#         self.fields['gfffile'].label = "GFF/FastA file"
#         self.fields['gfffeatures'].label = "GFF features"
        
        
    def dirty_data(self, POST):
        '''Gets the dirty values for each field'''
        result = {'__form__': self.__class__.__name__}
        for f in self.fields:
            result[f] = POST.get(f, '')
        return result

class ReferenceUploadForm(BootstrapForm):
    '''Form to upload features for reference'''

    featurefile = forms.FileField(help_text='Tab-delimited (TSV) or Comma-delimited (CSV) file of features (NAME, DESCRIPTION columns, with no heading row)')
#     gfffeatures = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple(), choices=common.models.REFERENCE_FEATURES_CHOICES, help_text='GFF features to use (in eFP)')
    
    def __init__(self, *args, **kwargs):
        super(ReferenceUploadForm, self).__init__(*args, **kwargs)
        
        self.fields['featurefile'].label = "TSV feature file"
#         self.fields['gfffeatures'].label = "GFF features"

