from __future__ import unicode_literals

from django.db import models


class TemplateGroup(models.Model):
    '''
    A group of Template operations to create a particular style for this application
    '''
    
    name = models.CharField(max_length=20)
    
    def __str__(self):
        return str(self.name)

class TemplateOperation(models.Model):
    '''
    A processing operation used to extract the template html/css from a website and
    save its value within the database for styling pages in this application
    '''
    
    function = models.CharField(max_length=500)
    args = models.TextField()
    order = models.IntegerField()
    group = models.ForeignKey(TemplateGroup)
    
    enabled = models.BooleanField(default = True)
    
    def __str__(self):
        return "%s(%s,...)" % (self.function, self.order)
