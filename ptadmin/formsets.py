'''
Created on 27Jan.,2017

@author: arobinson
'''
from django.core.exceptions import ValidationError
from django.forms.formsets import BaseFormSet


#     sample_name = forms.CharField(max_length=50)
#     is_control = forms.CheckboxInput()
#     efp_colour = forms.CharField(max_length=7)
#     replicates = forms.MultipleChoiceField()

class BaseSampleFormSet(BaseFormSet):
    def clean(self):
        """
        Adds validation to check samples are filled correctly
        """
        if any(self.errors):
            return
        
        has_control = False

        for form in self.forms:
            if form.cleaned_data:
                sample_name = form.cleaned_data['sample_name']
                is_control = form.cleaned_data['is_control']
                efp_colour = form.cleaned_data['efp_colour']
                replicates = form.cleaned_data['replicates']
                
                # check for multiple controls
                if is_control:
                    if has_control:
                        raise ValidationError('You must only have 1 control sample', code='duplicate_control')
                    has_control = True
                
                # check for partial records
                if (sample_name or efp_colour or replicates) and not (sample_name and efp_colour and replicates):
                    raise ValidationError('Sample name, eFP Colour and replicates are required', code='missing_data')
            
