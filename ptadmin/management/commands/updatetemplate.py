'''
Created on 11 Apr 2016

@author: arobinson
'''

import sys

from django.core.management.base import BaseCommand
from ptadmin.bl import templatemanager

class Command(BaseCommand):
    help = 'Updates the template used for the user facing pages'

    def handle(self, *args, **options):
        
        if len(args) > 1:
            sys.stderr.write("Usage: updatetemplate [templatename]\n")
            return
        
        if len(args) == 0:
            style = 'ltu'
        else:
            style = args[0]

        if templatemanager.TemplateManager.updateTemplate(style):
            print "Successfully update template to '%s'" % style
        else:
            print "Failed to update template to '%s'" % style



