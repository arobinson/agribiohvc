"""
Admin portal URL Configuration

Namespace: 'ptadmin'
"""
from django.conf.urls import url

import ptadmin.views.dataset
import ptadmin.views.organism
import ptadmin.views.system
import ptadmin.views.template
import ptadmin.views.usergroup

from common.uid import ORGANISMUID, DATASETUID, REFERENCEUID

urlpatterns = [
    
    url(r'^$', ptadmin.views.dataset.home, name='home'),
    
    # /dataset/DATASETUID/*
    url(r'^dataset$', ptadmin.views.dataset.list, name='listdataset'),
    url(r'^dataset/new$', ptadmin.views.dataset.newdataset, name='newdataset'),
    url(r'^dataset/('+DATASETUID+r')/upload$', ptadmin.views.dataset.upload, name='uploaddataset'),
    url(r'^dataset/('+DATASETUID+r')/edit$', ptadmin.views.dataset.edit, name='editdataset'),
    url(r'^dataset/('+DATASETUID+r')/efpimage$', ptadmin.views.dataset.efpimage, name='efpimage'),
    url(r'^dataset/('+DATASETUID+r')/deployefp$', ptadmin.views.dataset.deployefp, name='deployefp'),
    url(r'^dataset/('+DATASETUID+r')/deploydegust$', ptadmin.views.dataset.deploydegust, name='deploydegust'),
    
    # /organism/ORGANISMUID
    url(r'^organism$', ptadmin.views.organism.listOrganism, name='listorganism'),
    url(r'^organism/add$', ptadmin.views.organism.addOrganism, name='organismadd'),
    url(r'^organism/add-old$', ptadmin.views.organism.addOrganism, name='speciesadd'), # deprecated
    url(r'^organism/('+ORGANISMUID+r')$', ptadmin.views.organism.detailOrganism, name='detailorganism'),
    
    # /organism/ORGANISMUID/reference/REFERENCEUID/*
    url(r'^organism/('+ORGANISMUID+r')/reference/add$', ptadmin.views.organism.addReference, name='referenceadd'),
    url(r'^organism/('+ORGANISMUID+r')/reference/('+REFERENCEUID+r')/upload$', ptadmin.views.organism.uploadReferenceFeatures, name='referenceupload'),
    url(r'^organism/('+ORGANISMUID+r')/reference/('+REFERENCEUID+r')/deployefp$', ptadmin.views.organism.deployreferenceefp, name='deployreferenceefp'),
    url(r'^organism/('+ORGANISMUID+r')/reference/('+REFERENCEUID+r')/deploylanding$', ptadmin.views.organism.deployreferencelanding, name='deployreferencelanding'),
    
    url(r'^group$', ptadmin.views.usergroup.listgroup, name='listgroup'),
    url(r'^group/new$', ptadmin.views.usergroup.newgroup, name='newgroup'),
    url(r'^user/new$', ptadmin.views.usergroup.newuser, name='newuser'),
    url(r'^group/([0-9]+)/addusers$', ptadmin.views.usergroup.addusergroup, name='addusergroup'),
    url(r'^group/([0-9]+)/removeuser/([0-9]+)$', ptadmin.views.usergroup.removeusergroup, name='removeusergroup'),
    
    url(r'^view-template$', ptadmin.views.template.viewTemplate, name='viewTemplate'),
    url(r'^update-template$', ptadmin.views.template.updateTemplate, name='updateTemplate'),
    
    url(r'^colour-chart$', ptadmin.views.dataset.colourChart, name='colourChart'),
    
    url(r'^system/status$', ptadmin.views.system.status, name='systemstatus'),
#     url(r'^view$', ptadmin.views.view, name='view'),
#     url(r'^.*$', RedirectView.as_view(pattern_name='home', permanent=False)),
]
