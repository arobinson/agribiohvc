"""
AgriBioHVC (Top-level) URL Configuration
"""
from django.conf.urls import include, url
from django.contrib import admin as djadmin
from django.views.generic.base import RedirectView

import common.views
import efpbrowser.urls
import degustbrowser.urls
#import geneview.urls
import landing.urls
import ptadmin.urls
import background.urls
from AgriBioHVC import settings

urlpatterns = [
    
    url(r'^efp/', include(efpbrowser.urls, namespace='efp')),
    url(r'^degust/', include(degustbrowser.urls, namespace='degust')),
    url(r'^portal-admin/', include(ptadmin.urls, namespace='ptadmin')),
    url(r'^portal-admin$', RedirectView.as_view(url='portal-admin/', permanent=False)),
    url(r'^background/', include(background.urls, namespace='background')),
    
    url(r'^django-admin/', djadmin.site.urls),
    url(r'^django-admin$', RedirectView.as_view(url='django-admin/', permanent=False)),
    
    url(r'^login', common.views.login_user, name='login'),
    url(r'^logout', common.views.logout_user, name='logout'),
    
    url(r'^content/([^/]*)', common.views.content, name='content'),
    url(settings.DATA_URL, common.views.data_server, name='content'),
    
    url(r'^status', common.views.status, name='status'),
]
urlpatterns.extend(landing.urls.urlpatterns)
