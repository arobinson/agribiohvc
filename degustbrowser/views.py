import os
import sys
import logging

from django.core.exceptions import ObjectDoesNotExist
from django.http.response import HttpResponse, Http404, HttpResponseForbidden
from django.shortcuts import render
from django.template.context_processors import csrf
from django.urls.base import reverse
from django_cgi_wrap import cgi_wrap

import common
from AgriBioHVC import settings
from common.bl.messagemanager import MessageManager
from common.bl.datamanager import DataManager
from landing.views import _parentList


def home(request, organismuid, datasetuid, collectionuid=None):
    '''Renders the Degust browser (home view) inside our template'''
    
    # load data objects
    mycollectionuid = collectionuid
    if not collectionuid:
        mycollectionuid = settings.DEFAULT_COLLECTION
    try:
        collection = common.models.Collection.objects.get(uid=mycollectionuid)
        organism = common.models.Species.objects.get(uid=organismuid, deleted__isnull=True)
        dataset = organism.dataset_set.get(uid=datasetuid, deleted__isnull=True)
    except ObjectDoesNotExist:
        raise Http404
    
    # check user is (not) allowed
    if not DataManager.canViewDataset(dataset, request.user):
        return HttpResponseForbidden()
    
    # otherwise allow the request
    params = {
              'request': request,
              'messages': MessageManager.getMessages(request),
              'group': 'efp',
              'parents': _parentList(collection, not collectionuid,
                  ('%s datasets' % organism.name, reverse('datasetlist', kwargs={'collectionuid': collectionuid, 'organismuid': organism.uid,})),
                  (dataset.name, reverse('datasetdetails', kwargs={'collectionuid': collectionuid, 'organismuid': organism.uid, 'datasetuid': dataset.uid,})),
              ),
              'page': settings.DEGUST_COMMON_NAME, 
              'children': [ ],
              'organism': organism,
              'dataset': dataset,
              'settings': settings,
              'title': '%s - %s'%(dataset.name,settings.DEGUST_COMMON_NAME),
              'collection': collection,
             }
    params.update(csrf(request))
    
    return render(request, "degust/home.html", params)


def compare(request, speciesuid, datasetuid):
    '''Renders the Degust browser (home view) inside our template'''
    
    # load data objects
    dataset = common.models.Dataset.objects.get(uid=request.GET.get('code'), deleted__isnull=True)
    species = dataset.species
    
    # check user is (not) allowed
    if not DataManager.canViewDataset(dataset, request.user):
        return HttpResponseForbidden()
    
    # otherwise allow the request
    params = {
              'request': request,
              'messages': MessageManager.getMessages(request),
              'group': 'efp',
              'parents': [
                  settings.HOME_LINK,
                  (settings.PORTAL_NAME, reverse('home')),
                  ('%s datasets' % species.name, reverse('datasetlist', args=(species.uid, ))),
                  (dataset.name, reverse('datasetdetails', args=(species.uid, dataset.uid))),
              ],
              'page': "Degust Browser", 
              'children': [ ],
              'species': species,
              'dataset': dataset,
              'settings': settings,
              'title': '',
             }
    params.update(csrf(request))
    
    return render(request, "degust/compare-plain.html", params)


def rjson(request, speciesuid, datasetuid):
    '''Runs r-json.cgi file for selected dataset'''
    
    # load data objects
    dataset = common.models.Dataset.objects.get(uid=request.GET.get('code'), deleted__isnull=True)
    
    # check user is (not) allowed
    if not DataManager.canViewDataset(dataset, request.user):
        return HttpResponseForbidden()
    
    # otherwise allow the request
    return cgi_wrap(request, 
                    os.path.join(settings.VIEWERSETTINGS['degust']['builddir'], 'r-json.cgi'),
                    cwd=settings.VIEWERSETTINGS['degust']['builddir'])


MIMEEXTS = {
    'js': 'text/javascript',
    'css': 'text/css',
    'png': 'image/png',
    'gif': 'image/gif',
    'jpg': 'image/jpeg',
    'html': 'text/html',
}

def static(request, filename):
    '''Serves static files'''
    
    ext = str(filename).split('.')[-1]
    
    if ext in MIMEEXTS:
        try:
            with open(os.path.join(settings.VIEWERSETTINGS['degust']['staticdirectory'], filename)) as f:
                return HttpResponse(f.read(), content_type=MIMEEXTS[ext])
        except:
            pass
        
    raise Http404

