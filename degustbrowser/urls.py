"""
Landing portal URL Configuration

Namespace: degust
"""
from django.conf.urls import url

import degustbrowser.views

from common.uid import COLLECTIONUID, ORGANISMUID, DATASETUID

urlpatterns = [
    
    # /ORGANISMUID/DATASETUID/*
    url(r'^(?:(?P<collectionuid>'+COLLECTIONUID+r')/)?(?P<organismuid>'+ORGANISMUID+r')/(?P<datasetuid>'+DATASETUID+r')/$', degustbrowser.views.home, name='home'),
    url(r'^('+ORGANISMUID+r')/('+DATASETUID+r')/compare.html$', degustbrowser.views.compare, name='compare'),
    url(r'^('+ORGANISMUID+r')/('+DATASETUID+r')/r-json.cgi$', degustbrowser.views.rjson, name='r-json'),
    
    # /ORGANISMUID/DATASETUID/FILENAME
    url(r'^'+ORGANISMUID+r'/'+DATASETUID+r'/(css/[a-z]+.css)$', degustbrowser.views.static, name='staticcss'),
    url(r'^'+ORGANISMUID+r'/'+DATASETUID+r'/([a-z]+.js)$', degustbrowser.views.static, name='staticjs'),
    url(r'^'+ORGANISMUID+r'/'+DATASETUID+r'/(images/[-a-z]+.[pngif]{3})$', degustbrowser.views.static, name='staticimg'),
    url(r'^'+ORGANISMUID+r'/'+DATASETUID+r'/(css/images/[-_a-z0-9]+.[pngif]{3})$', degustbrowser.views.static, name='staticcssimg'),

]
