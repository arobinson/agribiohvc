# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-07-06 04:55
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='BackgroundJob',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uid', models.CharField(max_length=50, unique=True)),
                ('name', models.CharField(default='Background Job', max_length=200)),
                ('session_key', models.CharField(default='', max_length=40)),
                ('status', models.CharField(choices=[('Q', 'Queued'), ('R', 'Running'), ('S', 'Success'), ('E', 'Error')], default='Q', max_length=2)),
                ('log', models.TextField(default='')),
                ('finished', models.BooleanField(default=False)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='BackgroundTask',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
                ('status', models.CharField(choices=[('Q', 'Queued'), ('R', 'Running'), ('S', 'Success'), ('E', 'Error')], default='Q', max_length=2)),
                ('order', models.IntegerField()),
                ('job', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='background.BackgroundJob')),
            ],
        ),
    ]
