from django.contrib import admin

from background import models

# Register your models here.
admin.site.register(models.BackgroundJob)
admin.site.register(models.BackgroundTask)
