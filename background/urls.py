from django.conf.urls import url

from background import views

urlpatterns = [
    url(r'^status/([-a-f0-9]+)$', views.status, name='backgroundstatus'),
]
