
/**
 * Updates the screen to display the status of a background task
 */
function doStatusCheck(uid, id, url) {
	$.ajax({
		url: url,
		cache: false,
		dataType: "json",
	})
	.done(function( obj ) {
		var icons = {Q:"<span class=\"glyphicon glyphicon-tasks\" aria-hidden=\"true\"></span>", 
				R:"<span class=\"glyphicon glyphicon-cog\" aria-hidden=\"true\"></span>", 
				S:"<span class=\"glyphicon glyphicon-ok\" aria-hidden=\"true\"></span>", 
				E:"<span class=\"glyphicon glyphicon-remove\" aria-hidden=\"true\"></span>"};
		var styles = {Q: "info",
				R: "warning",
				S: "success",
				E: "danger"};
		var status = {Q: "Queued",
				R: "Running",
				S: "Success",
				E: "Failed"};
		$("#background-"+id).show()
				.removeClass().addClass("panel panel-" + styles[obj.status]);
		$("#background-"+id+"-header")
				.removeClass().addClass("text-" + styles[obj.status])
				.empty().append("<h3 class=\"panel-title\"><b>" + icons[obj.status] + " " + status[obj.status] + "</b>: " + obj.name + "</h3>");
		var tasklist = $("<ul></ul>");
		var i;
		for (i = 0; i < obj.tasks.length; ++i) {
			var task = obj.tasks[i];
			tasklist.append($("<li>" + icons[task[2]] + " " + task[1] + "</li>").addClass("text-" + styles[task[2]]));
		}
		$("#background-"+id+"-body").empty().append(tasklist);
		
		var timeout = 1;
		if (obj.refresh)
			timeout = obj.refresh;
//		if (obj.status != 'S')
//			$("#background-"+id+"-body").collapse('show');
		
		// schedule next status check if it's still running
		if (obj.status != 'S' && obj.status != 'E') {
			window.setTimeout(function() {doStatusCheck(uid, id, url);}, timeout * 1000);
		}
	})
	.fail(function(jqXHR, textStatus, errorThrown) {
		$("#background-"+id).hide();
	});
}

