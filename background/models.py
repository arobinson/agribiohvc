from __future__ import unicode_literals

from django.db import models
import time

BACKGROUND_STATUS = (
               ('Q', 'Queued'),
               ('R', 'Running'),
               ('S', 'Success'),
               ('E', 'Error'),
              )

class BackgroundJob(models.Model):
    '''A collection of tasks to be run in background'''
    
    uid = models.CharField(max_length=50, unique=True)
    name = models.CharField(max_length=200, default="Background Job")
#     user = models.ForeignKey(User)
    session_key=models.CharField(max_length=40, default='')
    status = models.CharField(max_length=2, default="Q", choices=BACKGROUND_STATUS)
    log = models.TextField(default="")
    finished = models.BooleanField(default=False)
    
    # system values
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def simpleStructure(self):
        result = {
                  'uid': self.uid,
                  'name': self.name,
                  'status': self.status,
                  'log': self.log,
                  'finished': self.finished,
                  'created': self.created,
                  'modified': self.modified,
                 }
        
        tasks = []
        for task in self.backgroundtask_set.order_by('order'):
            tasks.append(task.simpleStructure())
        
        result['tasks'] = tasks
        
        return result
    
    def __str__(self):
        return "%s (%s)" % (self.name, self.uid)

# End class BackgroundJob()


class BackgroundTask(models.Model):
    '''A section of an AsyncJob'''
    
    name = models.CharField(max_length=200)
    status = models.CharField(max_length=2, default="Q", choices=BACKGROUND_STATUS)
    order = models.IntegerField()
    job = models.ForeignKey(BackgroundJob)
    
    def __init__(self, *args, **kwargs):
        models.Model.__init__(self, *args, **kwargs)
        self._start = time.time()
    
    def simpleStructure(self):
        return (self.order, self.name, self.status)
    
    def running(self):
        self.status = 'R'
        self._start = time.time()
        self.save()
        
    def success(self):
        self.status = 'S'
        self.name += " [%.2fs]" % round(time.time() - self._start,2)
        self.save()
        
    def error(self, if_running=False):
        '''
        Set status to error.  If *if_running* is true then only do so if current status is running
        '''
        if if_running and self.status == 'R' or not if_running:
            self.status = 'E'
            self.save()
    
    def setName(self, name):
        self.name = name
        self.save()
    
    def __str__(self):
        return "%s (%s)" % (self.name, self.status)
    
# End class BackgroundTask()
