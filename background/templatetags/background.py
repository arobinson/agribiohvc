'''
Created on 28 Jul 2016

@author: arobinson
'''

from django import template
from django.utils.html import format_html
from django.utils.safestring import mark_safe
from django.core.urlresolvers import reverse

register = template.Library() 


@register.filter(name='dict_lookup') 
def dict_lookup(k, d, default=''):
    try:
        if k in d:
            return d[k]
    except:
        pass
    return default


@register.filter(name='dir') 
def dir_val(o):
    try:
        return str(dir(o))
    except:
        pass
    return 0

@register.simple_tag
def backgroundtasks(btasks):
    '''Creates HTML to display background tasks'''
    
    output = '''        <script src="/static/background/base.js" type="text/javascript"></script>
        <link href="/static/background/base.css" rel="stylesheet" media="screen" />
        <link href="/static/bootstrap/glyphicons.css" rel="stylesheet" media="screen" />''';
    i = 1
    for bt in btasks:
        html = format_html("""        
        <div id="background-{id}" class="panel">
            <div class="panel-heading">
                <a id="background-{id}-header" href="#resultbody"></a>
            </div>
            <div id="background-{id}-body" class="panel-body">
            </div>
        </div>
        <script>$(document).ready(function () {{doStatusCheck('{uid}', '{id}', '{baseurl}');}});</script>
        """,
        id=i, uid=bt, baseurl=reverse('background:backgroundstatus', args=(bt,)))
        
        output += html
        
        i += 1
    
    
    return mark_safe(output)
    
    

