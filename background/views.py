
import logging

from django.http.response import JsonResponse, Http404

from background import models


def status(request, jobid):
    '''View that returns status for a background task'''
    
    try:
        # load the objects
        job = models.BackgroundJob.objects.get(uid=jobid, session_key=request.session.session_key)
        
        if job.status in ('S', 'E'):
            job.finished = True
            job.save()
        
        o = job.simpleStructure()
        o['refresh'] = 1
        
        return JsonResponse(o)
    
    except:
        logging.exception("error")
        pass
    raise Http404
