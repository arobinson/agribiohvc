'''
Created on 25 Aug 2016

@author: arobinson
'''

import atexit
import logging
import Queue
import threading
import uuid

import background.models
import time



def job(func):
    '''
    A decorator used to schedule a method-call to run in background process.
    
    The decorated method must accept >=2 parameters.  The first two parameters will
    be replaced by the BackgroundJob and BackgroundTask list objects.
    
    @param key: a unique key for who owns this job (probably their session key) (i.e. can see job status)
    @param name: the user-facing name (string) for this job
    @param tasks: a list-like object of user-facing names (string) for the tasks
    @param ...: any other args or kwargs which will be passed toy decorated method
    @return: the unique identifier (string) for this job (used with status check).
    '''
    def decorator(session_key, name, tasks, *args, **kwargs):
        
        # create job object
        uid = str(uuid.uuid4())
        job = background.models.BackgroundJob()
        job.uid = uid
        job.name = name
        job.session_key = session_key
        job.save()
        
        # add tasks
        for i in range(len(tasks)):
            task = background.models.BackgroundTask(name=tasks[i], order=i, job=job)
            task.save()
        
        # queue job
        _queue.put((func, session_key, uid, args, kwargs))
        logging.info("Queued: %s" % func.__name__)
        
        return uid
    return decorator


def runningJobs():
    '''
    Gets a list of UID's for all running jobs.  If job is finished (success 
    or error) then it will be marked as finished so it doesn't get returned 
    next call to this function
    '''
    
    # get running tasks
    jobs = []
    for bt in background.models.BackgroundJob.objects.filter(finished=False):
        if bt.status in ('S', 'E'):
            bt.finished = True
            bt.save()
        jobs.append(bt.uid)
        
    return jobs
    
    

##### Support #####

def _worker():
    '''
    Background thread entry point
    '''
    while True:
        func, session_key, uid, args, kwargs = _queue.get()
        try:
            job = background.models.BackgroundJob.objects.get(uid=uid)
            tasks = job.backgroundtask_set.order_by('order')
            
            logging.info("Starting: %s" % func.__name__)
            job.status='R'
            job.save()
            
            try:
                starttime = time.time()
                rval = func(session_key, job, tasks, *args, **kwargs)
                if rval != True:
                    job.status='E'
                    job.log += "Returned: %s\n" % (rval,)
                    job.name += " [%.2fs]" % round(time.time() - starttime,2)
                    job.save()
                else:
                    logging.info("Finished: %s" % func.__name__)
                    job.status='S'
                    job.name += " [%.2fs]" % round(time.time() - starttime,2)
                    job.save()
                    
            except:
                import traceback
                details = traceback.format_exc()
                job.log += "Raised Exception: %s\n" % details
                job.status='E'
                job.save()
        except:
            logging.exception("Background process exception")
        finally:
            _queue.task_done()  # so we can join at exit

## setup background thread and queue
_queue = Queue.Queue()
_thread = threading.Thread(target=_worker)
_thread.daemon = True
_thread.start()

## cleanup and not complete background jobs before exiting.
def _cleanup():
    _queue.join()

atexit.register(_cleanup)

