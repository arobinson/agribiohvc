'''
Created on 11Jul.,2017

@author: arobinson
'''
from django.contrib.admin.options import ModelAdmin



class DatasetAdmin(ModelAdmin):
    exclude = ('features',)
