
from django.http.response import HttpResponseRedirect, Http404, HttpResponse
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render_to_response
from django.urls import reverse
from AgriBioHVC import settings
try:
    from django.template.context_processors import csrf
except:
    from django.core.context_processors import csrf

import models
from common.bl.messagemanager import MessageManager


def login_user(request):
#     logout(request)
    username = password = ''
    if request.POST:
        username = request.POST['username']
        password = request.POST['password']

        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                MessageManager.addSuccess("Logged in as '%s'" % user.get_full_name(), request)
                return HttpResponseRedirect(request.POST.get('next', '/'))
            else:
                MessageManager.addError("User is not active.  Please contact administrator or use another", request)
        else:
            MessageManager.addError("Username and Password do not match", request)
            
        return HttpResponseRedirect(request.path)
    
    params = {
              'request': request, 
              'messages': MessageManager.getMessages(request),
              'group': 'user', 
              'parents': [
                  settings.HOME_LINK,
                  (settings.PORTAL_NAME, reverse('home')),
              ],
              'children': [ ],
              'page': 'Login', 
              'next': request.GET.get('next', '/'),
              'settings': settings,
              'title': 'Login',
             }
    params.update(csrf(request))
    return render_to_response('common/login.html', params)

def logout_user(request):
    '''Logs current user out of system'''
    
    logout(request)
    
    MessageManager.addSuccess("Logged out", request)
    
    return HttpResponseRedirect(request.GET.get('next', '/'))
    

def status(request):
    '''Status page for monitoring'''
    
    return HttpResponse("Status: OK", content_type="text/plain")


def content(request, page):
    '''Serves the request updateable content'''
    
    try:
        cont = models.UpdateableContent.objects.get(name=page)
        if cont.mime is None or not cont.mime.strip():
            raise Http404
    except:
        raise Http404
    
    return HttpResponse(cont.value, content_type=cont.mime)

def data_server(request, url):
    '''redirects all matching url's to the server specified in the config file'''
    
    return HttpResponseRedirect(settings.DATA_HOST + url)

