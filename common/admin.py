from django.contrib import admin

import common.modeladmin, common.models

admin.site.register(common.models.Collection)
admin.site.register(common.models.Dataset, common.modeladmin.DatasetAdmin)
admin.site.register(common.models.DatasetFeature)
admin.site.register(common.models.Reference)
admin.site.register(common.models.ReferenceFeature)
admin.site.register(common.models.Sample)
admin.site.register(common.models.Species)
admin.site.register(common.models.UpdateableContent)
admin.site.register(common.models.UpdateableTemplate)
admin.site.register(common.models.UpdateableValue)
