'''
Created on 14 Feb 2017

@author: arobinson
'''

from django.db import models

class MultiCharField(models.CharField):
    '''Field that represents a lists of strings in Python and stored in a CharField in database'''
    
    def get_prep_value(self, value):
        return unicode(value)
    def from_db_value(self, value, expression, connection, context):
        value = str(value)
        if value != "":
            return eval(str(value), {"__builtins__":None, 'False': False, 'True': True}, {})
        return []
    def to_python(self, value):
        value = str(value)
        if value != "":
            return eval(str(value), {"__builtins__":None, 'False': False, 'True': True}, {})
        return []
#     def value_to_string(self, obj):
#         return getattr(obj, self.attname)


class MultiTextField(models.TextField):
    '''Field that represents a lists of strings in Python and stored in a TextField in database'''
    
    def get_prep_value(self, value):
        return unicode(value)
    def from_db_value(self, value, expression, connection, context):
        value = str(value)
        if value != "":
            return eval(str(value), {"__builtins__":None, 'False': False, 'True': True}, {})
        return []
    def to_python(self, value):
        value = str(value)
        if value != "":
            return eval(str(value), {"__builtins__":None, 'False': False, 'True': True}, {})
        return []
#     def value_to_string(self, obj):
#         return getattr(obj, self.attname)
