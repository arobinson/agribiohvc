'''
Created on 06/12/2012

@author: arobinson
'''

from django.contrib.auth.models import User,Group
from AgriBioHVC import settings
import logging


class ActiveDirectoryBackend(object):
    '''An authentication backend that checks users against an active directory'''
    
# # Active Directory domain controller configuration (one or more)
# # domainlabel: the string appended to usernames (by user) that should be authenticated with (this) AD e.g. ADUSERNAME@<domainlabel> where ADUSERNAME is a user object in domain controller's directory
# # userdomain: the string appended to username in local database (i.e. 'ADUSERNAME@<userdomain>' will be the matching username in database)
# # domain controller: the full domainname for the domain controller
# # autocreate: create user in database for any use who can auth on active directory
# ACTIVEDIRECTORYDOMAINS = {
#     #        '<id>': ('<userdomain>', '<domain controller>', <autocreate>),
# #    'COMPX': ('COMPX', 'ad.compx.com', True),
# #    'compx.com': ('COMPX', 'ad.compx.com', True),
#     # ...
# }
    
    def authenticate(self, username=None, password=None):
        '''Checks the username and password match the AD user'''
        u = str(username)
        if u.find('@') >= 0:
            uname, domlbl = u.split('@', 1)
            if domlbl in settings.ACTIVEDIRECTORYDOMAINS:
                usrdom,domcont,autocreate = settings.ACTIVEDIRECTORYDOMAINS[domlbl]
                try:
                    userobj = User.objects.get(username="%s@%s" %(uname,usrdom))
                except User.DoesNotExist:
                    if autocreate:
                        userobj = User(username="%s@%s" %(uname,usrdom))
                        userobj.is_superuser = False
                        userobj.is_staff = False
                        userobj.set_unusable_password()
                    else:
                        userobj = None
                if userobj is not None and self._checkAuth(userobj, uname, password, domcont):
                    userobj.save()
                    try:
                        domain_group = Group.objects.get(name="%s_USERS"%(usrdom,))
                    except Group.DoesNotExist:
                        domain_group = Group(name="%s_USERS"%(usrdom,))
                        domain_group.save()
                    domain_group.user_set.add(userobj)
                    return userobj
        
        return None
    
    def get_user(self, user_id):
        '''Get the user object associated with the selected Id'''
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None
        
    def _checkAuth(self, userobj, username, password, domain):
        if userobj is None:
            return False
        import ldap
        server = "ldap://%s" % domain
        fullUsername = '%s@%s' % (username, domain)
        base = 'dc=' + ',dc='.join(domain.split('.'))
        
        Scope = ldap.SCOPE_SUBTREE
        Filter = "(&(objectClass=user)(sAMAccountName=%s))" % username
        attributes = ['mail', 'givenName', 'sn']
        ldap.set_option(ldap.OPT_REFERRALS, 0)
        l = ldap.initialize(server)
        l.protocol_version = 3
        try:
            l.simple_bind_s(fullUsername, password)
            res = l.search(base, Scope, Filter, attributes)
            
            # load the details from the AD into the user object
            _,user = l.result(res,60)
            _,attrs = user[0]
            if hasattr(attrs, 'has_key'):
                if attrs.has_key('mail') and str(attrs['mail'][0]) != "":
                    userobj.email = attrs['mail'][0]
                if attrs.has_key('givenName'):
                    userobj.first_name = attrs['givenName'][0]
                if attrs.has_key('sn'):
                    userobj.last_name = attrs['sn'][0]
                userobj.save()
        except ldap.INVALID_CREDENTIALS:
            return False
        except Exception as e:
            if settings.DEBUG:
                logging.exception("AD Login Exception")
            return False
        return True



