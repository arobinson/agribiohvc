'''
Created on 11Jul.,2017

@author: arobinson
'''

ORGANISMUID=r'\w[-\w]*'
ORGANISMUID_LABEL="Limited to letters (a-z,A-Z), numbers (0-9), dash(-) and underscore(_), cannot start with dash(-)"

COLLECTIONUID=r'\w[-\w]*'
COLLECTIONUID_LABEL="Limited to letters (a-z,A-Z), numbers (0-9), dash(-) and underscore(_), cannot start with dash(-)"

DATASETUID=r'\w[-\w]*'
DATASETUID_LABEL="Limited to letters (a-z,A-Z), numbers (0-9), dash(-) and underscore(_), cannot start with dash(-)"

SAMPLEUID=r'\w[-\w]*'
SAMPLEUID_LABEL="Limited to letters (a-z,A-Z), numbers (0-9), dash(-) and underscore(_), cannot start with dash(-)"

FEATUREUID=r'[^/]+'
FEATUREUID_LABEL="Any character except forward-slash(/)"

REFERENCEUID=r'\w[-\w]*'
REFERENCEUID_LABEL="Limited to letters (a-z,A-Z), numbers (0-9), dash(-) and underscore(_), cannot start with dash(-)"

