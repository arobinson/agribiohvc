'''
Created on 7Apr.,2017

@author: arobinson
'''

from django import template
from common import models

register = template.Library() 

@register.filter(name='referencetype')
def referencetype_filter(reftypeid):
    try:
        return models.REFERENCE_TYPE_LOOKUP[reftypeid]
    except:
        return ""

    