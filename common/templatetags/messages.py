'''
Created on 12/03/2016
@author: Andrew Robinson
'''

from django import template

from AgriBioHVC import settings

register = template.Library()

@register.inclusion_tag('common/messages.html')
def rendermessages(messages):
    '''Renders messages to HTML'''
    return {
            'messages': messages,
           }

@register.inclusion_tag('common/message.html')
def mkmsg(message):
    '''Renders a single message to HTML'''
    return {
            'class': settings.MSGCOLOURS[message[0]],
            'message': message[1],
           }
