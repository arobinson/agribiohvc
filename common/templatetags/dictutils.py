'''
Created on 20Jan.,2017

@author: arobinson
'''

from django import template

register = template.Library() 

@register.filter
def dictlookup(d, i):
    try:
        return d[i]
    except:
        return ""

