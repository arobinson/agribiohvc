'''
Created on 20Jan.,2017

@author: arobinson
'''

from django import template

register = template.Library() 

@register.filter
def last(l):
    try:
        return l[-1]
    except:
        return ""

@register.filter
def idx(l, i):
    try:
        return l[i]
    except:
        return ""
