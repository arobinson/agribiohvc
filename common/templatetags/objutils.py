'''
Created on 20Jan.,2017

@author: arobinson
'''

from django import template

register = template.Library() 

@register.filter('dir')
def _dir(d):
    try:
        return str(dir(d))
    except:
        return ""

