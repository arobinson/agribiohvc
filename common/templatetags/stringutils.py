'''
Created on 19Jan.,2017

@author: arobinson
'''

from django import template

register = template.Library() 

@register.filter
def maxlen(s, l):
    if len(s) > l:
        if l > 3:
            return s[:l-3] + "..."
        return s[:l]
    return s

@register.filter(name="len")
def _len(s):
    try:
        return len(s)
    except:
        return 0

@register.filter
def upper(s):
    try:
        return s.upper()
    except:
        return ""

@register.filter
def title(s):
    try:
        return s.title()
    except:
        return ""

@register.filter
def lower(s):
    try:
        return s.lower()
    except:
        return ""
