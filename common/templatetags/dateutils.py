'''
Created on 19Jan.,2017

@author: arobinson
'''

from django import template
from django.utils import timezone
from django.utils.safestring import mark_safe

register = template.Library() 

@register.filter
def past(d):
    return d <= timezone.now()

@register.filter
def future(d):
    return d >= timezone.now()

@register.simple_tag
def copyyear(year):
    try:
        thisyear = timezone.now().year
        if int(year) == thisyear:
            return year
        else:
            return  mark_safe("%s &ndash; %s" % (year, thisyear))
    except:
        return thisyear
    