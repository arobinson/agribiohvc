from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import Group
from django.utils import timezone

from common import modelfields



class Species(models.Model):
    '''Each species represented in database'''
    
    uid = models.CharField(max_length=50, help_text='Unique identifier used in URL.  e.g. barley')
    name = models.CharField(max_length=100, help_text='User pointing name for species.  e.g. Barley')
    description_header = models.TextField(null=True, blank=True, help_text='Markdown/HTML formatted text for inclusion above data-set list on the Species landing page')
    description_footer = models.TextField(null=True, blank=True, help_text='Markdown/HTML formatted text for inclusion below data-set list on the Species landing page')
    
    # system fields
    public_dataset_count = models.IntegerField(default=0, help_text='Automatically updated by Dataset model.  Displayed on homepage')
    
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    deleted = models.DateTimeField(blank=True, null=True, help_text="Set the date/time to delete this record.")

    def __str__(self):
        return "Organism: %s [/%s]" % (self.name, self.uid)
    
    def references(self):
        return self.reference_set.filter(deleted__isnull=True)

REFERENCE_TYPE_CHOICES = (
    ('ge','Genome'),
    ('dg','Draft Genome'),
    ('tr','Transcriptome'),
    ('dt','Draft Transcriptome'),
    ('st','Self-assembled Transcriptome'),
)
REFERENCE_TYPE_LOOKUP = dict(REFERENCE_TYPE_CHOICES)

# REFERENCE_FEATURES_CHOICES = (
#     ('gene','Genes'),
#     ('tran','Transcripts'),
# )
# REFERENCE_FEATURES_LOOKUP = dict(REFERENCE_FEATURES_CHOICES)

class Reference(models.Model):
    '''A reference genome for a Species'''
    
    uid = models.CharField(max_length=50, help_text='Unique identifier used in URL and efp database table name.  e.g. barley')
    name = models.CharField(max_length=100, help_text='User pointing name for reference.  e.g. Barley')
    type = models.CharField(max_length=50, blank=True, help_text='Type of reference genome/transcriptome', choices=REFERENCE_TYPE_CHOICES)
    description = models.TextField(null=True, blank=True, help_text='Markdown/HTML formatted text describing this reference e.g. where it was sourced, changes etc.')
    description_footer = models.TextField(null=True, blank=True, help_text='Extra Markdown/HTML formatted text describing this reference (below data-set list)')
    featurefile = models.CharField(max_length=56, blank=True, help_text='The name of the feature file (as saved in upload dir)', )
#     gfffeatures = modelfields.MultiCharField(max_length=20, blank=True, null=True, help_text='The types of features to include in reference', choices=REFERENCE_FEATURES_CHOICES)
    
    species = models.ForeignKey(Species)
    
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    deleted = models.DateTimeField(blank=True, null=True, help_text="Set the date/time to delete this record.")

    def __str__(self):
        return "Reference: %s [%s]" % (self.name, self.species.name)


class ReferenceFeature(models.Model):
    '''Transcript details for describing gene on landing page'''
     
    uid = models.CharField(max_length=50, help_text='Unique identifier used in URL and efp database table name.  e.g. barley')
    name = models.CharField(max_length=100, null=True, blank=True, help_text='User friendly name for feature if available')
    description = models.TextField(null=True, blank=True, help_text='Extra annotation for the gene/transcript/feature')
    
    reference = models.ForeignKey(Reference)
    
#     created = models.DateTimeField(auto_now_add=True)
#     modified = models.DateTimeField(auto_now=True)
#     deleted = models.DateTimeField(blank=True, null=True, help_text="Set the date/time to delete this record.")
    
    def __str__(self):
        if self.name:
            return "%s (%s)" % (self.name, self.uid)
        return self.uid


class Collection(models.Model):
    '''Group of data-sets'''
    
    uid = models.CharField(max_length=50, help_text='Unique identifier used in URL, file-system and database.  Restrict to [._a-zA-Z0-9].  e.g. smith2017')
    name = models.CharField(max_length=100, help_text='User pointing name for Collection.  e.g. AgriConnect, La Trobe etc.')
    name_short = models.CharField(max_length=50, null=True, blank=True, default=None, help_text='User pointing name used in breadcrumbs.  e.g. AgriConnect, La Trobe etc.')
    description_short = models.CharField(max_length=250, null=True, blank=True, help_text='Plain text formatted text for inclusion on home-page with collection link')
    description_header = models.TextField(null=True, blank=True, help_text='Markdown/HTML formatted text for inclusion above data-sets')
    description_footer = models.TextField(null=True, blank=True, help_text='Markdown/HTML formatted text for inclusion below data-sets')

    template = models.ForeignKey('UpdateableTemplate', help_text='The template to use to draw landing page / viewers within')
    link_ltu = models.BooleanField(default=True, help_text='Link to the La Trobe pages in landing portal pages')
    link_top = models.BooleanField(default=True, help_text='Link to the top level landing portal page in landing portal pages, i.e. page selecting collection')
    link_collection = models.BooleanField(default=True, help_text='Include this collection on list of collections on landing home-page')

    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    deleted = models.DateTimeField(blank=True, null=True, help_text="Set the date/time to delete this record.")

    def __str__(self):
        return "%s [/%s]" % (self.name, self.uid)
    
    def get_name_short(self):
        if self.name_short:
            return self.name_short
        return self.name
    def get_template(self):
        if self.template:
            return "UPDATEABLE_%s" % (self.template.name,)
        return None


COUNT_FORMAT_CHOICES = (
    ('RAW','Raw counts'),
    ('RPKM','RPKM/FPKM normalised counts'),
    ('TPM','TPM normalised counts'),
)
COUNT_FORMAT_LOOKUP = dict(COUNT_FORMAT_CHOICES)


class Dataset(models.Model):
    '''Each dataset represented in database'''
    
    class Meta:
        permissions = (
            ("can_view_any_dataset", "View Any Dataset"),
            ("can_edit_any_dataset", "Edit Any Dataset"),
        )
    
    # basic details
    uid = models.CharField(max_length=50, help_text='Unique identifier used in URL, file-system and database.  Restrict to [a-z0-9].  e.g. smith2017')
    name = models.CharField(max_length=100, help_text='User pointing name for Dataset.  e.g. Barley germination')
    description_header = models.TextField(null=True, blank=True, help_text='Markdown/HTML formatted text for inclusion above samples on data-set details')
    description_footer = models.TextField(null=True, blank=True, help_text='Markdown/HTML formatted text for inclusion below samples on data-set details')
    
    # file uploads
    countsfile = models.CharField(max_length=50, blank=True, help_text='The name of the counts file (as saved in upload dir)', )
    countsformat = models.CharField(max_length=5, blank=True, help_text='The type of counts in counts file', choices=COUNT_FORMAT_CHOICES)
    efphomeimage = models.CharField(max_length=50, blank=True, help_text='The name of the efpimage file used on homescreen i.e. without any expression. (as saved in upload dir)')
    efpexprimage = models.CharField(max_length=50, blank=True, help_text='The name of the efpimage file user to show expression (as saved in upload dir)')
    efpdownimage = models.CharField(max_length=50, blank=True, help_text='The name of the efpimage file user to show expression on download (as saved in upload dir)')
    countsdownload = models.BooleanField(default=False, blank=True, help_text='Check to allow users to download counts file')
    
    # detailed settings
    publish = models.DateTimeField(blank=True, null=True, help_text="Set the date/time to publish this dataset (publicly).  If in future it will automatically publish after this date.")
    labgroup = models.ForeignKey(Group, blank=True, null=True, help_text="The group of users who can access this dataset regardless if it's published")
    collections = models.ManyToManyField(Collection, blank=True, help_text='List of collections this data-set should be included in')
    metadata = models.TextField(blank=True, help_text='JSON formatted object representing the metadata of this dataset')
    
    externallinkurl = models.CharField(max_length=500, blank=True, help_text='URL to use for external link, GENEID is replaced with actual gene id', )
    externallinklabel = models.CharField(max_length=50, blank=True, help_text='Text label to use for external link, GENEID is replaced with actual gene id', )
    externallinkregex = models.CharField(max_length=100, blank=True, help_text='Regular expression used to select part of geneid to use in external link. e.g. remove transcript number', )
    externallinksub = models.CharField(max_length=50, blank=True, help_text='Substitution string used with regular expression e.g. remove transcript number', )
    
    species = models.ForeignKey(Species, blank=True, null=True, help_text='Organism of samples')
    reference = models.ForeignKey(Reference, default=None, blank=True, null=True, help_text='Reference used during mapping.  Data-set IDs must match reference IDs.')
    
    idcolumn = models.CharField(max_length=100, blank=True, null=True, help_text='The column in data-set which represents the gene/transcript ID.  This must match IDs used in reference.')
    genelengthcolumn = models.CharField(max_length=100, blank=True, null=True, help_text='Column which contains gene/transcript length.  This is used in TPM/RPKM normalisation if needed.')
    annotationcolumns = modelfields.MultiCharField(max_length=1000, blank=True, null=True, help_text='Any additional annotation columns.  Note: you need to include the ID column otherwise Degust gene links won\'t work')
    
    # eFP settings
    efpdeployed = models.BooleanField(default=False, blank=True, help_text='Set to true when successfully deployed to eFP Browser')
    graphtop = models.IntegerField(default=0, blank=True, help_text='')
    graphleft = models.IntegerField(default=0, blank=True, help_text='')
    graphbottom = models.IntegerField(default=0, blank=True, help_text='')
    graphright = models.IntegerField(default=0, blank=True, help_text='')
    legendtop = models.IntegerField(default=0, blank=True, help_text='')
    legendleft = models.IntegerField(default=0, blank=True, help_text='')
    efplinks = modelfields.MultiTextField(blank=True, null=True)
    efpconfigoverride = models.TextField(blank=True, null=True, default="CONF['']=''", help_text="Python code which updates keys in CONF dictionary.  Used during deployment to override the defaults.")
#     efpconfiggenerated = models.TextField(blank=True, null=True, default="{}", help_text="DO NOT EDIT!! (updated on deployment). Python dictionary representing the efp browser config values that are generated during deployment")
    efpconfig = models.TextField(blank=True, null=True, default="{}", help_text="DO NOT EDIT!! (updated on deployment). Python dictionary representing the efp browser config for this dataset")
    efpcountsformat = models.CharField(max_length=5, blank=True, help_text='The type of counts to use in eFP Browser', choices=COUNT_FORMAT_CHOICES)
    efpdescription = models.TextField(null=True, blank=True, help_text='Markdown/HTML formatted text for inclusion above eFP browser')
    efpprintimgmultiplier = models.FloatField(default=2.0, blank=True, help_text='Coordinate multiplier for print quality images.  i.e. 2.0 means print image is 2x height and width of display image')
    
    # degust settings
    degustdeployed = models.BooleanField(default=False, blank=True, help_text='Set to true when successfully deployed to Degust Browser')
    degustconfigoverride = models.TextField(blank=True, null=True, default="CONF['']=''", help_text="Python code which updates keys in CONF dictionary.  Used during deployment to override the defaults.")
    degustdescription = models.TextField(null=True, blank=True, help_text='Markdown/HTML formatted text for inclusion above Degust browser')
    
    # features
    features = models.ManyToManyField(ReferenceFeature, blank=True, help_text='List of features which this data-set has measurements for')
    
    # system settings
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    deleted = models.DateTimeField(blank=True, null=True, help_text="Set the date/time to delete this record.")

    def __str__(self):
        return "Dataset: %s [%s]" % (self.name, self.uid)
    
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        
        # update species count
        if self.species:
            self.species.public_dataset_count = self.species.dataset_set.filter(deleted__isnull=True, publish__lte=timezone.now()).count()
            self.species.save()
        
        return models.Model.save(self, force_insert=force_insert, force_update=force_update, using=using, update_fields=update_fields)
    
    def getstatus(self):
        if self.publish is None:
            return "Private"
        elif self.publish >= timezone.now():
            return "Public at %s" % self.publish
        else:
            return "Public"
    
    def samplecount(self):
        return self.sample_set.filter(deleted__isnull=True).count()




class Sample(models.Model):
    '''Each sample that is part of a dataset'''
    
    # general sample settings
    name = models.CharField(max_length=100, help_text='User pointing name for Sample.  e.g. 24-hour Control')
    is_control = models.BooleanField(blank=True, default=False, help_text='This sample is control sample.  Only set for one sample in each dataset')
    replicates = modelfields.MultiTextField(null=True, blank=True, default="", help_text='Tab-delimited list of replicate column names (matches uploaded dataset)')
    description = models.TextField(null=True, blank=True, help_text='Markdown/HTML formatted text describing this sample such as treatments and/or time points')
    description_footer = models.TextField(null=True, blank=True, help_text='Markdown/HTML formatted text for inclusion below samples on data-set details')
    order = models.IntegerField(default=0, null=True, help_text='Order to display samples (in degust)')
    dataset = models.ForeignKey(Dataset)
    
    # efp sample settings
    efp_colour = models.CharField(max_length=7, default="", null=True, blank=True, help_text='The HTML colour code for this sample in eFP Image. e.g. #FF0000')
    efp_coords = models.CharField(max_length=1000, default="", null=True, blank=True, help_text='Imagemap area i.e. comma-separated x,y coords of click polygon')
    
    # system
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    deleted = models.DateTimeField(blank=True, null=True, help_text="Set the date/time to delete this record.")

    def __str__(self):
        deleted = ""
        if self.deleted:
            deleted = "Deleted "
        if self.dataset_id:
            return "%sSample: %s [%s]" % (deleted, self.name, self.dataset.name)
        return "%sSample: %s" % (deleted, self.name)


class DatasetFeature(models.Model):
    '''Transcript details for describing gene on landing page'''
     
    uid = models.CharField(max_length=50, help_text='Unique identifier used in URL and efp database table name.  e.g. barley')
    name = models.CharField(max_length=100, null=True, blank=True, help_text='User friendly name for feature if available')
    description = models.TextField(null=True, blank=True, help_text='Extra annotation for the gene/transcript/feature')
    
    dataset = models.ForeignKey(Dataset)
    
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    deleted = models.DateTimeField(blank=True, null=True, help_text="Set the date/time to delete this record.")
    
    def __str__(self):
        if self.name:
            return "%s (%s)" % (self.name, self.uid)
        return self.uid
   
   
class UpdateableContent(models.Model):
    '''A serve-able content such as html, js or css'''
    
    name = models.CharField(max_length=20)
    value = models.TextField()
    mime = models.CharField(max_length=50)
    
    def __str__(self):
        if len(self.value) > 40:
            return "%s: %s..." % (self.name, self.value[:40])
        return "%s: %s" % (self.name, self.value)

class UpdateableTemplate(models.Model):
    '''Template objects'''
    
    name = models.CharField(max_length=20)
    value = models.TextField() #models.TextField()
    
    def __str__(self):
        return "UpdateableTemplate: %s" % (self.name)

class UpdateableValue(models.Model):
    '''Values that are used on screen'''
    
    name = models.CharField(max_length=20)
    value = models.TextField(blank=True, null=True)
    
    def __str__(self):
        return "UpdateableValue: %s" % (self.name)



