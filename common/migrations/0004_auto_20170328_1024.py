# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-03-27 23:24
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('common', '0003_dataset_degustconfigoverride'),
    ]

    operations = [
        migrations.AddField(
            model_name='dataset',
            name='description_footer',
            field=models.TextField(blank=True, help_text='Markdown/HTML formatted text for inclusion below samples on data-set details', null=True),
        ),
        migrations.AddField(
            model_name='dataset',
            name='description_header',
            field=models.TextField(blank=True, help_text='Markdown/HTML formatted text for inclusion above samples on data-set details', null=True),
        ),
        migrations.AddField(
            model_name='sample',
            name='description_footer',
            field=models.TextField(blank=True, help_text='Markdown/HTML formatted text for inclusion below samples on data-set details', null=True),
        ),
        migrations.AddField(
            model_name='species',
            name='description_footer',
            field=models.TextField(blank=True, help_text='Markdown/HTML formatted text for inclusion below data-set list on the Species landing page', null=True),
        ),
        migrations.AddField(
            model_name='species',
            name='description_header',
            field=models.TextField(blank=True, help_text='Markdown/HTML formatted text for inclusion above data-set list on the Species landing page', null=True),
        ),
        migrations.AlterField(
            model_name='dataset',
            name='degustconfigoverride',
            field=models.TextField(blank=True, default="CONF['']=''", help_text='Python code which updates keys in CONF dictionary.  Used during deployment to override the defaults.', null=True),
        ),
        migrations.AlterField(
            model_name='reference',
            name='description',
            field=models.TextField(blank=True, help_text='Markdown/HTML formatted text describing this reference e.g. where it was sourced, changes etc.', null=True),
        ),
        migrations.AlterField(
            model_name='sample',
            name='description',
            field=models.TextField(blank=True, help_text='Markdown/HTML formatted text describing this sample such as treatments and/or time points', null=True),
        ),
    ]
