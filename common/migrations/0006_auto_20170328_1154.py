# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-03-28 00:54
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('common', '0005_updateablevalue'),
    ]

    operations = [
        migrations.AddField(
            model_name='dataset',
            name='degustdeployed',
            field=models.BooleanField(default=False, help_text='Set to true when successfully deployed to Degust Browser'),
        ),
        migrations.AddField(
            model_name='dataset',
            name='efpdeployed',
            field=models.BooleanField(default=False, help_text='Set to true when successfully deployed to eFP Browser'),
        ),
        migrations.AlterField(
            model_name='updateablevalue',
            name='value',
            field=models.TextField(blank=True, null=True),
        ),
    ]
