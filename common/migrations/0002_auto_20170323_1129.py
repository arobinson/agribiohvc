# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-03-23 00:29
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('common', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='dataset',
            options={'permissions': (('can_view_any_dataset', 'View Any Dataset'), ('can_edit_any_dataset', 'Edit Any Dataset'))},
        ),
    ]
