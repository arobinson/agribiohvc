# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-04-11 00:29
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('common', '0010_auto_20170407_1435'),
    ]

    operations = [
        migrations.RenameField(
            model_name='reference',
            old_name='gfffile',
            new_name='featurefile',
        ),
        migrations.RemoveField(
            model_name='reference',
            name='gfffeatures',
        ),
    ]
