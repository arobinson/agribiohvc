'''
Created on 27Jan.,2017

@author: arobinson
'''
from django.forms import models


class MultiObjectFormField(models.Field):
    '''
    An object that allows user to create multiple objects dynamically and 
    stores result as a string encoded list of dicts.
    '''
    
    def __init__(self, fields, *args, **kwargs):
        '''
        @param fields: a list of field names to store
        '''
        super(MultiObjectFormField, self).__init__(*args, **kwargs)
        
        self.fields = fields

    def clean(self, value):
        return models.Field.clean(self, value)