'''
Created on 23Mar.,2017

@author: arobinson
'''
import logging

from django.db.models import Q
from django.utils import timezone

import common.models

class DataManager(object):
    '''Aux methods for models'''
    
    @classmethod
    def canViewDataset(cls, dataset, user):
        '''
        Checks if provided user can view this dataset.
        
        @param dataset: the dataset to check
        @param user: the user object to check
        @return: boolean, True if they have permission
        '''
        
        # public dataset
        if cls.isDatasetPublic(dataset):
            return True
        
        # in labgroup
        if dataset.labgroup is not None and user.groups.filter(id=dataset.labgroup.id).exists():
            return True
        
        # has admin permission
        if user.has_perm('common.can_view_any_dataset'):
            return True
        
        return False

    @classmethod
    def isDatasetPublic(cls, dataset):
        '''
        Checks if a dataset is published
        
        @param dataset: the dataset to check
        @return: boolean, True if dataset is public at the present time
        '''
        
        return dataset.publish is not None and dataset.publish <= timezone.now()
    
    @classmethod
    def getDatasetsForUser(cls, user, datasets=common.models.Dataset.objects):
        '''
        Returns a queryset containing data-sets that user is allowed to view
        
        @param user: Django User object to check which datasets they can access
        @param datasets: A queryset (of datasets) which will be filtered to only datasets user can access.  Default: all datasets in database
        
        @return: Queryset: filtered to only show datasets user can view
        '''
        
        try:
            if (user.has_perm('AgriBioHVC.can_view_any_dataset')):
                return datasets
            
            return datasets.filter(Q(publish__lte=timezone.now())|Q(labgroup__in=user.groups.all()))
        except:
            logging.exception("getDatasetsForUser error")
            return []
        
    @classmethod
    def getOrganismsForCollectionAndUser(cls, collection, user):
        '''Returns a list of Organisms that should be displayed for a Collection / User combination'''
        
        #TODO: make this filter only Organisms that have a dataset
        datasets = cls.getDatasetsForUser(user, collection.dataset_set).filter(species__isnull=False, deleted__isnull=True)
        return common.models.Species.objects.filter(deleted__isnull=True, id__in=datasets.values_list('species_id', flat=True))

    @classmethod
    def getCollections(cls, user):
        '''Returns a list of collections available for user'''
        
        #TODO: make this filter only ones this user can view
        return common.models.Collection.objects.filter(deleted__isnull=True)

