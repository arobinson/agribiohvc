'''
Created on 09/03/2016
@author: Andrew Robinson
'''

class MessageManager():
    '''Class responsible for passing messages via the session to render on views'''
    
    SESSION_KEY='__Messages'
    
    @classmethod
    def addSuccess(cls, message, req):
        '''Add an success level message to the next view (Green)'''
        
        msgs = req.session.get(cls.SESSION_KEY, [])
        msgs.append(('SUCCESS', message))
        req.session[cls.SESSION_KEY] = msgs
    
    @classmethod
    def addMessage(cls, message, req):
        '''Add an message level message to the next view (Blue)'''
        
        msgs = req.session.get(cls.SESSION_KEY, [])
        msgs.append(('MESSAGE', message))
        req.session[cls.SESSION_KEY] = msgs
    
    @classmethod
    def addWarning(cls, message, req):
        '''Add an warning level message to the next view (Orange)'''
        
        msgs = req.session.get(cls.SESSION_KEY, [])
        msgs.append(('WARNING', message))
        req.session[cls.SESSION_KEY] = msgs
    
    @classmethod
    def addError(cls, message, req):
        '''Add an error level message to the next view (Red)'''
        
        msgs = req.session.get(cls.SESSION_KEY, [])
        msgs.append(('ERROR', message))
        req.session[cls.SESSION_KEY] = msgs

    @classmethod
    def getMessages(cls, req):
        '''Returns a lambda function that will retrieve any messages and remove 
        them from the list'''
        
        return LazyMessageList(req) 


class LazyMessageList():
    '''
    List like object that retrieves the list of messages from
    the session only on first access.
    '''
    
    def __init__(self, request):
        self._request = request
        self._data = None
        
    def __len__(self):
        self._loadData()
        return len(self._data)
    
    def __str__(self):
        self._loadData()
        return str(self._data)
    
    def __iter__(self):
        self._loadData()
        return self._data.__iter__()
        
    def _loadData(self):
        if self._data is None:
            self._data = self._request.session.get(MessageManager.SESSION_KEY,[])
            if MessageManager.SESSION_KEY in self._request.session:
                self._request.session[MessageManager.SESSION_KEY] = []
