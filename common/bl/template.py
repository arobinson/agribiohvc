'''
Created on 24Feb.,2017

@author: arobinson
'''
from django.template.loaders.base import Loader
from common.models import UpdateableTemplate
from django.template.base import Origin, Template
from django.template.exceptions import TemplateDoesNotExist
import logging

# import django.template.backends.django

# 
# django.template.backends.django.DjangoTemplates

PREFIX='UPDATEABLE_'

class DatabaseLoader(Loader):
    '''Loads templates from UpdateableTemplate model'''
    
    def get_template_sources(self, template_name):
        
        if template_name.startswith(PREFIX):
            tn = template_name[len(PREFIX):]
            if UpdateableTemplate.objects.filter(name=tn).exists():
                yield Origin(tn, template_name, self)
    
    def get_contents(self, origin):
        
        try:
            assert origin.loader == self
            
            tp = UpdateableTemplate.objects.get(name=origin.name)
            
            return tp.value.strip()
#             return unicode(tp.value, "utf-8").strip()
        except AssertionError:
            pass
        except:
            logging.exception('%s does not exist' % origin.template_name)
        raise TemplateDoesNotExist('%s does not exist' % origin.template_name)
    
    def get_template(self, template_name, template_dirs=None, skip=None):
        
        try:
            assert template_name.startswith(PREFIX)
            
            tn = template_name[len(PREFIX):]
            tp = UpdateableTemplate.objects.get(name=tn)
            
            return Template(tp.value.strip(), name=template_name)
        except AssertionError:
            pass
        except:
            logging.exception('%s does not exist' % template_name)
        raise TemplateDoesNotExist('%s does not exist' % template_name)

