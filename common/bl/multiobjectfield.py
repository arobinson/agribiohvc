'''
Created on 27Jan.,2017

@author: arobinson
'''
from django.db import models
from common.ui.multiobjectformfield import MultiObjectFormField


class MultiObjectField(models.TextField):
    '''
    An object that allows user to create multiple objects dynamically and 
    stores result as a string encoded list of dicts.
    '''
    
    def __init__(self, fields, *args, **kwargs):
        '''
        @param fields: a list of field names to store
        '''
        super(MultiObjectField, self).__init__(*args, **kwargs)
        
        self.fields = fields
        
        
    def deconstruct(self):
        name, path, args, kwargs = super(MultiObjectField, self).deconstruct()
        
        args.append(self.fields)
        
        return name, path, args, kwargs
    
    def from_db_value(self, value, expression, connection, context):
        if value is None:
            return value
        return str(value)

    def to_python(self, value):
        if isinstance(value, dict):
            return value

        if value is None:
            return value

        return eval(value, {"__builtins__":None, 'False': False, 'True': True}, {})
    
    def get_prep_value(self, value):
        return str(value)

    def formfield(self, **kwargs):
        defaults = {'form_class': MultiObjectFormField}
        defaults.update(kwargs)
        return super(MultiObjectField, self).formfield(**defaults)
